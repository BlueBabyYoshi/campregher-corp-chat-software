package tests.base.entities;

import lib.model.BaseModel;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The type Test entity.
 */
@Entity
@Table(name = "TestEntities")
@Access(AccessType.PROPERTY)
public class TestEntity extends BaseModel {

    private String name;
    private Boolean isTest;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets test.
     *
     * @return the test
     */
    public Boolean getTest() {
        return isTest;
    }

    /**
     * Sets test.
     *
     * @param test the test
     */
    public void setTest(Boolean test) {
        isTest = test;
    }
}

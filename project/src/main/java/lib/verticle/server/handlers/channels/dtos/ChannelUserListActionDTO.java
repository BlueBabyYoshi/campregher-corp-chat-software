package lib.verticle.server.handlers.channels.dtos;

import java.util.List;

public class ChannelUserListActionDTO extends ChannelIdDTO {

    private List<Integer> userIds;

    public List<Integer> getUserId() {
        return userIds;
    }

    public void setUserId(List<Integer> userId) {
        this.userIds = userId;
    }
}

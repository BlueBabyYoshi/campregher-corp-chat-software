package lib.Database.datasources;

import javax.sql.DataSource;

/**
 * The interface Data source factory.
 */
public interface DataSourceFactory {

    /**
     * Create data source.
     *
     * @param url      the url
     * @param username the username
     * @param password the password
     * @return the data source
     */
    DataSource createDatasource(String url, String username, String password);
}

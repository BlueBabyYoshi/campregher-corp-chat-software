package lib.annotations;

import java.lang.annotation.Annotation;

/**
 * The interface Annotation found handler.
 */
public interface IAnnotationFoundHandler {

    /**
     * Hands over class and Annotation to the set Handler when annotation was found
     *
     * @param cls        the class
     * @param annotation the annotation
     */
    void annotationFound(Class<?> cls, Annotation annotation);
}

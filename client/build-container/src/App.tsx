import * as React from "react";
import { BrowserRouter, Switch, Route, Link, Redirect } from "react-router-dom";
import * as CSS from "csstype";
import NavBar from "./components/NavBar/navBar";
import Footer from "./components/footer/footer";
import { Login } from "./components/Login/Login";
import Test from "./components/Test/test";
import {profile} from "./profile/profile";
import home from "./secured/home";
import userAdminSettings from "./components/admin/userAdminSettings";
import logout from "./components/logout/logout";
import Avatar from "@material-ui/core/Avatar";
import Cookies from 'universal-cookie';
import {AuthorizationResultDTO} from "./Dtos/AuthorizationResultDTO";
import {AuthorizationDTO} from "./Dtos/AuthorizationDTO";
import {newUser} from "./components/admin/newUser";
import { MonisMessages } from "./components/channel/MonisMessage";



//https://medium.com/@glweems/simple-navbar-component-using-react-typescript-and-styled-components-54e357e2cbcb

const container: CSS.Properties = {
  display: "flex",
  alignItems: "flex-start"
};

const pageStyle: CSS.Properties = {
  position: "relative",
  minHeight: "100vh"
};

const contentStyle: CSS.Properties = {
  paddingBottom: "2.5rem"
};

const footerStyle: CSS.Properties = {
  position: "absolute",
  bottom: "0",
  height: "2.5rem",
  width: "100%"
};


async function isAuthorized(role: string): Promise<AuthorizationResultDTO> {
    let resultDTO: AuthorizationResultDTO;
    const cookies = new Cookies();
    const sessionId = cookies.get('vertx-web.session');
    if (sessionId == undefined) {
        resultDTO = {
            isAuthenticated: false,
            isAuthorized: false
        };
        return resultDTO;
    }
    let authDTO: AuthorizationDTO = {
        sessionId: sessionId,
        role: role
    }
    try {
        let response = await fetch("http://localhost:8080/checkAuthorization", {
            headers: {
            "Content-Type": "application/json"
            },
            method: "POST",
            body: JSON.stringify(authDTO),
            mode: "cors",
            credentials: "include"
            });
        let jsonResult = await response.json();
        resultDTO = JSON.parse(JSON.stringify(jsonResult));
        console.log(resultDTO);
        return resultDTO;
    } catch (e) {
        console.log(e);
        resultDTO = {
            isAuthenticated: false,
            isAuthorized: false
        };
        console.log(resultDTO);
        return resultDTO;
    }
}

const RequireAuth = (Component, role: string) => {
    return class App extends React.Component {
        state = {
            isAuthenticated: false,
            isAuthorized: false,
            isLoading: true
        }

        componentDidMount() {
            isAuthorized(role).then((res) => {
                console.log(res);
                this.setState({isAuthenticated: res.isAuthenticated, isAuthorized: res.isAuthorized, isLoading: false});
            }).catch(() => {
                this.setState({isAuthenticated: false, isAuthorized: false, isLoading: false});
            })
        }

        render() {
            const { isAuthenticated, isAuthorized, isLoading } = this.state;
            if (isLoading) {
                return <div>Loading...</div>
            }
            if (!isAuthenticated) {
                return <Redirect to="/login" />
            }
            if (!isAuthorized) {
                return <Redirect to="/" />
            }
            return <Component {...this.props} />
        }
    }
}

export { RequireAuth }


//please add the elements you want to link to
const navigation = {
  brand: { name: "Navbar", to: "/" },
  links: [
    { name: "Home", to: "/secured/home" },
    { name: "Login", to: "/login" },
    { name: "Profil", to: "/profile/profile" },
    { name: "Benutzerliste", to: "/admin/userAdminSettings" },
    { name: "Benutzer anlegen", to: "/admin/newUser"},
    { name: "Logout", to: "/logout" },
    { name: "MonisMessages", to: "/channel/MonisMessages" },
  ]
};


{
  /*npm install react-burger-menu --save*/
}
class App extends React.Component {
  render() {
    // Descructured object
    const { brand, links } = navigation;
    return (
      <div className="App" style={pageStyle}>
        <div className="content" style={contentStyle}>
          <div className="navigation">
            <NavBar brand={brand} links={links} />
          </div>

          <div className="main">
            <BrowserRouter>
              {/* write down menu bar here and don't forget them in const navigation in this file (spell them correctly*/}
              <Switch>
                <Route path="/login" component={Login} />
                <Route path="/secured/home" component={RequireAuth(home, "")} />
                <Route path="/Test/test" component={RequireAuth(Test, "")} />
                <Route path="/profile/profile" component={RequireAuth(profile, "")} />
                <Route path="/channel/MonisMessages" component={RequireAuth(MonisMessages, "")} />
                <Route path="/admin/userAdminSettings" component={RequireAuth(userAdminSettings, "Admin")} />
                <Route path="/admin/newUser" component={RequireAuth(newUser, "Admin")} />
                <Route path="/logout" component={RequireAuth(logout, "")} />
                <Route path="/footer/footer" component={Footer} />
              </Switch>
            </BrowserRouter>
          </div>
        </div>

        <div className="footer" style={footerStyle}>
          <Footer />
        </div>
      </div>
    );
  }
}

export default App;

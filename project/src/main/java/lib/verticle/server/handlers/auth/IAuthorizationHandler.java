package lib.verticle.server.handlers.auth;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Authorization handler.
 */
public interface IAuthorizationHandler extends IHttpHandler {
}

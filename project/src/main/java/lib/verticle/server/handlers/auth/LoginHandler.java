package lib.verticle.server.handlers.auth;

import com.google.gson.Gson;
import com.google.inject.Inject;
import controller.AuthenticationController;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.DatasourceService;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.verticle.server.dtos.LoginDTO;
import lib.verticle.server.dtos.LoginResultDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

/**
 * Handler that takes the plain password and username and fowrds the information to the Autentication Controller who does the actual logic
 */
@HttpHandler(endpoint = HandlerEndpoints.Authorization.LOGIN, method = HttpMethod.POST, interfaceType = ILoginHandler.class)
public class LoginHandler extends AbstractHttpHandler implements ILoginHandler {

    @Inject
    private IQueryService queryService;
    private AuthenticationController controller;

    @Override
    public void handle(RoutingContext context) {
        System.out.println("test");
        var gson = new Gson();
        LoginDTO loginInfo = gson.fromJson(context.getBodyAsString(), LoginDTO.class);
        if (loginInfo != null) {
            DatasourceService datasourceService = new DatasourceService();
            controller = new AuthenticationController(getVertx(), queryService, datasourceService.createDataSource());
            controller.loginUser(loginInfo).setHandler(res -> {
                LoginResultDTO resultDTO = new LoginResultDTO();
                if (res.succeeded()) {
                    User user = res.result();
                    context.setUser(user);
                    context.session().put("user", user);
                    context.session().put("username", loginInfo.getUsername());
                    resultDTO.setSuccess(true);
                    resultDTO.setMessage("Benutzer " + user.principal().getString("username") + " erfolgreich eingeloggt");

                    user.isAuthorized("role:" + "Admin", auth -> {
                        if (auth.succeeded()) {
                            boolean result = auth.result();
                            resultDTO.setAdmin(result);
                            System.out.println(resultDTO.getMessage() + " inside " + result);
                            writeResponseToContext(context, resultDTO);
                        }

                    });
                } else {
                    resultDTO.setSuccess(false);
                    resultDTO.setMessage("Konnte Benutzer nicht einloggen.");
                    resultDTO.setAdmin(false);
                    System.out.println(resultDTO.getMessage());
                    writeResponseToContext(context, resultDTO);
                }
            });
        }
    }

    private void writeResponseToContext(RoutingContext context, LoginResultDTO dto) {
        var gson = new Gson();
        var resultAsString = gson.toJson(dto);
        System.out.println(resultAsString);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

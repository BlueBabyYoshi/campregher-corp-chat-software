package lib.Database.persistenceModel;

import java.util.List;

/**
 * The Persistence unit.
 */
public class PersistenceUnit extends BaseXmlElement {

    private String name;
    private List<PersistenceProperty> properties;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets properties.
     *
     * @return the properties
     */
    public List<PersistenceProperty> getProperties() {
        return properties;
    }

    /**
     * Sets properties.
     *
     * @param properties the properties
     */
    public void setProperties(List<PersistenceProperty> properties) {
        this.properties = properties;
    }
}

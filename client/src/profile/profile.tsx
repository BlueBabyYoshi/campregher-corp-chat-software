import * as React from "react";
import TextField from '@material-ui/core/TextField';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Radio from '@material-ui/core/Radio';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import * as CSS from 'csstype';
import {UserInfoState} from "./UserInfoState";
import {UserInfoDTO} from "../Dtos/UserInfoDTO";
import {NewUserResultDTO} from "../Dtos/newUserResultDTO";
import {EditUserResultDTO} from "../Dtos/EditUserResultDTO";
import Typography from "@material-ui/core/Typography";
import FormGroup from "@material-ui/core/FormGroup";
import {Checkbox} from "@material-ui/core";
import Button from "@material-ui/core/Button";

export class profile extends React.Component<UserInfoState> {

    public state: UserInfoState = {
        username: "",
        name: "",
        emailAddress: "",
        isAdmin: false,
        isUser: false,
        department: "",
        office: "",
        emailFrequency: "",
        isActiveUser: true,
        isLoading: false,
        isSameUser: false,
        result: ""
    }

    private userInfoDTO: UserInfoDTO | undefined;

    constructor(props) {
        super(props);
        this.onSaveClick = this.onSaveClick.bind(this);
        this.onResultsAreIn = this.onResultsAreIn.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.onUserInfoIsIn = this.onUserInfoIsIn.bind(this);
        this.onNameChanged = this.onNameChanged.bind(this);
        this.onEmailAddressChanged = this.onEmailAddressChanged.bind(this);
        this.onDepartmentChanged = this.onDepartmentChanged.bind(this);
        this.onOfficeChanged = this.onOfficeChanged.bind(this);
        this.onEmailFrequencyChanged = this.onEmailFrequencyChanged.bind(this);
    }

    public async onSaveClick(): Promise<void> {
        this.userInfoDTO = {
            username: this.state.username,
            name: this.state.name,
            emailAddress: this.state.emailAddress,
            isAdmin: this.state.isAdmin,
            isUser: this.state.isUser,
            department: this.state.department,
            office: this.state.office,
            emailFrequency: this.state.emailFrequency,
            isActiveUser: this.state.isActiveUser,
            isSameUser: this.state.isSameUser
        };
        var stringVal = JSON.stringify({
            username: this.state.username,
            name: this.state.name,
            emailAddress: this.state.emailAddress,
            department: this.state.department,
            office: this.state.office,
            emailFrequency: this.state.emailFrequency
        });
        console.log(stringVal);
        try {
            let response = await fetch("http://localhost:8080/editUser", {
                headers: {
                    "Content-Type": "application/json",
                },
                method: "POST",
                body: JSON.stringify(this.userInfoDTO),
                mode: "cors",
                credentials: "include"
            });
            let jsonResult = await response.json();
            let jsonObject: EditUserResultDTO = JSON.parse(JSON.stringify(jsonResult));
            this.onResultsAreIn(jsonObject);
        } catch(e) {
            console.log("User Edit Error");
            this.setState({
                ...this.state,
                result: "Fehler bei Webserverabruf."
            });
            console.log(e);
        }
    }

    public onResultsAreIn(dto: EditUserResultDTO): void {
        this.setState({
            ...this.state,
            result: dto.message
        });
    }

    public onUserInfoIsIn(dto: UserInfoDTO): void {
        this.setState({
            ...this.state,
            username: dto.username,
            name: dto.name,
            emailAddress: dto.emailAddress,
            isAdmin: dto.isAdmin,
            isUser: dto.isUser,
            department: dto.department,
            office: dto.office,
            emailFrequency: dto.emailFrequency,
            isActiveUser: dto.isActiveUser,
            isSameUser: dto.isSameUser,
            isLoading: false
        });
    }

    public async componentDidMount(): Promise<void> {
        this.setState({...this.state, isLoading: true});
        try {
            let response = await fetch("http://localhost:8080/userInfo", {
                headers: {
                    "Content-Type": "application/json",
                },
                method: "POST",
                body: "",
                mode: "cors",
                credentials: "include"
            });
            let jsonResult = await response.json();
            let jsonObject: UserInfoDTO = JSON.parse(JSON.stringify(jsonResult));
            this.onUserInfoIsIn(jsonObject);
        } catch(e) {
            console.log("User Info Error");
            this.setState({
                ...this.state,
                result: "Fehler bei Webserverabruf."
            });
            console.log(e);
        }
    }

    public onNameChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            name: event.target.value
        });
    }

    public onEmailAddressChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            emailAddress: event.target.value
        });
    }


    public onDepartmentChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            department: event.target.value
        });
    }

    public onOfficeChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            office: event.target.value
        });
    }

    public onEmailFrequencyChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            emailFrequency: event.target.value
        });
    }

    render() {

        const formStyle: CSS.Properties = {
            padding: '10px 5px 5px 10px',
            textAlign:'center'
        };
  
        const useStyles = makeStyles((theme: Theme) =>
            createStyles({
                formControl: {
                    margin: theme.spacing(3),
                },
            }),
        );

        if (this.state.isLoading) {
            return <p>Loading ...</p>;
        }

        return (
            <form style={formStyle}>
                <Typography component="h1" variant="h5">
                    Profil
                </Typography>
                <div>
                    <h5>Username:</h5>
                    <TextField value={this.state.username} required={true} variant="outlined" margin="normal" disabled={true} />
                </div>
                <div>
                    <h5>Name:</h5>
                    <TextField value={this.state.name} required={true} variant="outlined" margin="normal" onChange={this.onNameChanged} />
                </div>
                <div>
                    <h5>Email-Adresse:</h5>
                    <TextField value={this.state.emailAddress} required={true} variant="outlined" margin="normal" onChange={this.onEmailAddressChanged} />
                </div>
                <div>
                    <FormControl component="fieldset">
                        <FormLabel component="legend">Rechte:</FormLabel>
                        <FormGroup>
                            <FormControlLabel label="Admin" control={<Checkbox />} value={this.state.isAdmin} checked={this.state.isAdmin === true} disabled={true} />
                            <FormControlLabel label="Benutzer" control={<Checkbox />} value={this.state.isUser} checked={this.state.isUser === true} disabled={true} />
                        </FormGroup>
                    </FormControl>
                </div>
                <div>
                    <FormControl component="fieldset" >
                        <FormLabel component="legend">Abteilung:</FormLabel>
                        <RadioGroup aria-label="department" name="department">
                            <FormControlLabel value="Other" control={<Radio />} label="Andere" checked={this.state.department === "Other"} onChange={this.onDepartmentChanged} />
                            <FormControlLabel value="HR" control={<Radio />} label="Personal" checked={this.state.department === "HR"} onChange={this.onDepartmentChanged} />
                            <FormControlLabel value="Marketing" control={<Radio />} label="Marketing" checked={this.state.department === "Marketing"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Sales" control={<Radio />} label="Verkauf" checked={this.state.department === "Sales"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Production" control={<Radio />} label="Produktion" checked={this.state.department === "Production"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="IT" control={<Radio />} label="IT" checked={this.state.department === "IT"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Finance" control={<Radio />} label="Finanz" checked={this.state.department === "Finance"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Management" control={<Radio />} label="Management" checked={this.state.department === "Management"} onChange={this.onDepartmentChanged}/>
                        </RadioGroup>
                    </FormControl>
                </div>
                <div>
                    <h5>Büro:</h5>
                    <TextField value={this.state.office} required={true} variant="outlined" margin="normal" onChange={this.onOfficeChanged} />
                </div>
                <div>
                    <FormControl component="fieldset" >
                        <FormLabel component="legend">Wie oft sollen Mails verschickt werden:</FormLabel>
                        <RadioGroup aria-label="email-frequency" name="email-frequency">
                            <FormControlLabel value="Monthly" control={<Radio />} label="Monatlich" checked={this.state.emailFrequency === "Monthly"} onChange={this.onEmailFrequencyChanged} />
                            <FormControlLabel value="Weekly" control={<Radio />} label="Wöchentlich" checked={this.state.emailFrequency === "Weekly"} onChange={this.onEmailFrequencyChanged}/>
                            <FormControlLabel value="Never" control={<Radio />} label="Nie" checked={this.state.emailFrequency === "Never"} onChange={this.onEmailFrequencyChanged}/>
                        </RadioGroup>
                    </FormControl>
                </div>

                <Button onClick={this.onSaveClick}>Speichern</Button>

                <div>
                    <h3>{this.state.result}</h3>
                </div>
            </form>
        )
    }
}
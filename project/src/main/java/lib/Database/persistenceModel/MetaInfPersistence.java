package lib.Database.persistenceModel;

import javax.xml.bind.annotation.*;

/**
 * The Meta inf persistence.
 */
@XmlRootElement(name = "persistence")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class MetaInfPersistence extends BaseXmlElement {

    @XmlElement(name = "persistence-unit", type = PersistenceUnit.class)
    private PersistenceUnit persistenceUnit;

    /**
     * Gets persistence unit.
     *
     * @return the persistence unit
     */
    @XmlTransient
    public PersistenceUnit getPersistenceUnit() {
        return persistenceUnit;
    }

    /**
     * Sets persistence unit.
     *
     * @param persistenceUnit the persistence unit
     */
    public void setPersistenceUnit(PersistenceUnit persistenceUnit) {
        this.persistenceUnit = persistenceUnit;
    }
}

package lib.verticle.server.handlers.channels.dtos;

import java.util.List;

public class NewChannelDTO {
    private String name;
    private boolean isLocked;
    private boolean isPublic;
    private List<Integer> userIds;
    private List<Integer> modIds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public List<Integer> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Integer> userIds) {
        this.userIds = userIds;
    }

    public List<Integer> getModIds() {
        return modIds;
    }

    public void setModIds(List<Integer> modIds) {
        this.modIds = modIds;
    }
}

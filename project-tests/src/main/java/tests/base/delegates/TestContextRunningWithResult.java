package tests.base.delegates;

import io.vertx.core.Future;
import io.vertx.junit5.VertxTestContext;

/**
 * The interface Test context running with result.
 *
 * @param <T> the type parameter
 */
public interface TestContextRunningWithResult<T> {

    /**
     * Run future.
     *
     * @param ctx the ctx
     * @return the future
     * @throws Throwable the throwable
     */
    Future<T> run(VertxTestContext ctx) throws Throwable;
}

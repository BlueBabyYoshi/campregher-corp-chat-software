package lib.verticle.server;

import com.google.inject.Injector;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import lib.annotations.AnnotationSearch;
import lib.annotations.DefaultDefinedAnnotation;
import lib.annotations.HttpHandler;
import lib.services.ConfigService.ConfigService;
import lib.verticle.server.handlers.IHttpHandler;

import javax.naming.OperationNotSupportedException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * The Server verticle, handles creation of the Rest Service aswell as looking for the REST Handlers via Annotationsearch.
 */
public class ServerVerticle extends AbstractVerticle {

    private static final String PORT = "port";
    private Injector injector;
    private ConfigService configService;

    /**
     * Instantiates a new Server verticle.
     *
     * @param injector the injector
     */
    public ServerVerticle(Injector injector) {

        this.injector = injector;
    }

    @Override
    public void start() throws Exception {

        configService = injector.getInstance(ConfigService.class);
        configService.loadProperties();
        var port = Integer.parseInt(configService.getProperty(PORT, "8080"));

        super.start();

        var server = vertx.createHttpServer();
        server.requestHandler(buildRouter())
                .listen(port, r -> {
                    if (r.succeeded()) {
                        System.out.println("Server läuft");
                    } else {
                        System.out.println("nicht.");
                    }
                });
    }

    private Router buildRouter() {
        var router = Router.router(vertx);
        router.route().handler(BodyHandler.create());
        registerPreflightHandler(router);
        registerSessionHandler(router);
        var search = new AnnotationSearch();
        var httpAnnot = new DefaultDefinedAnnotation();
        httpAnnot.setAnnotationType(HttpHandler.class);
        httpAnnot.setFoundHandler((cls, annot) -> httpServiceFound(cls, annot, router));
        search.addAnnotation(httpAnnot);
        search.searchAnnotations();
        return router;
    }


    private void registerSessionHandler(Router router) {
        var store = LocalSessionStore.create(vertx);
        var sessionHandler = SessionHandler.create(store);
        // set session timeout to 30 minutes in milliseconds
        sessionHandler.setSessionTimeout(30 * 60 * 1000);
        router.route().handler(sessionHandler);
    }


    private void registerPreflightHandler(Router router) {
        router.route().handler(CorsHandler.create(".*.")
                .allowedMethod(io.vertx.core.http.HttpMethod.GET)
                .allowedMethod(io.vertx.core.http.HttpMethod.POST)
                .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
                .allowedMethod(HttpMethod.DELETE)
                .allowCredentials(true)
                .allowedHeader("Access-Control-Request-Method")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Headers")
                .allowedHeader("Content-Type"));
    }

    private void httpServiceFound(Class serviceClass, Annotation annotation, Router router) {

        if (annotation instanceof HttpHandler && IHttpHandler.class.isAssignableFrom(serviceClass)) {
            System.out.println("Found handler: " + serviceClass.getSimpleName());
            var castedAnnot = (HttpHandler) annotation;
            Route action = null;
            try {

                action = pickAction(router, castedAnnot);
            } catch (OperationNotSupportedException ex) {
                //do stuff
            }
            if (action != null) {
                try {
                    var type = getCorrectClass(serviceClass, castedAnnot);
                    var inst = createHandlerByClass(type);
                    inst.setVertxInstance(vertx);
                    action.handler(inst::handle);
                } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ex) {
                    //do other stuff
                }

            }
        }
    }

    private Class<?> getCorrectClass(Class<?> serviceClass, HttpHandler annot) {
        if (!annot.interfaceType().equals(void.class) && annot.interfaceType().isInterface() && IHttpHandler.class.isAssignableFrom(annot.interfaceType())) {
            return annot.interfaceType();
        }
        return serviceClass;
    }

    private IHttpHandler createHandlerByClass(Class<?> cls) throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        if (cls.isInterface()) {
            return (IHttpHandler) injector.getInstance(cls);
        }
        Constructor<?> ctor = cls.getConstructor();
        return (IHttpHandler) ctor.newInstance();
    }

    private Route pickAction(Router router, HttpHandler annot) throws OperationNotSupportedException {

        switch (annot.method()) {
            case GET:
                return router.get(annot.endpoint());
            case PUT:
                return router.put(annot.endpoint());
            case POST:
                return router.post(annot.endpoint());
            case DELETE:
                return router.delete(annot.endpoint());
            default:
                throw new OperationNotSupportedException("");
        }
    }

}

package lib.verticle.server.handlers.users;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.AsyncJinq;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.dtos.UserInfoDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import org.jinq.orm.stream.JinqStream;

import java.util.ArrayList;
import java.util.List;


/**
 * The Get all users handler is called by an admin to see all users
 * it sends the information about all users to the frontend
 */
@HttpHandler(endpoint = HandlerEndpoints.Users.GET_ALL_USER, method = HttpMethod.GET, interfaceType = IGetAllUsersHandler.class)
public class GetAllUsersHandler extends AbstractHttpHandler implements IGetAllUsersHandler {

    @Inject
    DatabaseHandler handler;
    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {
        System.out.println("GET AllUsers was called!");
        getUsers().setHandler(r -> {
            if (r.succeeded()) {
                List<User> users = r.result();
                List<UserInfoDTO> userInfoDTOS = new ArrayList<>();
                for (User u : users) {
                    // check if user is allowed to read all users
                    if (u.getUsername().equals(context.session().get("username")) && !u.hasRole("Admin")) {
                        System.out.println("WARNING: unauthorized user wanted to read all users");
                        return;
                    }
                    // only show active users
                    if (u.getActiveUser())
                        userInfoDTOS.add(new UserInfoDTO(u.getUsername(), u.getName(), u.getEmailAddress(), u.hasRole("Admin"), u.hasRole("User"), u.getDepartment().toString(), u.getOffice(), u.getEmailFrequency().toString(), u.getActiveUser()));
                }
                writeResponseToContext(context, userInfoDTOS);
            } else {
                System.out.println("Error all users list");
            }
        });


    }

    private Future<List<User>> getUsers() {
        Promise<List<User>> promise = Promise.promise();
        handler.jinqQuery(User.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<User> stream = r.result();
                //JinqStream<User> usernameStream = stream.where(user -> user.getEmailAddress().equals(email));
                AsyncJinq.asyncToList(stream, getVertx()).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    private void writeResponseToContext(RoutingContext context, List<UserInfoDTO> users) {
        var gson = new Gson();
        var resultAsString = gson.toJson(users);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}




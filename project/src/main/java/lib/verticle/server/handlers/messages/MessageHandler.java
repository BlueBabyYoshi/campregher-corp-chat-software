package lib.verticle.server.handlers.messages;

import com.google.inject.Inject;
import io.vertx.core.AsyncResult;
import io.vertx.ext.web.RoutingContext;
import lib.model.Message;
import lib.services.GsonService.GsonService;
import lib.verticle.server.handlers.AbstractHttpHandler;
import org.jinq.jpa.JPAJinqStream;

import java.util.function.Function;

/**
 * The type Message handler.
 */
public abstract class MessageHandler extends AbstractHttpHandler {

    @Inject
    private GsonService gsonService;

    /**
     * Gets gson service.
     *
     * @return the gson service
     */
    protected GsonService getGsonService() {
        return gsonService;
    }

    /**
     * Handle message completed.
     *
     * @param completedObj the completed obj
     * @param ctx          the ctx
     */
    protected void handleMessageCompleted(AsyncResult<Message> completedObj, RoutingContext ctx) {
        if (completedObj.succeeded()) {
            var res = completedObj.result();
            var stringRes = gsonService.toJson(res);
            ctx.response()
                    .putHeader("Content-Type", "application/json")
                    .end(stringRes);
        }
    }

    /**
     * Gets qry modifier.
     *
     * @return the qry modifier
     */
    protected Function<JPAJinqStream<Message>, JPAJinqStream<Message>> getQryModifier() {

        return r -> r;
    }
}

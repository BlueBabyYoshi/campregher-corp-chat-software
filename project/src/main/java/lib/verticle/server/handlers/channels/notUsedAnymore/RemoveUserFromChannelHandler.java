package lib.verticle.server.handlers.channels.notUsedAnymore;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import lib.model.Channel;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.channels.ChannelHandler;
import lib.verticle.server.handlers.channels.dtos.ChannelUserActionDTO;

/**
 * The type Remove user from channel handler.
 */
//@HttpHandler(endpoint = HandlerEndpoints.Channels.REMOVE_USER, method = HttpMethod.POST, interfaceType = IRemoveUserFromChannelHandler.class)
public class RemoveUserFromChannelHandler extends ChannelHandler implements IRemoveUserFromChannelHandler {

    @Inject
    private DatabaseHandler handler;

    @Override
    public void handle(RoutingContext context) {
        var dto = getGsonService().fromJson(context.getBodyAsString(), ChannelUserActionDTO.class);
        reloadById(Channel.class, dto.getChannelId(), getQryModifier()).setHandler(r -> {
            if (r.succeeded()) {
                var channel = r.result();
                if (!channel.isUserMod(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to remove users from channel");
                    return;
                }
                channel.getChannelMembers().removeIf(user -> user.getId() == dto.getUserId());
                handler.saveOrUpdateObject(Channel.class, channel)
                        .setHandler(end -> handleChannelCompleted(end, context));
            }
        });
    }
}

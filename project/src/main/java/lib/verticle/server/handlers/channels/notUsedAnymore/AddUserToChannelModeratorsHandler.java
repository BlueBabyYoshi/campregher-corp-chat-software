package lib.verticle.server.handlers.channels.notUsedAnymore;

import io.vertx.ext.web.RoutingContext;
import lib.verticle.server.handlers.channels.AbstractAddUserHandler;
import lib.verticle.server.handlers.channels.dtos.ChannelUserActionDTO;

/**
 * The type Add user to channel moderators handler.
 */
//@HttpHandler(endpoint = HandlerEndpoints.Channels.ADD_MODERATOR, method = HttpMethod.POST, interfaceType = IAddUserToChannelModeratorsHandler.class)
public class AddUserToChannelModeratorsHandler extends AbstractAddUserHandler implements IAddUserToChannelModeratorsHandler {

    @Override
    public void handle(RoutingContext context) {
        var dto = getGsonService().fromJson(context.getBodyAsString(), ChannelUserActionDTO.class);
        addUser(dto.getUserId(), dto.getChannelId(), context, (c, u) -> c.getModerator().add(u));
    }
}

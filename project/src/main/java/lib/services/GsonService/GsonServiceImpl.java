package lib.services.GsonService;

import com.google.gson.Gson;

/**
 * The Gson service implementation.
 */
public class GsonServiceImpl implements GsonService {

    private Gson gsonInstance = new Gson();

    @Override
    public <T> String toJson(T obj) {
        return gsonInstance.toJson(obj);
    }

    @Override
    public <T> T fromJson(String json, Class<T> type) {
        return gsonInstance.fromJson(json, type);
    }
}

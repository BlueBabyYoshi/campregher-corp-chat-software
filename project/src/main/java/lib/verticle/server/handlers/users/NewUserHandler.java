package lib.verticle.server.handlers.users;

import com.google.gson.Gson;
import com.google.inject.Inject;
import controller.AuthenticationController;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.DatasourceService;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.User;
import lib.verticle.server.dtos.NewUserDTO;
import lib.verticle.server.dtos.NewUserResultDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

import java.util.List;

/**
 * The  New user handler is called by an admin to register a new user
 * the handler receives all required information about a user and handles her/his registration
 */
@HttpHandler(endpoint = HandlerEndpoints.Users.NEW_USER, method = HttpMethod.POST, interfaceType = INewUserHandler.class)
public class NewUserHandler extends AbstractHttpHandler implements INewUserHandler {

    @Inject
    private IQueryService queryService;
    private AuthenticationController controller;

    @Override
    public void handle(RoutingContext context) {
        DatasourceService datasourceService = new DatasourceService();
        controller = new AuthenticationController(getVertx(), queryService, datasourceService.createDataSource());
        var gson = new Gson();
        NewUserDTO newUserInfo = gson.fromJson(context.getBodyAsString(), NewUserDTO.class);
        NewUserResultDTO resultDTO = new NewUserResultDTO();
        if (newUserInfo != null && !newUserInfo.isAnyValueEmpty()) {
            queryService.getAllUsers().setHandler(allUsers -> {
                if (allUsers.succeeded()) {
                    boolean usernameExists = false;
                    List<User> users = allUsers.result();
                    for (User u : users) {
                        // check if user is allowed to register new users
                        if (u.getUsername().equals(context.session().get("username")) && !u.hasRole("Admin")) {
                            System.out.println("WARNING: unauthorized user wanted to register new user");
                            return;
                        }
                        // check if username already exists
                        if (u.getUsername().trim().equals(newUserInfo.getUsername().trim())) {
                            usernameExists = true;
                        }
                    }
                    if (usernameExists) {
                        resultDTO.setMessage("Benutzername ist schon vergeben");
                        resultDTO.setSuccess(false);
                        writeResponseToContext(context, resultDTO);
                    } else {
                        controller.registerNewUser(newUserInfo).setHandler(res -> {
                            if (res.succeeded()) {
                                resultDTO.setSuccess(true);
                                resultDTO.setMessage("Neuer Benutzer " + res.result().getUsername() + " wurde angelegt");
                                System.out.println(resultDTO.getMessage());
                            } else {
                                resultDTO.setSuccess(false);
                                resultDTO.setMessage("Benutzer " + newUserInfo.getUsername() + " konnte nicht angelegt werden");
                                System.out.println(resultDTO.getMessage());
                            }
                            writeResponseToContext(context, resultDTO);
                        });
                    }
                }
            });
        } else {
            resultDTO.setMessage("Bitte füllen Sie alle Felder aus");
            resultDTO.setSuccess(false);
            writeResponseToContext(context, resultDTO);
        }
    }

    private void writeResponseToContext(RoutingContext context, NewUserResultDTO dto) {
        var gson = new Gson();
        var resultAsString = gson.toJson(dto);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }

}

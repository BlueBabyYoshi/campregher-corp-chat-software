package lib.model.fileUpload;

/**
 * The enum Uploaded file type.
 */
public enum UploadedFileType {
    Picture,
    Document,
    Audio,
    Video
}

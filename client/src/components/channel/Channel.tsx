export interface Channel {
    id: number,
    channelName: string,
    isPublic: boolean
 }
package lib.Database;

/**
 * The type Crud actions.
 */
public class CRUDActions {

    /**
     * The constant SAVE.
     */
    public static final String SAVE = "save";
    /**
     * The constant SAVE_OR_UPDATE.
     */
    public static final String SAVE_OR_UPDATE = "saveOrUpdate";
    /**
     * The constant DELETE.
     */
    public static final String DELETE = "delete";
    /**
     * The constant FORCE_DELETE.
     */
    public static final String FORCE_DELETE = "force_delete";
    /**
     * The constant READ.
     */
    public static final String READ = "read";
    /**
     * The constant READ_LIST.
     */
    public static final String READ_LIST = "readList";
    /**
     * The constant JINQ.
     */
    public static final String JINQ = "jinq";
}

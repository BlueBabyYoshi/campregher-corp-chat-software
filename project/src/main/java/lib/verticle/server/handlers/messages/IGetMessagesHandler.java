package lib.verticle.server.handlers.messages;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Get messages handler.
 */
public interface IGetMessagesHandler extends IHttpHandler {
}

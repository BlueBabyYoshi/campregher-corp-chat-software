package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.AsyncResult;
import io.vertx.ext.web.RoutingContext;
import lib.model.Channel;
import lib.services.GsonService.GsonService;
import lib.verticle.server.handlers.AbstractHttpHandler;
import org.jinq.jpa.JPAJinqStream;

import java.util.function.Function;

/**
 * The type Channel handler.
 */
public abstract class ChannelHandler extends AbstractHttpHandler {

    @Inject
    private GsonService gsonService;

    /**
     * Gets gson service.
     *
     * @return the gson service
     */
    protected GsonService getGsonService() {
        return gsonService;
    }

    /**
     * Handle channel completed.
     *
     * @param completedObj the completed obj
     * @param ctx          the ctx
     */
    protected void handleChannelCompleted(AsyncResult<Channel> completedObj, RoutingContext ctx) {
        if (completedObj.succeeded()) {
            var res = completedObj.result();
            var stringRes = gsonService.toJson(res);
            ctx.response()
                    .putHeader("Content-Type", "application/json")
                    .end(stringRes);
        }
    }

    /**
     * Handle channel setting save completed.
     *
     * @param message the message
     * @param ctx     the ctx
     */
    protected void handleChannelSettingSaveCompleted(String message, RoutingContext ctx) {
        var stringRes = gsonService.toJson(message);
        ctx.response()
                .putHeader("Content-Type", "application/json")
                .end(stringRes);

    }

    /**
     * Gets qry modifier.
     *
     * @return the qry modifier
     */
    protected Function<JPAJinqStream<Channel>, JPAJinqStream<Channel>> getQryModifier() {

        return r -> r;
    }
}

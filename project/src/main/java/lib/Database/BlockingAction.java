package lib.Database;


/**
 * The interface Blocking action.
 *
 * @param <T> the type parameter
 */
public interface BlockingAction<T> {

    T function();
}

package lib.Database.datasources;

import javax.sql.DataSource;

/**
 * The interface Data source functionality implementor.
 */
public interface DataSourceFunctionalityImplementor {

    /**
     * Create data source.
     *
     * @return the data source
     */
    DataSource create();

    /**
     * Sets url.
     *
     * @param source the source
     * @param url    the url
     */
    void setUrl(DataSource source, String url);

    /**
     * Sets user.
     *
     * @param source the source
     * @param user   the user
     */
    void setUser(DataSource source, String user);

    /**
     * Sets password.
     *
     * @param source   the source
     * @param password the password
     */
    void setPassword(DataSource source, String password);
}

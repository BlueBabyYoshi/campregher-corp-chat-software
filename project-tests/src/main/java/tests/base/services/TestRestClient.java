package tests.base.services;

import lib.services.GsonService.GsonService;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

/**
 * The type Test rest client.
 */
public class TestRestClient {

    private static final MediaType JSON
            = MediaType.get("application/json; charset=utf-8");

    private String baseUrl;
    private GsonService gson;

    /**
     * Instantiates a new Test rest client.
     *
     * @param baseUrl the base url
     * @param gson    the gson
     */
    public TestRestClient(String baseUrl, GsonService gson) {
        this.baseUrl = baseUrl;
        this.gson = gson;
    }

    /**
     * Get collection.
     *
     * @param <T>             the type parameter
     * @param endpoint        the endpoint
     * @param entity          the entity
     * @param collectionClass the collection class
     * @return the collection
     * @throws Throwable the throwable
     */
    public <T> Collection<T> get(String endpoint, T entity, Class<Collection<T>> collectionClass) throws Throwable {
        var client = new OkHttpClient();
        RequestBody body = getBody(entity);
        Request req = new Request.Builder()
                .url(baseUrl + endpoint)
                .post(body)
                .build();
        try (var response = client.newCall(req).execute()) {
            var jsonRes = response.body().string();
            return gson.fromJson(jsonRes, collectionClass);
        }
    }

    /**
     * Get t.
     *
     * @param <T>      the type parameter
     * @param endpoint the endpoint
     * @param cls      the cls
     * @return the t
     * @throws Throwable the throwable
     */
    public <T> T get(String endpoint, Class<T> cls) throws Throwable {
        var client = new OkHttpClient();
        Request req = new Request.Builder()
                .url(baseUrl + endpoint)
                .get()
                .build();
        try (var response = client.newCall(req).execute()) {
            var jsonRes = response.body().string();
            return gson.fromJson(jsonRes, cls);
        }
    }

    /**
     * Delete.
     *
     * @param <T>      the type parameter
     * @param <R>      the type parameter
     * @param endpoint the endpoint
     * @param entity   the entity
     * @throws Throwable the throwable
     */
    public <T, R> void delete(String endpoint, R entity) throws Throwable {
        var client = new OkHttpClient();
        RequestBody body = getBody(entity);
        Request req = new Request.Builder()
                .url(baseUrl + endpoint)
                .delete(body)
                .build();
        var response = client.newCall(req).execute();
        response.close();
    }

    /**
     * Post t.
     *
     * @param <T>      the type parameter
     * @param <R>      the type parameter
     * @param endpoint the endpoint
     * @param entity   the entity
     * @param cls      the cls
     * @return the t
     * @throws Throwable the throwable
     */
    public <T, R> T post(String endpoint, R entity, Class<T> cls) throws Throwable {
        var client = new OkHttpClient.Builder()
                .callTimeout(60, TimeUnit.SECONDS)
                .build();
        RequestBody body = getBody(entity);
        Request req = new Request.Builder()
                .url(baseUrl + endpoint)
                .post(body)
                .build();

        try (var response = client.newCall(req).execute()) {
            var jsonRes = response.body().string();
            return gson.fromJson(jsonRes, cls);
        }
    }

    private <T> RequestBody getBody(T entity) {
        return RequestBody.create(gson.toJson(entity), JSON);
    }
}

package lib.Database;

import io.vertx.core.eventbus.DeliveryOptions;
import lib.verticle.eventbus.codecs.MessageCodecNames;

/**
 * The type Persistence option, important for the eventbus, this gets attached to a message and mages the eventbus use the custom codec to encode and decode the sent message/Opject.
 */
public class PersistenceOption {

    /**
     * Create crud option delivery options.
     *
     * @param action     the action
     * @param entityType the entity type
     * @return the delivery options
     */
    public static DeliveryOptions createCRUDOption(String action, Class<?> entityType) {
        DeliveryOptions opt = new DeliveryOptions();
        opt.addHeader("action", action)
                .setCodecName(MessageCodecNames.PERSISTENCE)
                .addHeader("entityType", entityType.getName());
        return opt;
    }
}

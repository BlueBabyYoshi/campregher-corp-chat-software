package lib.model.fileUpload;


import lib.model.BaseModel;

import javax.persistence.*;

@Entity
@Table(name = "uploadedfilecontent")
@Access(AccessType.PROPERTY)
public class UploadedFileContent extends BaseModel {


    private UploadedFile file;

    private byte[] data;

    public UploadedFileContent() {
    }

    public UploadedFileContent(UploadedFile file, byte[] data) {
        this.file = file;
        this.data = data;
    }

    @OneToOne
    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    @Column(length = 16777216)
    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}

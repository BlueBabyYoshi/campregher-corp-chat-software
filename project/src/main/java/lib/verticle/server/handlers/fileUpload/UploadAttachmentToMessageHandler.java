package lib.verticle.server.handlers.fileUpload;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.model.Message;
import lib.services.GsonService.GsonService;
import lib.services.LoggingService.ILoggingService;
import lib.services.fileupload.IFileUploadService;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.fileUpload.dtos.FileMessageDTO;

import java.io.IOException;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.util.Base64;

/**
 * Handler that gets a channelId, FileName and fileData via Rest Call, creates a Message and saves the attatchment into the Database
 */
@HttpHandler(interfaceType = IUploadAttachmentToMessageHandler.class, method = HttpMethod.POST, endpoint = HandlerEndpoints.FileUpload.ADD)
public class UploadAttachmentToMessageHandler extends AbstractHttpHandler implements IUploadAttachmentToMessageHandler {

    @Inject
    private IFileUploadService fileUploadService;
    @Inject
    private IQueryService queryService;
    @Inject
    private GsonService gsonService;
    @Inject
    private ILoggingService loggingService;

    @Override
    public void handle(RoutingContext context) {
        var dto = readDTO(context);
        createMessage(context, dto.getChannelId(), dto.getName()).setHandler(messageCreate -> {
            if (messageCreate.succeeded()) {
                try {
                    var splitBase64 = dto.getData().split(",");
                    byte[] fileData = Base64.getDecoder().decode(splitBase64[1]);
                    fileUploadService.saveMessageAttachment(messageCreate.result(), dto.getName(), fileData);
                    loggingService.logInfo("MessageAttatchment " + dto.getName() + " got Saved for Channel with id: " + dto.getChannelId() + "from user " + context.session().get("username"));
                    writeResponseToContext(context, "Attachment " + dto.getName() + " successfully saved!");
                } catch (URISyntaxException | IOException ex) {
                    ex.printStackTrace();
                    loggingService.logError("Saving of MessageAttatchment failed!", ex);
                    context.fail(ex);
                }
            }
        });
    }

    private Future<Message> createMessage(RoutingContext context, int channelId, String messagetext) {
        Promise<Message> messagePromise = Promise.promise();
        Message mes = new Message(messagetext);
        mes.setCreateDate(LocalDateTime.now());
        queryService.getUserByUsername(context.session().get("username"), getVertx()).setHandler(r -> {
            if (r.succeeded()) {
                mes.setFromUser(r.result());
                var channel = new Channel(channelId);
                handler.readObject(Channel.class, channel).setHandler(readChannel -> {
                    if (readChannel.succeeded()) {
                        mes.setInChannel(readChannel.result());
                        handler.saveObject(Message.class, mes).setHandler(messageSave -> {
                            if (messageSave.succeeded()) {
                                messagePromise.complete(messageSave.result());
                            }
                        });
                    }
                });
            }
        });
        return messagePromise.future();
    }

    private void writeResponseToContext(RoutingContext context, String message) {
        Gson gson = new Gson();
        String resultString = gson.toJson(message);
        System.out.println(resultString);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultString);
    }


    private FileMessageDTO readDTO(RoutingContext context) {
        return gsonService.fromJson(context.getBodyAsString(), FileMessageDTO.class);
    }
}

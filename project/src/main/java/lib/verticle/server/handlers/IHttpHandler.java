package lib.verticle.server.handlers;

import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;

/**
 * The interface Http handler.
 */
public interface IHttpHandler {

    /**
     * Handle.
     *
     * @param context the context
     */
    void handle(RoutingContext context);

    /**
     * Sets vertx instance.
     *
     * @param vertx the vertx
     */
    void setVertxInstance(Vertx vertx);
}

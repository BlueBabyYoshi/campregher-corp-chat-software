package lib.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * The type Channel.
 */
@Entity
@Table(name = "Channels")
@Access(AccessType.PROPERTY)
public class Channel extends BaseModel {

    private String name;

    private Boolean isPublic;

    private Set<User> moderator = new HashSet<>();

    private boolean isLocked;

    private Set<User> channelMembers = new HashSet<>();

    private Channel parentChannel;

    public Channel() {
    }

    public Channel(String name, Boolean isPublic, Set<User> channelMembers, boolean isLocked) {
        this.name = name;
        this.isPublic = isPublic;
        this.channelMembers = channelMembers;
        this.isLocked = isLocked;
    }

    public Channel(String name, Boolean isPublic, Set<User> moderator, boolean isLocked, Set<User> channelMembers, Channel parentChannel) {
        this.name = name;
        this.isPublic = isPublic;
        this.moderator = moderator;
        this.isLocked = isLocked;
        this.channelMembers = channelMembers;
        this.parentChannel = parentChannel;
    }

    public Channel(int channelid) {
        this.id = channelid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getPublic() {
        return isPublic;
    }

    public void setPublic(Boolean aPublic) {
        isPublic = aPublic;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Channel_Moderators",
            joinColumns = @JoinColumn(
                    name = "CHANNEL_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    public Set<User> getModerator() {
        return moderator;
    }

    public void setModerator(Set<User> moderator) {
        this.moderator = moderator;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public Channel getParentChannel() {
        return parentChannel;
    }

    public void setParentChannel(Channel parentChannel) {
        this.parentChannel = parentChannel;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "Channel_Users",
            joinColumns = @JoinColumn(
                    name = "CHANNEL_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    public Set<User> getChannelMembers() {
        return channelMembers;
    }

    public void setChannelMembers(Set<User> channelMembers) {
        this.channelMembers = channelMembers;
    }

    public boolean isUserMod(String username) {
        for (User u : moderator) {
            if (u.getUsername().equals(username)) return true;
        }
        return false;
    }
}

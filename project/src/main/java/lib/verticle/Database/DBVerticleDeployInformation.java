package lib.verticle.Database;

import io.vertx.core.Verticle;
import lib.verticle.deploy.IVerticleDeployInformation;

/**
 * The DBVerticleDeployInformation needed for the Verticle Deployment.
 */
public class DBVerticleDeployInformation implements IVerticleDeployInformation {

    private final DBVerticle instance = new DBVerticle();

    @Override
    public Verticle getInstance() {
        return instance;
    }

    @Override
    public String getName() {
        return "database";
    }
}

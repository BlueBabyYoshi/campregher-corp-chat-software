import * as React from "react";

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

export default function FormDialog() {
  const [open, setOpen] = React.useState(false);

  const [eMail, setEmail] = React.useState("");

  const [answer, setAnswer] = React.useState("");


  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
    setEmail("");
  };


  const handleSubmit = async () => {
    try {    
      let response = await fetch("http://localhost:8080/resetPassword", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: eMail,
        mode: "cors",
        credentials: "include"
      });
      let jsonResult = await response.json();
      setAnswer(jsonResult);
    }
    catch(e) {
    console.log("Reset Password Error");
    }  
  };

  

  return (
    <div>
      <Button onClick={handleClickOpen}>
        Passwort vergessen
      </Button>
      <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Neues Passwort zusenden</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Bitte tragen Sie ihre E-Mail Adresse ein. Das neue Passwort wird Ihnen in kürze zugeschickt.
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="e-mail"
            label="E-Mail Adresse"
            type="email"
            value={eMail}
            fullWidth
            onChange={(e) => setEmail(e.target.value)}
            
          />
        </DialogContent>
        <div>
          <h3>{answer}</h3>
        </div>
        <DialogActions>
        <Button onClick={handleClose} color="primary">
            Fenster schließen
          </Button>
          <Button onClick={handleClose} color="primary">
            Abbrechen
          </Button>
          <Button onClick={handleSubmit} color="primary">
            Neues Passwort senden
          </Button>
        </DialogActions>

      </Dialog>
    </div>
  );
}

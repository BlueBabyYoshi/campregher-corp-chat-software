package lib.annotations;

/**
 * The Default defined annotation.
 */
public class DefaultDefinedAnnotation implements IDefinedAnnotation {

    private Class type;
    private IAnnotationFoundHandler handler;

    @Override
    public Class getAnnotationType() {
        return type;
    }

    @Override
    public void setAnnotationType(Class type) {
        this.type = type;
    }

    @Override
    public IAnnotationFoundHandler getFoundHandler() {
        return handler;
    }

    @Override
    public void setFoundHandler(IAnnotationFoundHandler handler) {
        this.handler = handler;
    }
}

export interface AuthorizationDTO {
    sessionId: string;
    role: string;
}
package lib.model.fileUpload;

/**
 * The enum Uploaded file extension.
 */
public enum UploadedFileExtension {
    PDF,
    TXT,
    JPG,
    JPEG,
    PNG,
    MP3,
    MP4
}

package lib.Database.services;

import com.google.common.io.Resources;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import lib.Database.persistenceModel.MetaInfPersistence;
import lib.Database.persistenceModel.PersistenceProperty;
import lib.Database.persistenceModel.PersistenceUnit;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * The Persistence reader service implementation.
 */
public class PersistenceReaderServiceImpl implements PersistenceReaderService {

    public MetaInfPersistence readPersistence() throws IOException, JAXBException {

        var resource = Thread.currentThread().getContextClassLoader().getResource("META-INF/persistence.xml");
        if (resource != null) {
            var xmlString = Resources.toString(resource, StandardCharsets.UTF_8);
            var stream = createAndConfigureSerializer();
            var xstreamObject = stream.fromXML(xmlString);
            if (!MetaInfPersistence.class.isAssignableFrom(xstreamObject.getClass())) {
                throw new IllegalArgumentException("persistence info could not be read from persistence xml.");
            }
            return (MetaInfPersistence) xstreamObject;
        }
        return null;
    }

    private XStream createAndConfigureSerializer() {
        var stream = new XStream(new StaxDriver());
        stream.alias("persistence", MetaInfPersistence.class);
        stream.alias("property", PersistenceProperty.class);
        stream.alias("persistence-unit", PersistenceUnit.class);
        stream.aliasField("persistence-unit", MetaInfPersistence.class, "persistenceUnit");
        stream.useAttributeFor(PersistenceProperty.class, "value");
        stream.useAttributeFor(PersistenceProperty.class, "name");
        stream.useAttributeFor(PersistenceUnit.class, "name");
        stream.aliasField("name", PersistenceUnit.class, "name");
        stream.ignoreUnknownElements();
        return stream;
    }
}

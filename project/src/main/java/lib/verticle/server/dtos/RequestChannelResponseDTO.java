package lib.verticle.server.dtos;

public class RequestChannelResponseDTO {
    //test only with text same in Frontend with DTO
    private int id;
    private String channelName;
    private boolean isPublic;


    public RequestChannelResponseDTO(int id, String channelName, boolean isPublic) {
        this.id = id;
        this.channelName = channelName;
        this.isPublic = isPublic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }
}

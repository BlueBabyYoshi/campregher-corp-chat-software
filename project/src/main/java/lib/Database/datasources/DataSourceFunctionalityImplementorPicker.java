package lib.Database.datasources;

import lib.Database.datasources.implementors.MsSqlDataSourceImplementor;
import lib.Database.datasources.implementors.MySqlDatasourceImplementor;
import lib.Database.datasources.implementors.PostgresDataSourceImplementor;

/**
 * The Data source functionality implementor picker implementation.
 */
public class DataSourceFunctionalityImplementorPicker {

    /**
     * Pick data source functionality implementor depending on found Datasource config in persistence.xml.
     *
     * @param dbKind the db kind
     * @return the data source functionality implementor
     */
    public static DataSourceFunctionalityImplementor pickImplementor(String dbKind) {

        if (dbKind.equals(DatabaseKindsConstants.MYSQL)) {
            return new MySqlDatasourceImplementor();
        } else if (dbKind.equals(DatabaseKindsConstants.MSSQL)) {
            return new MsSqlDataSourceImplementor();
        } else if (dbKind.equals(DatabaseKindsConstants.POSTGRES)) {
            return new PostgresDataSourceImplementor();
        }
        throw new UnsupportedOperationException("Database kind: " + dbKind + " does not yet have a corresponding implementor");
    }
}

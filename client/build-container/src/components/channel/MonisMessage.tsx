import * as React from "react";

import { Container } from "@material-ui/core";
import Typography from "material-ui/styles/typography";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { Component, MouseEvent } from 'react';

import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import SendIcon from "@material-ui/icons/Send";
import AttachFileSharpIcon from "@material-ui/icons/AttachFileSharp";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import SettingsIcon from "@material-ui/icons/Settings";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import Avatar from "@material-ui/core/Avatar";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import InputLabel from "@material-ui/core/InputLabel";
/* eslint-disable no-use-before-define */
import Autocomplete from "@material-ui/lab/Autocomplete";

//Interfaces 
import { Message } from "./Message";
import { Channel } from "./Channel";
import { MessageProperties } from "./MessageProperties";
import { MessageState } from "./MessageState";

import { MessageDTO } from "../../Dtos/MessageDTO";
import { ChannelDTO } from '../../Dtos/ChannelDTO';
import { MessageResultDTO } from "../../Dtos/MessageResultDTO";

//API
const API = "http://localhost:8080/allMessages";
const APISetMessages = "http://localhost:8080/setMessage";
const APIDeleteMessages = "http://localhost:8080/deleteMessage";

const APIChannels = "http://localhost:8080/channels"

//Websocket 
const client = new W3CWebSocket('ws://127.0.0.1:8887');

var templateMessage: Message = { text: "Test" };
var channelsTemp2: Channel[] =
  [
    { name: "Channel 1", id: 1, private: true },
    { name: "Channel 2", id: 2, private: false },
    { name: "Channel 3", id: 3, private: false }
  ]; //must be reloaded from the backend 

export class MonisMessages extends React.Component<MessageProperties, MessageState> {

  //for received messages from Backend  
  public state: MessageState = {
    channels: channelsTemp2,
    channelsSelected: [],
    channelsTemp: [],
    messages: [],
    messageToSend: "",
    editDialogOpen: false,
    editMessage: templateMessage, //store temporary message
    screenWidth:  window.innerWidth
  }

  private messageDTO: MessageDTO;
  private messageResultDTO: MessageResultDTO;

  constructor(props: MessageProperties) {
    super(props);
    //backend to front 
    this.showMessages = this.showMessages.bind(this);
    this.onGetMessage = this.onGetMessage.bind(this);

    this.showChannels = this.showChannels.bind(this);
    this.onGetChannels = this.onGetChannels.bind(this);

    //front to backend 


    //Event Handler 
    this.onButtonClickSendMessages = this.onButtonClickSendMessages.bind(this);
    this.onMessageEntered = this.onMessageEntered.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.chatConversation = this.chatConversation.bind(this)
    this.inputMessage = this.inputMessage.bind(this);
    this.handleDialogClick = this.handleDialogClick.bind(this);
    this.closeDialogClick = this.closeDialogClick.bind(this);
    this.newChannelSet = this.newChannelSet.bind(this);
    this.listChannels = this.listChannels.bind(this);
    this.selectOnlyPrivateChannels = this.selectOnlyPrivateChannels.bind(this);
    this.selectAllChannels = this.selectAllChannels.bind(this);
  }

  //********************************************************************************************* Backend To Frontend **************************************************************//

  //automatically call 
  componentDidMount() {
    //https://blog.logrocket.com/websockets-tutorial-how-to-go-real-time-with-node-and-react-8e4693fbf843/ --> for websocket handshake with backend
    client.onopen = () => {
      console.log('WebSocket Client Connected');
    };
    client.onmessage = (message) => {
      console.log(message);
    };
    this.showMessages();
    this.showChannels();
  }

  //receive message from backend
  public async showMessages(): Promise<void> {
    try {
      let response = await fetch(API, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: "",
        mode: "cors",
        credentials: "include"
      });
      let jsonResponse = await response.json(); //parsed JSON object
      console.log(jsonResponse);
      let jsonObject: MessageDTO[] = JSON.parse(JSON.stringify(jsonResponse));
      console.log("JsonObject" + jsonObject);
      //jsonObject.forEach(x => console.log("JSonOBject + text " + x.text));

      this.onGetMessage(jsonObject);

      console.log("this state " + this.state.messages);
    } catch (e) {
      console.log(e);
    }
  }

  public onGetMessage(dto: MessageDTO[]): void {
    this.setState({
      ...this.state,
      messages: dto
    });
  }

  //receive channels from backend 
  public async showChannels(): Promise<void> {
    try {
      let response = await fetch(APIChannels, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: "",
        mode: "cors",
        credentials: "include"
      });
      let jsonResponse = await response.json(); //parsed JSON object
      console.log(jsonResponse);
      let jsonObject: ChannelDTO[] = JSON.parse(JSON.stringify(jsonResponse));
      console.log("JsonObject" + jsonObject);
      //jsonObject.forEach(x => console.log("JSonOBject + text " + x.text));

      this.onGetChannels(jsonObject);

      console.log("this state channels" + this.state.channels);
    } catch (e) {
      console.log(e);
    }
  }

  public onGetChannels(dto: ChannelDTO[]): void {
    this.setState({
      ...this.state,
      channelsTemp: dto
    });
  }

  //************************************ messages from Frontend To Backend *******************************************************************//

  //send messages to backend
  public async onButtonClickSendMessages(): Promise<void> {
    this.messageResultDTO = {
      text: this.state.messageToSend,
      //toUser: ""
    };
    var stringVal = JSON.stringify({
      text: this.state.messageToSend
    });
    try {
      let response = await fetch(APISetMessages, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(this.messageResultDTO),
        mode: "cors",
        credentials: "include"
      });
      console.log(JSON.stringify(this.messageResultDTO))
    } catch (e) {
      this.setState({
        ...this.state,
        messageToSend: ""
      });
      console.log(e);
    }
  }

  public onMessageEntered(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({
      ...this.state,
      messageToSend: event.target.value
    });
  }

  //************************************ frontend *******************************************************************//
  //handeClick 
  public async handleClick(messagetext: String): Promise<void> {
    //setOpen(true);
    console.log("Button angeklickt");
    console.log(messagetext);

    var stringVal = JSON.stringify({
      text: messagetext
    });
    try {
      let response = await fetch(APIDeleteMessages, {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(this.messageDTO),
        mode: "cors",
        credentials: "include"
      });
    } catch (e) {
      this.setState({
        ...this.state,
        messageToSend: ""
      });
      console.log(e);
    }

    //event.preventDefault(); --> wirft error
    //alert(event.bubbles)
    //alert(event.currentTarget.tagName); // alerts BUTTON
  }


  /* ----------------------------------
  This is to list all messages
  ------------------------------------- */

  //open dialog with !!message!! to edit 
  public handleDialogClick(obj: Message) {
    this.setState({
      ...this.state,
      editDialogOpen: true,
      editMessage: obj
    });
  }

  //close dialog button for edit
  public closeDialogClick() {
    this.setState({
      ...this.state,
      editDialogOpen: false,
    });
  }

  //function to list all messages 
  chatConversation = () => {
    var chatBubbles = this.state.messages.map((obj, i = 0) => (
      <div
        style={{
          justifyContent: 'flex-start',
          display: "flex",
        }}
        key={i}
      >

        <div>
          <Avatar src="/profile.jpg" />
        </div>

        <div
          key={i++}
          style={{
            border: "0.5px solid black",
            background: "rgb(176, 196, 222)",
            borderRadius: "10px",
            margin: "5px",
            padding: "10px",
            display: "inline-block",
            maxWidth: "60%"
          }}
        >

          <div>
            {obj.text}
            <Button
              variant="outlined"
              color="primary"
              onClick={() => this.handleDialogClick(obj)} //save obj as a temporary Message for edit in dialog 
            >
              Edit
            </Button>
            <Button
              variant="outlined"
              color="primary"
            //onClick={}
            >
              Delete
            </Button>
          </div>

          <div style={{ textAlign: "end" }}>
          </div>
        </div>
      </div>
    ))

    return (
      <div>
        <Dialog open={this.state.editDialogOpen}>
          <DialogContent>
            Bearbeiten sie folgenden Text
          <TextField
              defaultValue={this.state.editMessage.text}
              //value --> set a value and later replace the old message with this value 
              onChange={this.onMessageEntered}
              rows={3}
              multiline
              aria-label="maximum height"
              placeholder="Maximum 4 rows"
              variant="outlined"
              fullWidth={true}
            />
            <Button
              variant="outlined"
              color="primary"
            //onClick={this.closeDialogClick}
            >
              Save
            </Button>
            <Button
              variant="outlined"
              color="primary"
              onClick={this.closeDialogClick}
            >Close</Button>
          </DialogContent>
        </Dialog>
        {chatBubbles}
      </div>
    );
  }

  public newChannelSet(id: String) { //handover ID 
    let tempMessages: Message[] = [templateMessage, templateMessage, templateMessage]; //should be loaded from backend 

    this.setState({
      ...this.state,
      messages: tempMessages //changes states 
    });
  }


  /* ----------------------------------
  This is to create a new message
  ------------------------------------- */

  inputMessage = () => {
    return (
      <div style={{ paddingTop: "20px" }}>
        <div style={{
          display: "flex",
          alignItems: "flex-start"
        }}>
          <TextField
            value={this.state.messageToSend}
            onChange={this.onMessageEntered}
            rows={3}
            multiline
            aria-label="maximum height"
            placeholder="Maximum 4 rows"
            defaultValue="Nachricht"
            variant="outlined"
            fullWidth={true}
          />
          <IconButton aria-label="delete" color="primary" onClick={this.onButtonClickSendMessages}>
            <SendIcon />
          </IconButton>
        </div>
      </div>
    );
  };

  /* ----------------------------------
  To list all possible channels 
  ------------------------------------- */
  selectOnlyPrivateChannels() {
    this.setState({
      ...this.state,
      channelsSelected: this.state.channels.filter(x => x.private)
    });
  }

  selectAllChannels() {
    this.setState({
      ...this.state,
      channelsSelected: this.state.channels
    });
  }

  listChannels = () => {
    const listChannels = this.state.channelsSelected.map((obj, i = 0) => (
      <ListItem button>
        <ListItemText primary={obj.name} onClick={() => this.newChannelSet("temp")} />
      </ListItem>
    ));

    return (
      <div arial-label="text formatting">
        <div style={{ textAlign: "center", padding: "10px" }}>
          <ToggleButtonGroup
            exclusive
            color="prigmary"
            aria-label="outlined primary button group"
          >
            <ToggleButton value="all" aria-label="left aligned" onClick={() => this.selectAllChannels()} >
              All
            </ToggleButton>
            <ToggleButton value="private" aria-label="left aligned" onClick={() => this.selectOnlyPrivateChannels()} >
              Private
            </ToggleButton>
          </ToggleButtonGroup>
        </div>
        <List component="nav" aria-label="secondary mailbox folders">
          {listChannels}
        </List>
      </div>
    );
  };

  render() {
    console.log("Messages section");
    return (
        <div>
          <div style={{
            display: "flex",
            justifyContent: "center",
            minHeight: "80vh"
          }}>

            <div style={{ flexGrow: 1 }}>
              {this.listChannels()}
            </div>

            <div style={{
              flexGrow: 8,
              padding: "20px",
              display: "block",
              maxHeight: "80vh",
              overflowX: "hidden",
              scrollBehavior: "smooth"
            }}>
              {this.chatConversation()}
              {this.inputMessage()}
              {this.state.channelsTemp.length}
            </div>

          </div>
          {this.state.screenWidth}
        </div>
    );
  }
}
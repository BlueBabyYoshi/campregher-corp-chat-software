package lib.model.fileUpload;

import lib.model.BaseModel;
import lib.model.Message;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * The type Uploaded file.
 */
@Entity
@Table(name = "UploadedFiles")
@Access(AccessType.PROPERTY)
public class UploadedFile extends BaseModel {

    private Message message;

    private String fileName;

    private UploadedFileType fileType;

    private UploadedFileExtension fileExtension;

    private String relativePath;

    private LocalDateTime uploadDateTime;

    private UploadedFileContent fileContent;

    public UploadedFile() {
    }

    public UploadedFile(String fileName, UploadedFileType fileType, UploadedFileExtension fileExtension, String relativePath, LocalDateTime uploadDateTime, UploadedFileContent fileContent) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileExtension = fileExtension;
        this.relativePath = relativePath;
        this.uploadDateTime = uploadDateTime;
        this.fileContent = fileContent;
    }


    @OneToOne(cascade = CascadeType.ALL)
    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public UploadedFileType getFileType() {
        return fileType;
    }

    public void setFileType(UploadedFileType fileType) {
        this.fileType = fileType;
    }

    public UploadedFileExtension getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(UploadedFileExtension fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getRelativePath() {
        return relativePath;
    }

    public void setRelativePath(String relativePath) {
        this.relativePath = relativePath;
    }

    public LocalDateTime getUploadDateTime() {
        return uploadDateTime;
    }

    public void setUploadDateTime(LocalDateTime uploadDateTime) {
        this.uploadDateTime = uploadDateTime;
    }

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    public UploadedFileContent getFileContent() {
        return fileContent;
    }

    public void setFileContent(UploadedFileContent fileContent) {
        this.fileContent = fileContent;
    }
}

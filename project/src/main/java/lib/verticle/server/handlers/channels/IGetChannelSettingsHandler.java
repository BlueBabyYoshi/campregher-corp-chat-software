package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Get channel settings handler.
 */
public interface IGetChannelSettingsHandler extends IHttpHandler {
}

package lib.verticle.server.handlers.fileUpload;

import com.google.inject.Inject;
import io.vertx.core.AsyncResult;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.AsyncJinq;
import lib.annotations.HttpHandler;
import lib.model.Message;
import lib.services.GsonService.GsonService;
import lib.services.fileupload.IFileUploadService;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.fileUpload.dtos.AttachmentMessageDTO;
import lib.verticle.server.handlers.fileUpload.dtos.MessageIdDTO;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * The GetMessageAttachmentHandler handler gets messageId via Rest Call and retrieves the Data via FielUploadService fomr Directory or Database.
 */
@HttpHandler(endpoint = HandlerEndpoints.FileUpload.GET, method = HttpMethod.POST, interfaceType = IGetMessageAttachmentHandler.class)
public class GetMessageAttachmentHandler extends AbstractHttpHandler implements IGetMessageAttachmentHandler {

    @Inject
    private GsonService gsonService;

    @Inject
    private IFileUploadService fileUploadService;

    @Override
    public void handle(RoutingContext context) {
        var msgIdDTO = readDTO(context);
        var msgId = msgIdDTO.getMessageId();
        handler.jinqQuery(Message.class).setHandler(qry -> {
            if (qry.succeeded()) {
                AsyncJinq.asyncFirstOrDefault(qry.result().where(r -> r.getId() == msgId), getVertx()).setHandler(qryResult -> {
                    try {
                        fileUploadService.readFile(qryResult.result(), true).setHandler(fileByteResp -> answerToContext(context, fileByteResp));
                    } catch (IOException | URISyntaxException ex) {
                        ex.printStackTrace();
                    }
                });
            }
        });
    }

    private void answerToContext(RoutingContext context, AsyncResult<byte[]> res) {
        context.response().end(gsonService.toJson(createRespDto(res)));
    }

    private AttachmentMessageDTO createRespDto(AsyncResult<byte[]> res) {
        var respDto = new AttachmentMessageDTO();
        if (res.succeeded()) {
            respDto.setAttachmentExists(true);
            respDto.setData(res.result());
        } else {
            respDto.setAttachmentExists(false);
            respDto.setData(new byte[0]);
        }
        return respDto;
    }

    private MessageIdDTO readDTO(RoutingContext context) {
        return gsonService.fromJson(context.getBodyAsString(), MessageIdDTO.class);
    }
}

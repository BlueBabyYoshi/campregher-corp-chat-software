package lib.verticle.server.handlers.messages;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.model.Message;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.PushMessageService;
import lib.verticle.server.dtos.MessageNewMessageDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

import java.time.LocalDateTime;

/**
 * The SetMessageHandler gets ChannelId, and messagetext via Rest Call, we create a new Message, retrieve the User and Channel via ID, set those on the Message and save it
 */
@HttpHandler(endpoint = HandlerEndpoints.Messages.SET_MESSAGE, method = HttpMethod.POST, interfaceType = ISetMessageHandler.class)
public class SetMessageHandler extends AbstractHttpHandler implements ISetMessageHandler {

    @Inject
    private DatabaseHandler handler;
    @Inject
    private IQueryService queryService;
    @Inject
    private PushMessageService pushMessageService;

    @Override
    public void handle(RoutingContext context) {

        System.out.println("POST setMessage was called!");
        Gson gson = new Gson();
        MessageNewMessageDTO messageDTO = gson.fromJson(context.getBodyAsString(), MessageNewMessageDTO.class);
        System.out.println(messageDTO);
        if (messageDTO == null) {
            System.out.println("Message was empty");
            context.fail(new Exception("Message was empty"));
        } else {
            Message mes = new Message(messageDTO.getText());
            mes.setCreateDate(LocalDateTime.now());
            queryService.getUserByUsername(context.session().get("username"), getVertx()).setHandler(r -> {
                if (r.succeeded()) {
                    mes.setFromUser(r.result());
                    var channel = new Channel(messageDTO.getChannelid());
                    handler.readObject(Channel.class, channel).setHandler(readChannel -> {
                        if (readChannel.succeeded()) {
                            mes.setInChannel(readChannel.result());
                            // if in messageDTO mes.setFromUser(messageDTO.getFromUser());
                            handler.saveObject(Message.class, mes);
                            pushMessageService.pushMessage(mes.getText(), mes.getInChannel().getName());
                            System.out.println("next message in line " + pushMessageService.getNextMessage());
                            writeResponseToContext(context, "the message with '" + mes.getText() + "in channel" + mes.getInChannel().getId() + "' was created at " + mes.getCreateDate());
                        }
                    });
                }
            });
        }
    }

    private void writeResponseToContext(RoutingContext context, String message) {
        Gson gson = new Gson();
        String resultString = gson.toJson(message);
        System.out.println(resultString);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultString);
    }
}

package lib.verticle.server.handlers.messages;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Set message handler.
 */
public interface ISetMessageHandler extends IHttpHandler {
}

package lib.verticle.server.handlers.users;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Get all users handler.
 */
public interface IGetAllUsersHandler extends IHttpHandler {
}

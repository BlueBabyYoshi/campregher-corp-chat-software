package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface New channel handler.
 */
public interface INewChannelHandler extends IHttpHandler {
}

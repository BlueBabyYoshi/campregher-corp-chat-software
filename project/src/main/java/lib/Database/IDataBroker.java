package lib.Database;

//import org.jinq.jpa.JPAJinqStream;

import org.jinq.jpa.JPAJinqStream;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * The interface Data broker.
 */
public interface IDataBroker {

    /**
     * Apply.
     *
     * @param em the em
     */
    void apply(EntityManager em);

    /**
     * Save or update t.
     *
     * @param <T> the type parameter
     * @param t   the t
     * @return the t
     */
    <T> T saveOrUpdate(T t);

    /**
     * Save or update.
     *
     * @param <T> the type parameter
     * @param t   the t
     * @param em  the em
     */
    <T> void saveOrUpdate(T t, EntityManager em);

    /**
     * Gets all objects.
     *
     * @param <T>    the type parameter
     * @param i_Type the type
     * @return the all objects
     */
    <T> List<T> getAllObjects(Class<T> i_Type);

    /**
     * Gets object by id.
     *
     * @param <T>    the type parameter
     * @param i_Type the type
     * @param i_ID   the id
     * @return the object by id
     */
    <T> T getObjectById(Class<T> i_Type, int i_ID);

    /**
     * Gets objectby email.
     *
     * @param <T>     the type parameter
     * @param i_Type  the type
     * @param i_email the email
     * @return the objectby email
     */
    <T> T getObjectbyEmail(Class<T> i_Type, String i_email);

    /**
     * Delete object.
     *
     * @param <T>    the type parameter
     * @param i_Type the type
     * @param t      the t
     */
    <T> void deleteObject(Class<T> i_Type, int t);

    /**
     * Gets jinq query.
     *
     * @param <T>    the type parameter
     * @param i_Type the type
     * @param em     the em
     * @return the query
     */
    <T> JPAJinqStream<T> getQuery(Class<T> i_Type, EntityManager em);

    /**
     * Exec query list.
     *
     * @param <T>     the type parameter
     * @param i_Type  the type
     * @param i_Query the query
     * @return the list
     */
    <T> List<T> ExecQuery(Class<T> i_Type, CriteriaQuery<T> i_Query);

    /**
     * Query entity t.
     *
     * @param <T>            the type parameter
     * @param i_Type         the type
     * @param i_PropertyName the property name
     * @param i_Value        the value
     * @return the t
     */
    <T> T queryEntity(Class<T> i_Type, String i_PropertyName, Object i_Value);

    /**
     * Query entity list list.
     *
     * @param <T>            the type parameter
     * @param i_Type         the type
     * @param i_PropertyName the property name
     * @param i_Value        the value
     * @return the list
     */
    <T> List<T> queryEntityList(Class<T> i_Type, String i_PropertyName, Object i_Value);

    /**
     * Gets all objects.
     *
     * @param <T>           the type parameter
     * @param i_Type        the type
     * @param propertyNames the property names
     * @return the all objects
     */
    <T> List<T> getAllObjects(Class<T> i_Type, String[] propertyNames);

    /**
     * Gets object by id.
     *
     * @param <T>    the type parameter
     * @param i_Type the type
     * @param i_ID   the id
     * @param params the params
     * @return the object by id
     */
    <T> T getObjectById(Class<T> i_Type, int i_ID, String[] params);

    /**
     * Gets entity manager.
     *
     * @return the entity manager
     */
    EntityManager getEntityManager();

    /**
     * Close.
     */
    void close();

    /**
     * Gets database name.
     *
     * @return the database name
     */
    String getDatabaseName();

}

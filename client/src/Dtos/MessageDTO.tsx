//Denke nicht dass diese DTO schon passt

export interface MessageDTO {
    id: number;
    text: string;
    username: string;
    myMessage: boolean;
    channelId: number;
}
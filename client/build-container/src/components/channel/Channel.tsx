export interface Channel {
    id: number,
    name: string,
    private: boolean
 }
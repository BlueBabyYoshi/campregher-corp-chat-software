package lib.verticle.server.handlers.users;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.User;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

/**
 * The DeleteUserHandler receives a username from the frontend and deletes the user by setting isActiveUser to false
 */
@HttpHandler(endpoint = HandlerEndpoints.Users.DELETE_USER, method = HttpMethod.POST, interfaceType = IDeleteUserHandler.class)
public class DeleteUserHandler extends AbstractHttpHandler implements IDeleteUserHandler {

    @Inject
    private IQueryService queryService;
    @Inject
    private Vertx vertx = getVertx();

    @Override
    public void handle(RoutingContext context) {
        var gson = new Gson();
        String username = gson.fromJson(context.getBodyAsString(), String.class);
        queryService.getUserByUsername(context.session().get("username")).setHandler(checkUser -> {
            if (checkUser.succeeded()) {
                User requestingUser = checkUser.result();
                // check if user is allowed to delete users
                if (!requestingUser.hasRole("Admin")) {
                    System.out.println("WARNING: unauthorized user wanted to delete another user or her/his self");
                    return;
                }
                queryService.getUserByUsername(username).setHandler(readUser -> {
                    if (readUser.succeeded()) {
                        User user = readUser.result();
                        // check if user to delete is admin (admins can not be deleted)
                        if (user.hasRole("Admin")) {
                            System.out.println("WARNING: admin wanted to delete another admin or her/his self");
                            return;
                        }
                        user.setActiveUser(false);
                        queryService.updateUser(user).setHandler(deleteUser -> {
                            if (deleteUser.succeeded()) {
                                System.out.println("user " + username + " deleted");
                                writeResponseToContext(context, "deleted");
                            } else {
                                System.out.println("Error when deleting user");
                                writeResponseToContext(context, "error");
                            }
                        });
                    }
                });
            }
        });
    }

    private void writeResponseToContext(RoutingContext context, String result) {
        var gson = new Gson();
        var resultAsString = gson.toJson(result);
        System.out.println(resultAsString);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

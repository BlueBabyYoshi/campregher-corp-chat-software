package lib.verticle.server.handlers.users;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Department;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.model.UserRole;
import lib.verticle.server.dtos.EditUserResultDTO;
import lib.verticle.server.dtos.UserInfoDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

import java.util.HashSet;

/**
 * The EditUserHandler is called by a user to edit her/his profile
 * or by an admin to edit the profile of a specific user
 * the handler receives the new information and updates the user
 */
@HttpHandler(endpoint = HandlerEndpoints.Users.EDIT_USER, method = HttpMethod.POST, interfaceType = IEditUserHandler.class)
public class EditUserHandler extends AbstractHttpHandler implements IEditUserHandler {

    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {
        var gson = new Gson();
        UserInfoDTO userInfo = gson.fromJson(context.getBodyAsString(), UserInfoDTO.class);
        EditUserResultDTO resultDTO = new EditUserResultDTO();
        queryService.getUserByUsername(context.session().get("username")).setHandler(checkUser -> {
            if (checkUser.succeeded()) {
                User requestingUser = checkUser.result();
                // check if user is allowed to edit another user
                if (!userInfo.getUsername().equals(requestingUser.getUsername()) && !requestingUser.hasRole("Admin")) {
                    System.out.println("WARNING: unauthorized user wanted to edit another user");
                    return;
                }
                if (userInfo != null && !userInfo.isAnyValueEmpty()) {
                    User userToUpdate = new User(userInfo.getUsername(), userInfo.getName(), null, null, userInfo.getEmailAddress(), new HashSet<UserRole>(), Department.valueOf(userInfo.getDepartment()), userInfo.getOffice(), userInfo.isActiveUser(), EmailFrequency.valueOf(userInfo.getEmailFrequency()));
                    if (userInfo.isAdmin()) userToUpdate.getRoles().add(new UserRole("Admin", userToUpdate));
                    if (userInfo.isUser()) userToUpdate.getRoles().add(new UserRole("User", userToUpdate));
                    // get existing user from database to update changed values
                    queryService.getUserByUsername(userInfo.getUsername()).setHandler(readUser -> {
                        if (readUser.succeeded()) {
                            User userFromDB = readUser.result();
                            // check if admin wants to edit another admin
                            if (userFromDB.hasRole("Admin") && !requestingUser.getUsername().equals(userInfo.getUsername())) {
                                System.out.println("WARNING: admin wanted to edit another admin");
                                return;
                            }
                            userToUpdate.setId(userFromDB.getId());
                            userToUpdate.setPassword(userFromDB.getPassword());
                            userToUpdate.setSalt(userFromDB.getSalt());
                            // update user
                            queryService.updateUser(userToUpdate).setHandler(updateUser -> {
                                if (updateUser.succeeded()) {
                                    resultDTO.setSuccess(true);
                                    resultDTO.setMessage("Änderungen wurden gespeichert");
                                    System.out.println(resultDTO.getMessage());
                                } else {
                                    resultDTO.setSuccess(false);
                                    resultDTO.setMessage("Änderungen konnten nicht gespeichert werden");
                                    System.out.println(resultDTO.getMessage());
                                }
                                writeResponseToContext(context, resultDTO);
                            });
                        } else {
                            resultDTO.setSuccess(false);
                            resultDTO.setMessage("Änderungen konnten nicht gespeichert werden");
                            System.out.println(resultDTO.getMessage() + " -> Fehler beim DB auslesen");
                            writeResponseToContext(context, resultDTO);
                        }
                    });
                } else {
                    resultDTO.setMessage("Bitte füllen Sie alle Felder aus");
                    resultDTO.setSuccess(false);
                    writeResponseToContext(context, resultDTO);
                }
            }
        });
    }

    private void writeResponseToContext(RoutingContext context, EditUserResultDTO dto) {
        var gson = new Gson();
        var resultAsString = gson.toJson(dto);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

package lib.verticle.server.handlers.fileUpload.dtos;

public class FileMessageDTO {

    private String data;
    private String name;
    private int channelId;

    public String getData() {
        return data;
    }

    public void setData(String date) {
        this.data = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}

package lib.Database;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import org.jinq.orm.stream.JinqStream;

import java.util.List;

/**
 * The Async jinq class, workaround to make JINQ work with the Eventbus and to be able to look for First found element of a query or for a list of entries.
 */
public class AsyncJinq {

    /**
     * Async to list future, attatched to a JINQ query this resolves it and returns a list of found db entries
     *
     * @param <T>           the type parameter
     * @param stream        the stream
     * @param vertxInstance the vertx instance
     * @return the future
     */
    public static <T> Future<List<T>> asyncToList(JinqStream<T> stream, Vertx vertxInstance) {

        return executeBlocking(stream::toList, vertxInstance);
    }

    /**
     * Async first or default future, attatched to a JINQ query this resolves it and returns the first element or null
     *
     * @param <T>           the type parameter
     * @param stream        the stream
     * @param vertxInstance the vertx instance
     * @return the future
     */
    public static <T> Future<T> asyncFirstOrDefault(JinqStream<T> stream, Vertx vertxInstance) {

        return executeBlocking(() -> stream.findFirst().orElse(null), vertxInstance);
    }

    private static <T> Future<T> executeBlocking(BlockingAction<?> action, Vertx vertxInstance) {

        Promise<T> blockingPromise = Promise.promise();
        vertxInstance.executeBlocking(promise -> promise.complete(action.function()), reply -> {
            if (reply.succeeded()) {
                blockingPromise.complete((T) reply.result());
            } else {
                blockingPromise.fail("fail halt");
                System.out.println("Fail or ResultSet Null!");
            }
        });
        return blockingPromise.future();
    }
}

package lib.verticle.server.dtos;

public class NewUserDTO {
    private String username;
    private String name;
    private String emailAddress;
    private Boolean isAdmin;
    private Boolean isUser;
    private String department;
    private String office;
    private String emailFrequency;

    public NewUserDTO() {

    }

    public NewUserDTO(String username, String name, String emailAddress, Boolean isAdmin, Boolean isUser, String department, String office, String emailFrequency) {
        this.username = username;
        this.name = name;
        this.emailAddress = emailAddress;
        this.isAdmin = isAdmin;
        this.isUser = isUser;
        this.department = department;
        this.office = office;
        this.emailFrequency = emailFrequency;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Boolean getUser() {
        return isUser;
    }

    public void setUser(Boolean user) {
        isUser = user;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getEmailFrequency() {
        return emailFrequency;
    }

    public void setEmailFrequency(String emailFrequency) {
        this.emailFrequency = emailFrequency;
    }

    public boolean isAnyValueEmpty() {
        return (username.isBlank() || name.isBlank() || emailAddress.isBlank() || office.isBlank());
    }
}

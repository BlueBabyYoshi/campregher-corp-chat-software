package lib.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * The type User.
 */
@Entity
@Table(name = "Users")
@Access(AccessType.PROPERTY)
public class User extends BaseModel {


    private String username;

    private String name;

    private String password;

    private String salt;

    private String emailAddress;

    private Set<UserRole> roles;

    private Department department;

    private String office;

    private Boolean isActiveUser;

    private EmailFrequency emailFrequency;

    public User() {

    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String name, String password, String salt, String emailAddress, Set<UserRole> roles, Department department, String office, Boolean isActiveUser, EmailFrequency emailFrequency) {
        this.username = username;
        this.name = name;
        this.password = password;
        this.salt = salt;
        this.emailAddress = emailAddress;
        this.roles = roles;
        this.department = department;
        this.office = office;
        this.isActiveUser = isActiveUser;
        this.emailFrequency = emailFrequency;
    }

    public User(int userId) {
        this.id = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL)
    public Set<UserRole> getRoles() {
        if (roles == null)
            roles = new HashSet<UserRole>();
        return roles;
    }

    public void setRoles(Set<UserRole> roles) {
        this.roles = roles;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public Boolean getActiveUser() {
        return isActiveUser;
    }

    public void setActiveUser(Boolean activeUser) {
        isActiveUser = activeUser;
    }

    public EmailFrequency getEmailFrequency() {
        return emailFrequency;
    }

    public void setEmailFrequency(EmailFrequency emailFrequency) {
        this.emailFrequency = emailFrequency;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean hasRole(String role) {
        for (UserRole r : getRoles()) {
            if (r.getRoleName().equals(role))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "{" +
                "username: \"" + username + "\", " +
                "name: \"" + name + "\", " +
                "emailAddress: \"" + emailAddress + "\", " +
                "isAdmin: \"" + hasRole("Admin") + "\", " +
                "isUser: \"" + hasRole("User") + "\", " +
                "department: \"" + department.toString() + "\", " +
                "office: \"" + office + "\"" +
                '}';
    }
}

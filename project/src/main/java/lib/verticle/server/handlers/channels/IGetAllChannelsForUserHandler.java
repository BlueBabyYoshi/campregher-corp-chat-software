package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Get all channels for user handler.
 */
public interface IGetAllChannelsForUserHandler extends IHttpHandler {
}

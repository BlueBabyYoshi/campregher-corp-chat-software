package lib.services.ConfigService;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * The Config  implementation.
 */
public class ConfigServiceImpl implements ConfigService {

    private final String configFileName = "config.properties";
    private final String configFilePath = Paths.get("team4", "application", "config", configFileName).toString();
    private Properties applicationProperties;
    private String completePath = "";

    @Override
    public void loadProperties() throws IOException, URISyntaxException {

        createCompletePath();
        File configFile = new File(completePath);

        if (!configFile.exists()) {
            configFile.getParentFile().mkdirs();
            configFile.createNewFile();
        }

        FileReader reader = new FileReader(configFile);

        applicationProperties = new Properties();
        // load the properties file:
        applicationProperties.load(reader);
    }

    @Override
    public void setProperty(String key, String value) {
        applicationProperties.setProperty(key, value);
    }

    @Override
    public String getProperty(String key) {
        return applicationProperties.getProperty(key);
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        return applicationProperties.getProperty(key, defaultValue);
    }

    private void createCompletePath() throws URISyntaxException {
        String path = new File(ConfigServiceImpl.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
        completePath = Paths.get(path, configFilePath).toString();
    }

    @Override
    public void saveProperties() throws IOException, URISyntaxException {
        createCompletePath();
        File configFile = new File(completePath);
        FileWriter writer = new FileWriter(configFile);
        applicationProperties.store(writer, "Team 4 App Config");
        writer.close();
    }
}

package lib.verticle.server.handlers.channels.notUsedAnymore;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.model.Channel;
import lib.verticle.server.handlers.channels.AbstractAddUserHandler;
import lib.verticle.server.handlers.channels.dtos.ChannelUserActionDTO;

/**
 * The type Add user to channel handler.
 */
//@HttpHandler(endpoint = HandlerEndpoints.Channels.ADD_USER, method = HttpMethod.POST, interfaceType = IAddUserToChannelHandler.class)
public class AddUserToChannelHandler extends AbstractAddUserHandler implements IAddUserToChannelHandler {

    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {
        var dto = getGsonService().fromJson(context.getBodyAsString(), ChannelUserActionDTO.class);
        queryService.getChannelById(dto.getChannelId()).setHandler(getChannel -> {
            if (getChannel.succeeded()) {
                Channel channel = getChannel.result();
                if (!channel.isUserMod(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to add user to channel");
                    return;
                }
                addUser(dto.getUserId(), dto.getChannelId(), context, (c, u) -> c.getChannelMembers().add(u));
            }
        });
    }
}

import * as React from "react";
import Cookies from 'universal-cookie';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import { ClickAwayListener } from "@material-ui/core";
import { Redirect } from "react-router-dom";


async function logout() {
  const cookies = new Cookies();
  cookies.remove('adminCookie');
  try{
    let response = await fetch("http://localhost:8080/logout", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: "true",
      mode: "cors",
      credentials: "include"
    });
    let jsonResponse = await  response.json();
    console.log(jsonResponse);

    const sessionId = cookies.get('vertx-web.session');
    if(await response.json() === true){
      cookies.remove('vertx-web.session'); 
    }
  }
  catch(e){

  }
}

class Test extends React.Component {
    render() {
      logout();
        return <Redirect to="/login" />
    }
}
export default Test;
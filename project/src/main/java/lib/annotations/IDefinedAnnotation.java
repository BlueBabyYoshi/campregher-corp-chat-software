package lib.annotations;

/**
 * The interface Defined annotation.
 */
public interface IDefinedAnnotation {

    /**
     * Returns annotation type/class.
     *
     * @return the annotation type
     */
    Class<?> getAnnotationType();

    /**
     * Sets annotation type/class to search for.
     *
     * @param type the type
     */
    void setAnnotationType(Class<?> type);

    /**
     * Returns the set Handler/Method for an Annotation Type
     *
     * @return the found handler
     */
    IAnnotationFoundHandler getFoundHandler();

    /**
     * Sets the handler/method that should get excecuted when an annotation as found
     *
     * @param handler the handler
     */
    void setFoundHandler(IAnnotationFoundHandler handler);
}

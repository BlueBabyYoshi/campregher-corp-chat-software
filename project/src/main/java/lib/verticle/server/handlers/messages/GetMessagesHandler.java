package lib.verticle.server.handlers.messages;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Message;
import lib.verticle.server.dtos.MessageDTO;
import lib.verticle.server.dtos.RequestMessageDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

import java.util.ArrayList;
import java.util.List;

/**
 * The GetMessagesHandler, gets messages by channelId, checks if current user wrote it and ads a flag for every message to the response so we know in the frontend if current viewing user write them
 */
@HttpHandler(endpoint = HandlerEndpoints.Messages.GET_ALL_MESSAGES, method = HttpMethod.POST, interfaceType = IGetMessagesHandler.class)
public class GetMessagesHandler extends AbstractHttpHandler implements IGetMessagesHandler {


    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {

        System.out.println("POST Messages was called!");
        Gson gson = new Gson();

        RequestMessageDTO requestMessageDTO = gson.fromJson(context.getBodyAsString(), RequestMessageDTO.class);

        queryService.getUserByUsername(context.session().get("username")).setHandler(r -> {
            if (r.succeeded()) {
                int requestingUserId = r.result().getId();
                String username = r.result().getUsername();
                queryService.getMessages(requestMessageDTO.getChannelId()).setHandler(r2 -> {
                    if (r.succeeded()) {
                        String json = "";
                        List<Message> mes = r2.result();
                        List<MessageDTO> mesDTO = new ArrayList<>();
                        if (mes != null) {
                            for (Message message : mes) {
                                mesDTO.add(new MessageDTO(message.getId(), message.getText(), message.getFromUser().getUsername(), requestingUserId == message.getFromUser().getId(), message.getInChannel().getId()));
                            }
                        }
                        if (mesDTO != null) {
                            json = gson.toJson(mesDTO);
                        } else {
                            System.out.println("No Messages got returned!");
                        }
                        HttpServerResponse response = context.response();
                        response.putHeader("content-type", "application/json");
                        response.end(json);
                    }
                });
            }
        });
    }
}

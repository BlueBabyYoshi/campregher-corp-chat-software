import MUIDataTable from "mui-datatables";
import * as React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import Avatar from "@material-ui/core/Avatar";
import SettingsIcon from "@material-ui/icons/Settings";
import DeleteIcon from '@material-ui/icons/Delete';
import {UserInfoDTO} from "../../Dtos/UserInfoDTO";
import {AllUsersState} from "./AllUsersState";
import { confirmAlert } from 'react-confirm-alert'; // Import
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    IconButton
} from "@material-ui/core";
import {UserInfoState} from "../../profile/UserInfoState";
import FormLabel from "@material-ui/core/FormLabel";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import {NewUserResultDTO} from "../../Dtos/newUserResultDTO";
import {EditUserResultDTO} from "../../Dtos/EditUserResultDTO";

//import { observable, computed } from "mobx";


class userAdminSettings extends React.Component {

  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  public state: AllUsersState = {
    users: [],
    isLoading: false
  }

  public async componentDidMount(): Promise<void> {
    this.setState({...this.state, isLoading: true});
    try {
      let response = await fetch("http://localhost:8080/allUsers", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "GET",
        mode: "cors",
        credentials: "include"
      });
      let jsonResult = await response.json();
      let jsonObject: UserInfoDTO = JSON.parse(JSON.stringify(jsonResult));
      this.setState({...this.state, users: jsonObject, isLoading: false});
    } catch(e) {
      console.log("All Users Error");
      this.setState({
        ...this.state,
        result: "Fehler bei Webserverabruf."
      });
      console.log(e);
    }
  }

    private columns = [
        {
            name: "image",
            label: "Avatar",
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return <Avatar alt="Profile" src={value} />;
                }
            }
        },
        {
            name: "username",
            label: "Username",
            options: {
                filter: true
            }
        },
        {
            name: "name",
            label: "Name",
            options: {
                filter: true
            }
        },
        {
            name: "emailAddress",
            label: "Email-Adresse",
            options: {
                filter: true,
            }
        },
        {
            name: "isAdmin",
            label: "Admin",
            options: {
                filter: true,
                customBodyRender: (value) => {
                    return <Checkbox checked={value}/>;
                }
            }
        },
        {
            name: "isUser",
            label: "User",
            options: {
                filter: true,
                customBodyRender: (value) => {
                    return <Checkbox checked={value} />;
                }
            }
        },
        {
            name: "department",
            label: "Abteilung",
            options: {
                filter: true,
            }
        },
        {
            name: "office",
            label: "Büro",
            options: {
                filter: true,
            }
        },
        {
            name: "emailFrequency",
            label: "Emailversand",
            options: {
                filter: true,
            }
        },
        {
            name: "username",
            label: "Settings",
            options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                    function EditDialog() {
                        const username = value;
                        const [name, setName] = React.useState("");
                        const [emailAddress, setEmailAddress] = React.useState("");
                        const [isAdmin, setAdmin] = React.useState(false);
                        const [isUser, setUser] = React.useState(false);
                        const [department, setDepartment] = React.useState("");
                        const [office, setOffice] = React.useState("");
                        const [emailFrequency, setEmailFrequency] = React.useState("");
                        const [isSameUser, setSameUser] = React.useState(false);
                        const [open, setOpen] = React.useState(false);
                        const handleClickOpen = async () => {
                            let response = await fetch("http://localhost:8080/userInfo", {
                                headers: {
                                    "Content-Type": "application/json",
                                },
                                method: "POST",
                                body: value,
                                mode: "cors",
                                credentials: "include"
                            });
                            let jsonResult = await response.json();
                            let jsonObject: UserInfoDTO = JSON.parse(JSON.stringify(jsonResult));
                            setName(jsonObject.name);
                            setEmailAddress(jsonObject.emailAddress);
                            setAdmin(jsonObject.isAdmin);
                            setUser(jsonObject.isUser);
                            setDepartment(jsonObject.department);
                            setOffice(jsonObject.office);
                            setEmailFrequency(jsonObject.emailFrequency);
                            setSameUser(jsonObject.isSameUser);
                            setOpen(true);
                        };
                        const handleSave = async () => {
                            const userInfoDTO: UserInfoDTO = {
                                username: value,
                                name: name,
                                emailAddress: emailAddress,
                                isAdmin: isAdmin,
                                isUser: isUser,
                                department: department,
                                office: office,
                                emailFrequency: emailFrequency,
                                isActiveUser: true,
                                isSameUser: isSameUser
                            }
                            let response = await fetch("http://localhost:8080/editUser", {
                                headers: {
                                    "Content-Type": "application/json",
                                },
                                method: "POST",
                                body: JSON.stringify(userInfoDTO),
                                mode: "cors",
                                credentials: "include"
                            });
                            let jsonResult = await response.json();
                            let jsonObject: EditUserResultDTO = JSON.parse(JSON.stringify(jsonResult));
                            if (jsonObject.success) {
                                window.location.reload();
                            }
                            setOpen(false);
                        }
                        const handleClose = () => {
                            setOpen(false);
                        };
                        const onNameChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setName(event.target.value);
                        }
                        const onEmailAddressChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setEmailAddress(event.target.value);
                        }
                        const onAdminChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setAdmin(event.target.checked);
                        }
                        const onUserChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setUser(event.target.checked);
                        }
                        const onDepartmentChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setDepartment(event.target.value);
                        }
                        const onOfficeChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setOffice(event.target.value);
                        }
                        const onEmailFrequencyChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
                            setEmailFrequency(event.target.value);
                        }
                        return (
                            <div>
                                <IconButton
                                    aria-label="delete"
                                    color="primary"
                                    onClick={handleClickOpen}
                                >
                                    <SettingsIcon/>
                                </IconButton>
                                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                                    <DialogTitle id="form-dialog-title">Benutzer bearbeiten</DialogTitle>
                                    <DialogContent>
                                        <TextField
                                            autoFocus
                                            value={value}
                                            margin="dense"
                                            id="username"
                                            label="Username"
                                            disabled={true}
                                            fullWidth
                                        />
                                        <TextField
                                            autoFocus
                                            disabled={isAdmin && !isSameUser}
                                            required={true}
                                            value={name}
                                            margin="dense"
                                            id="name"
                                            label="Name"
                                            onChange={onNameChanged}
                                            fullWidth
                                        />
                                        <TextField
                                            autoFocus
                                            disabled={isAdmin && !isSameUser}
                                            required={true}
                                            value={emailAddress}
                                            margin="dense"
                                            id="emailAddress"
                                            label="Email-Adresse"
                                            onChange={onEmailAddressChanged}
                                            fullWidth
                                        />
                                        <TextField
                                            autoFocus
                                            disabled={isAdmin && !isSameUser}
                                            required={true}
                                            value={office}
                                            margin="dense"
                                            id="office"
                                            label="Büro"
                                            onChange={onOfficeChanged}
                                            fullWidth
                                        />
                                        <FormControl component="fieldset">
                                            <FormLabel component="legend">Rechte:</FormLabel>
                                            <FormGroup>
                                                <FormControlLabel label="Admin" control={<Checkbox />} disabled={isAdmin || isSameUser} value={isAdmin} checked={isAdmin === true} onChange={onAdminChanged} />
                                                <FormControlLabel label="Benutzer" control={<Checkbox />} disabled={isAdmin || isSameUser} value={isUser} checked={isUser === true} onChange={onUserChanged} />
                                            </FormGroup>
                                        </FormControl>
                                        <FormControl component="fieldset" >
                                            <FormLabel component="legend">Abteilung:</FormLabel>
                                            <RadioGroup aria-label="department" name="department">
                                                <FormControlLabel value="Other" control={<Radio />} disabled={isAdmin && !isSameUser} label="Andere" checked={department === "Other"} onChange={onDepartmentChanged} />
                                                <FormControlLabel value="HR" control={<Radio />} disabled={isAdmin && !isSameUser} label="Personal" checked={department === "HR"} onChange={onDepartmentChanged} />
                                                <FormControlLabel value="Marketing" control={<Radio />} disabled={isAdmin && !isSameUser} label="Marketing" checked={department === "Marketing"} onChange={onDepartmentChanged}/>
                                                <FormControlLabel value="Sales" control={<Radio />} disabled={isAdmin && !isSameUser} label="Verkauf" checked={department === "Sales"} onChange={onDepartmentChanged}/>
                                                <FormControlLabel value="Production" control={<Radio />} disabled={isAdmin && !isSameUser} label="Produktion" checked={department === "Production"} onChange={onDepartmentChanged}/>
                                                <FormControlLabel value="IT" control={<Radio />} disabled={isAdmin && !isSameUser} label="IT" checked={department === "IT"} onChange={onDepartmentChanged}/>
                                                <FormControlLabel value="Finance" control={<Radio />} disabled={isAdmin && !isSameUser} label="Finanz" checked={department === "Finance"} onChange={onDepartmentChanged}/>
                                                <FormControlLabel value="Management" control={<Radio />} disabled={isAdmin && !isSameUser} label="Management" checked={department === "Management"} onChange={onDepartmentChanged}/>
                                            </RadioGroup>
                                        </FormControl>
                                        <FormControl component="fieldset" >
                                            <FormLabel component="legend">Wie oft sollen Mails verschickt werden:</FormLabel>
                                            <RadioGroup aria-label="email-frequency" name="email-frequency">
                                                <FormControlLabel value="Monthly" control={<Radio />} disabled={isAdmin && !isSameUser} label="Monatlich" checked={emailFrequency === "Monthly"} onChange={onEmailFrequencyChanged} />
                                                <FormControlLabel value="Weekly" control={<Radio />} disabled={isAdmin && !isSameUser} label="Wöchentlich" checked={emailFrequency === "Weekly"} onChange={onEmailFrequencyChanged}/>
                                                <FormControlLabel value="Never" control={<Radio />} disabled={isAdmin && !isSameUser} label="Nie" checked={emailFrequency === "Never"} onChange={onEmailFrequencyChanged}/>
                                            </RadioGroup>
                                        </FormControl>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleClose} color="primary">
                                            Abbrechen
                                        </Button>
                                        <Button disabled={isAdmin && !isSameUser} onClick={handleSave} color="primary">
                                            Speichern
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </div>
                        );
                    }
                    return (
                        <div>
                            <EditDialog/>
                        </div>
                    );
                }
            }
        },
        {
            name: "username",
            label: "Löschen",
            options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                    function DeleteDialog() {
                        const username = value;
                        const [isAdmin, setAdmin] = React.useState(false);
                        const [open, setOpen] = React.useState(false);
                        const handleClickOpen = async () => {
                            let response = await fetch("http://localhost:8080/userInfo", {
                                headers: {
                                    "Content-Type": "application/json",
                                },
                                method: "POST",
                                body: value,
                                mode: "cors",
                                credentials: "include"
                            });
                            let jsonResult = await response.json();
                            let jsonObject: UserInfoDTO = JSON.parse(JSON.stringify(jsonResult));
                            setAdmin(jsonObject.isAdmin);
                            setOpen(true);
                        };
                        const handleDelete = async () => {
                            let result = await fetch("http://localhost:8080/deleteUser", {
                                headers: {
                                    "Content-Type": "application/json",
                                },
                                method: "POST",
                                body: JSON.stringify(value),
                                mode: "cors",
                                credentials: "include"
                            });
                            let jsonResult = await result.json();
                            let jsonObject = JSON.parse(JSON.stringify(jsonResult));
                            console.log("delete test")
                            if(jsonObject == "deleted"){
                                console.log("delete test inside")
                                window.location.reload();
                            }
                            
                            setOpen(false);
                        };
                        const handleClose = () => {
                            setOpen(false);
                        };

                        return (
                            <div>
                                <IconButton
                                    aria-label="delete"
                                    color="primary"
                                    onClick={handleClickOpen}
                                >
                                    <DeleteIcon />
                                </IconButton>
                                <Dialog
                                    open={open}
                                    onClose={handleClose}
                                    aria-labelledby="alert-dialog-title"
                                    aria-describedby="alert-dialog-description"
                                >
                                    <DialogTitle id="alert-dialog-title">{"Benutzer löschen"}</DialogTitle>
                                    <DialogContent>
                                        <DialogContentText id="alert-dialog-description">
                                            {"Sind Sie sicher?"}
                                        </DialogContentText>
                                    </DialogContent>
                                    <DialogActions>
                                        <Button onClick={handleClose} color="primary">
                                            Abbrechen
                                        </Button>
                                        <Button disabled={isAdmin} onClick={handleDelete} color="primary" autoFocus>
                                            Löschen
                                        </Button>
                                    </DialogActions>
                                </Dialog>
                            </div>
                        );
                    }
                    return (
                        <div>
                            <DeleteDialog/>
                        </div>
                    );
                }
            }
        }
    ];

    private options = {
        responsive: "scrollMaxHeight",
        filter: true,
        filterType: "dropdown",
        selectableRows: "none"
    };


  render() {
    if (this.state.isLoading) {
      return <p>Loading ...</p>;
    }
    return (
        <div style={{ padding: "50px" }}>
          <MUIDataTable
              aria-label="simple table"
              columns={this.columns}
              options={this.options}
              data={this.state.users}
          />
        </div>
    );
  }
}
export default userAdminSettings;

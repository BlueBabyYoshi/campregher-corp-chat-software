package lib.services.LoggingService;

/**
 * The interface Logging service.
 */
public interface ILoggingService {
    /**
     * Log info.
     *
     * @param msg the msg
     */
    void logInfo(String msg);

    /**
     * Log error.
     *
     * @param msg the msg
     * @param ex  the ex
     */
    void logError(String msg, Exception ex);

    /**
     * Log debug.
     *
     * @param msg the msg
     */
    void logDebug(String msg);

    /**
     * Log warn.
     *
     * @param msg the msg
     */
    void logWarn(String msg);
}

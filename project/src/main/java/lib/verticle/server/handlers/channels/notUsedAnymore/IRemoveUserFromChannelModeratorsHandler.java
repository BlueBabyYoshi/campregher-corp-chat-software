package lib.verticle.server.handlers.channels.notUsedAnymore;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Remove user from channel moderators handler.
 */
public interface IRemoveUserFromChannelModeratorsHandler extends IHttpHandler {
}

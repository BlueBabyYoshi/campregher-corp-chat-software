package lib.verticle.server.handlers.channels.dtos;

public class ChannelUserActionDTO extends ChannelIdDTO {

    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}

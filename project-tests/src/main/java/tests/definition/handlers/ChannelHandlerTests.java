package tests.definition.handlers;

import lib.model.Channel;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.channels.dtos.ChannelEditDTO;
import lib.verticle.server.handlers.channels.dtos.ChannelIdDTO;
import lib.verticle.server.handlers.channels.dtos.ChannelUserActionDTO;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import tests.base.testdefinition.AbstractVertxTest;

import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The type Channel handler tests.
 */
public class ChannelHandlerTests extends AbstractVertxTest {

    /**
     * Sets database.
     */
    @BeforeEach
    public void setupDatabase() {
        clearDatabaseFrom(Channel.class);
    }

    /**
     * Test add user to channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testAddUserToChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var userActionDto = prepareData(handler, false, false);
        var client = getRestClient();
        client.post(HandlerEndpoints.Channels.ADD_USER, userActionDto, Channel.class);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> {
            var loadedChannel = res.result().stream()
                    .filter(r -> r.getId() == userActionDto.getChannelId())
                    .findFirst().orElse(null);
            assertThat(loadedChannel).isNotNull();
            assertThat(loadedChannel.getChannelMembers()).isNotEmpty();
        });
    }

    /**
     * Test add moderator to channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testAddModeratorToChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var userActionDto = prepareData(handler, false, false);
        var client = getRestClient();
        client.post(HandlerEndpoints.Channels.ADD_MODERATOR, userActionDto, Channel.class);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> {
            var loadedChannel = res.result().stream()
                    .filter(r -> r.getId() == userActionDto.getChannelId())
                    .findFirst().orElse(null);
            assertThat(loadedChannel).isNotNull();
            assertThat(loadedChannel.getModerator()).isNotEmpty();
        });
    }

    /**
     * Test remove user from channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testRemoveUserFromChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var userActionDto = prepareData(handler, false, true);
        var client = getRestClient();
        client.post(HandlerEndpoints.Channels.REMOVE_USER, userActionDto, Channel.class);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> {
            var loadedChannel = res.result().stream().findFirst().orElse(null);
            assertThat(loadedChannel).isNotNull();
            assertThat(loadedChannel.getChannelMembers()).isEmpty();
        });
    }

    /**
     * Test remove moderator from channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testRemoveModeratorFromChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var userActionDto = prepareData(handler, true, false);
        var client = getRestClient();
        client.post(HandlerEndpoints.Channels.REMOVE_MODERATOR, userActionDto, Channel.class);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> {
            var loadedChannel = res.result().stream().findFirst().orElse(null);
            assertThat(loadedChannel).isNotNull();
            assertThat(loadedChannel.getModerator()).isEmpty();
        });
    }

    private ChannelUserActionDTO prepareData(DatabaseHandler handler, boolean saveWithMod, boolean saveWithMember) throws Throwable {
        var user = createUser();
        var savedUser = new AtomicReference<User>();
        runAndVerifyInContext(ctx -> handler.saveOrUpdateObject(User.class, user), res -> savedUser.set(res.result()));
        var channel = createChannel();
        if (saveWithMember) {
            channel.getChannelMembers().add(savedUser.get());
        }
        if (saveWithMod) {
            channel.getModerator().add(savedUser.get());
        }
        var savedChannel = new AtomicReference<Channel>();
        runAndVerifyInContext(ctx -> handler.saveOrUpdateObject(Channel.class, channel), res -> savedChannel.set(res.result()));
        var userActionDto = new ChannelUserActionDTO();
        userActionDto.setUserId(savedUser.get().getId());
        userActionDto.setChannelId(savedChannel.get().getId());
        return userActionDto;
    }

    /**
     * Test edit channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testEditChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var channel = createChannel();
        var savedChannel = new AtomicReference<Channel>();
        runAndVerifyInContext(ctx -> handler.saveOrUpdateObject(Channel.class, channel), res -> savedChannel.set(res.result()));
        var savedChann = savedChannel.get();
        assertThat(savedChann.getPublic()).isTrue();
        var editDTO = new ChannelEditDTO();
        editDTO.setName(savedChann.getName());
        editDTO.setPublic(false);
        editDTO.setLocked(savedChann.isLocked());
        editDTO.setChannelId(savedChann.getId());
        var client = getRestClient();
        client.post(HandlerEndpoints.Channels.EDIT_CHANEL, editDTO, Channel.class);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> {
            var obj = res.result().stream().filter(r -> r.getId() == savedChannel.get().getId()).findFirst().orElse(null);
            assertThat(obj).isNotNull();
            assertThat(obj.getPublic()).isFalse();
        });
    }

    /**
     * Test delete channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testDeleteChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var channel = createChannel();
        var savedChannel = new AtomicReference<Channel>();
        runAndVerifyInContext(ctx -> handler.saveOrUpdateObject(Channel.class, channel), res -> savedChannel.set(res.result()));
        var client = getRestClient();
        assertThat(savedChannel.get()).isNotNull();
        var channelDTO = new ChannelIdDTO();
        channelDTO.setChannelId(savedChannel.get().getId());
        client.delete(HandlerEndpoints.Channels.DELETE_CHANNEL, channelDTO);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> assertThat(res.result()).isEmpty());
    }

    /**
     * Test new channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testNewChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var client = getRestClient();
        var channel = client.get(HandlerEndpoints.Channels.NEW_CHANNEL, Channel.class);
        assertThat(channel).isNotNull();
        assertThat(channel.getId()).isGreaterThan(0);
        runAndVerifyInContext(ctx -> handler.readObjects(Channel.class), res -> {
            assertThat(res.result()).isNotEmpty();
        });
    }

    /**
     * Test lock channel.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testLockChannel() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var channel = createChannel();
        var channelRef = new AtomicReference<Channel>();

        runAndVerifyInContext(ctx -> handler.saveOrUpdateObject(Channel.class, channel), res -> channelRef.set(res.result()));

        var client = getRestClient();
        var channelDTO = new ChannelIdDTO();
        channelDTO.setChannelId(channelRef.get().getId());
        var lockedChannel = client.post(HandlerEndpoints.Channels.LOCK_CHANNEL, channelDTO, Channel.class);
        assertThat(lockedChannel.isLocked()).isEqualTo(true);
    }

    private User createUser() {
        var user = new User();
        user.setActiveUser(true);
        user.setName("Test");
        user.setPassword("****");
        user.setSalt("1234");
        user.setEmailAddress("test@success.com");
        user.setUsername("TestUser");
        return user;
    }

    private Channel createChannel() {
        var channel = new Channel();
        channel.setLocked(false);
        channel.setName("test");
        channel.setPublic(true);
        return channel;
    }
}

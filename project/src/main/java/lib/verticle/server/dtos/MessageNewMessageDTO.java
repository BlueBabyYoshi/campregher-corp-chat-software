package lib.verticle.server.dtos;

public class MessageNewMessageDTO {
    //test only with text same in Frontend with DTO

    private int channelid;
    private String text;

    public MessageNewMessageDTO(String text, int channelid) {
        this.text = text;
        this.channelid = channelid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getChannelid() {
        return channelid;
    }

    public void setChannelid(int channelid) {
        this.channelid = channelid;
    }
}

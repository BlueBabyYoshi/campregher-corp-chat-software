package lib.model;

/**
 * The enum Department.
 */
public enum Department {
    Other,
    HR,
    Marketing,
    Sales,
    Production,
    IT,
    Finance,
    Management
}

package lib.Database.services;

import com.google.inject.Inject;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import lib.Database.AsyncJinq;
import lib.model.Channel;
import lib.model.Message;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.basic.BaseEventbusUser;
import org.jinq.orm.stream.JinqStream;

import java.util.List;

/**
 * Service to make different database queries
 * -- Usage: Inject the Interface
 * <pre>
 * {@code
 *    @Inject
 *     private IQueryService queryService;
 * }*
 * </pre>
 * <p>
 * --Usage: Use a function:
 * <pre>
 * {@code
 * queryService.someFunction(someParameter).setHandler(anyName ->{
 *      if(anyName.succeeded()){
 *          //Any code if query was successful (no Error, null results can happen)
 *      }
 * else {
 *          //any Code for a query Error
 *       }
 * });
 * </pre>
 * <p>
 * --Usage: handler.something functions if you have a key in your object or it gets generated.
 * --Usage: handler.jinqQuery() function if you only have a non key attribute*
 */
public class QueryService extends BaseEventbusUser implements IQueryService {

    @Inject
    Vertx vertx;
    @Inject
    private DatabaseHandler handler;

    /**
     * Function for getting all users from Database
     * @return a list of all active users
     */
    @Override
    public Future<List<User>> getAllUsers() {
        return handler.readObjects(User.class);
    }

    /**
     * Function for saving user to the Database
     * @param user of our Data model
     * @return the User with the Id the db gave to it
     */
    @Override
    public Future<User> saveUser(User user) {
        return handler.saveObject(User.class, user);
    }

    /**
     * Functions to save channels to database or update them
     * @param channel the channel you want to save
     * @return the saved channel with the id it was given by the database
     */
    @Override
    public Future<Channel> saveChannel(Channel channel) {
        return handler.saveOrUpdateObject(Channel.class, channel);
    }
    /**
     * Functions to save messages to database or update them
     * @param message the message you want to save
     * @return the saved message with the id it was given by the database
     */
    @Override
    public Future<Message> saveMessage(Message message) {
        return handler.saveOrUpdateObject(Message.class, message);
    }

    /**
     * Get a message with a special id
     * @param messageId the messageid
     * @return the full message from the database
     */
    @Override
    public Future<Message> getMessageById(int messageId) {
        Message message = new Message();
        message.setId(messageId);
        return handler.readObject(Message.class, message);
    }


    /**
     * Updates an existing User
     *
     * @param user a user implementation. user.Id must not be not null
     * @return the full updated User
     */

    @Override
    public Future<User> updateUser(User user) {
        return handler.saveOrUpdateObject(User.class, user);
    }

    /**
     *
     * @param username the username
     * @param vertx    the vertx, if it was not injected
     * @return the full user with all its data
     */
    @Override
    public Future<User> getUserByUsername(String username, Vertx vertx) {
        Promise<User> promise = Promise.promise();
        handler.jinqQuery(User.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<User> stream = r.result();
                JinqStream<User> UsernameStream = stream.where(user -> user.getUsername().equals(username));
                AsyncJinq.asyncFirstOrDefault(UsernameStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    /**
     *
     * @param username the username
     * @return the full user with all its data
     */
    @Override
    public Future<User> getUserByUsername(String username) {
        Promise<User> promise = Promise.promise();
        handler.jinqQuery(User.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<User> stream = r.result();
                JinqStream<User> UsernameStream = stream.where(user -> user.getUsername().equals(username));
                AsyncJinq.asyncFirstOrDefault(UsernameStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    /**
     * Get a channel with specific Channelname from Database
     * @param channelname the channelname
     * @return the full Channel with all its data
     */
    @Override
    public Future<Channel> getChannelByChannelname(String channelname) {
        Promise<Channel> promise = Promise.promise();
        handler.jinqQuery(Channel.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<Channel> stream = r.result();
                JinqStream<Channel> UsernameStream = stream.where(channel -> channel.getName().equals(channelname));
                AsyncJinq.asyncFirstOrDefault(UsernameStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    /**
     * Gets a channel with a specific id form Database
     * @param channelId the channelid
     * @return the wished channel with all its information
     */
    @Override
    public Future<Channel> getChannelById(int channelId) {
        Promise<Channel> promise = Promise.promise();
        handler.jinqQuery(Channel.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<Channel> stream = r.result();
                JinqStream<Channel> UsernameStream = stream.where(channel -> channel.getId() == channelId);
                AsyncJinq.asyncFirstOrDefault(UsernameStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    /**
     * GEts all the channel where the user with a specific user id is a member
     * @param userId the userid
     * @return returns all the channel where the user with that specific id is a member
     */
    @Override
    public Future<List<Channel>> getChannelForUser(int userId) {
        Promise<List<Channel>> promise = Promise.promise();
        handler.jinqQuery(Channel.class).setHandler(r -> {
            if (r.succeeded()) {
                var user = new User(userId);
                JinqStream<Channel> stream = r.result();
                JinqStream<Channel> channelStream = stream.where(channel -> channel.getChannelMembers().contains(user));
                AsyncJinq.asyncToList(channelStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    /**
     *
     * @param email
     * @param vertx the vertx
     * @return The User with the requested e-mail
     */
    @Override
    public Future<User> getUserByEmail(String email, Vertx vertx) {
        Promise<User> promise = Promise.promise();
        handler.jinqQuery(User.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<User> stream = r.result();
                JinqStream<User> EmailStream = stream.where(user -> user.getEmailAddress().equals(email));
                AsyncJinq.asyncFirstOrDefault(EmailStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    /**
     *
     * @param channelId the channelid
     * @return The channel with requested id
     */
    @Override
    public Future<List<Message>> getMessages(int channelId) {
        Promise<List<Message>> promise = Promise.promise();
        handler.jinqQuery(Message.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<Message> stream = r.result();
                JinqStream<Message> messageStream = stream.where(message -> message.getInChannel().getId() == channelId);
                AsyncJinq.asyncToList(messageStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }
}

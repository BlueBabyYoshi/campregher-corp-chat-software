package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Edit channel moderators handler.
 */
public interface IEditChannelModeratorsHandler extends IHttpHandler {
}

package lib.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * The type Roles permission.
 */
@Entity
@Table(name = "RolesPermission")
@Access(AccessType.PROPERTY)
public class RolesPermission extends BaseModel {

    private String permission;
    private String roleName;

    public RolesPermission() {

    }

    public RolesPermission(String roleName, String permission) {
        this.roleName = roleName;
        this.permission = permission;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

}


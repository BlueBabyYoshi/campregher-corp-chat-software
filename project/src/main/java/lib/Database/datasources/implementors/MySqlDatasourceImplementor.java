package lib.Database.datasources.implementors;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;

/**
 * The MYSQL datasource implementor.
 */
public class MySqlDatasourceImplementor extends BaseDataSourceFunctionalityImplementor<MysqlDataSource> {

    @Override
    public DataSource create() {
        return new MysqlDataSource();
    }

    @Override
    protected Class<MysqlDataSource> getAssumedDataSourceType() {
        return MysqlDataSource.class;
    }

    @Override
    protected void setUrlInternal(MysqlDataSource source, String url) {
        source.setUrl(url);
    }

    @Override
    protected void setUserInternal(MysqlDataSource source, String user) {
        source.setUser(user);
    }

    @Override
    protected void setPasswordInternal(MysqlDataSource source, String pw) {
        source.setPassword(pw);
    }
}

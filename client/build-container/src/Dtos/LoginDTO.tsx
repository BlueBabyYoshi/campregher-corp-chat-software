import { Login } from "../components/Login/Login";

export interface LoginDTO {
    username: string;
    password: string;
}
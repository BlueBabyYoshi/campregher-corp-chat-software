package lib.verticle.Database.handlers;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import lib.Database.CRUDActions;
import lib.Database.PersistenceOption;
import lib.verticle.basic.BaseEventbusUser;
import org.jinq.jpa.JPAJinqStream;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * The Database handler implementation.
 */
public class DatabaseHandlerImpl extends BaseEventbusUser implements DatabaseHandler {
    @Override
    public <T> Future<T> saveObject(Class<T> type, T object) {
        Promise<T> savePromise = Promise.promise();
        sendCrudMessage(type, object, CRUDActions.SAVE, savePromise);
        return savePromise.future();
    }

    @Override
    public <T> Future<T> saveOrUpdateObject(Class<T> type, T object) {
        Promise<T> saveOrUpdatePromise = Promise.promise();
        sendCrudMessage(type, object, CRUDActions.SAVE_OR_UPDATE, saveOrUpdatePromise);
        return saveOrUpdatePromise.future();
    }

    @Override
    public <T> Future<T> deleteObject(Class<T> type, T object) {
        Promise<T> deletePromise = Promise.promise();
        sendCrudMessage(type, object, CRUDActions.DELETE, deletePromise);
        return deletePromise.future();
    }

    @Override
    public <T> Future<T> readObject(Class<T> type, T object) {
        Promise<T> readPromise = Promise.promise();
        sendCrudMessage(type, object, CRUDActions.READ, readPromise);
        return readPromise.future();
    }

    @Override
    public <T> Future<List<T>> readObjects(Class<T> type) {
        Promise<List<T>> readPromise = Promise.promise();
        sendCrudMessage(type, null, CRUDActions.READ_LIST, readPromise);
        return readPromise.future();
    }

    @Override
    public <T> Future<JPAJinqStream<T>> jinqQuery(Class<T> type) {
        Promise<JPAJinqStream<T>> readPromise = Promise.promise();
        sendJinqMessage(type, null, null, CRUDActions.JINQ, readPromise);
        return readPromise.future();
    }

    private <T> void sendCrudMessage(Class<?> entityType, T object, String action, Promise<T> actionPromise) {
        request("database-call", object, PersistenceOption.createCRUDOption(action, entityType), r -> {
            if (r.succeeded()) {
                Message<T> msg;
                msg = r.result();
                actionPromise.complete(msg.body());
            } else {
                actionPromise.fail(r.result().body() + "failed");
            }
        });
    }

    private <T> void sendJinqMessage(Class<?> entityType, EntityManager em, T object, String action, Promise<T> actionPromise) {
        jinqRequest("database-call", em, object, PersistenceOption.createCRUDOption(action, entityType), r -> {
            if (r.succeeded()) {
                Message<T> msg;
                msg = r.result();
                actionPromise.complete(msg.body());
            } else {
                actionPromise.fail(r.result().body() + "failed");
            }
        });
    }
}

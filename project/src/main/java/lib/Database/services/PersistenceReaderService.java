package lib.Database.services;

import lib.Database.persistenceModel.MetaInfPersistence;

import javax.xml.bind.JAXBException;
import java.io.IOException;

/**
 * The interface Persistence reader service.
 */
public interface PersistenceReaderService {

    /**
     * Read persistence meta inf persistence.
     *
     * @return the meta inf persistence
     * @throws IOException   the io exception
     * @throws JAXBException the jaxb exception
     */
    MetaInfPersistence readPersistence() throws IOException, JAXBException;
}

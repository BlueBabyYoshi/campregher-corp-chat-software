package lib.verticle.deploy;

import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Verticle deployer.
 */
public class VerticleDeployer {

    /**
     * Deploy verticles.
     *
     * @param vertxInstance the vertx instance
     * @param verticles     the verticles
     * @return the future
     */
    public static Future<?> DeployVerticles(Vertx vertxInstance, Collection<IVerticleDeployInformation> verticles) {

        var promise = Promise.promise();
        List<Future> futures = verticles.stream()
                .map(r -> deployVerticle(vertxInstance, r))
                .collect(Collectors.toList());
        CompositeFuture.all(futures)
                .setHandler(r -> promise.complete());
        return promise.future();
    }

    private static Future deployVerticle(Vertx vertxInstance, IVerticleDeployInformation verticle) {

        var promise = Promise.promise();
        vertxInstance.deployVerticle(verticle.getInstance(), result -> {
            if (result.succeeded()) {
                System.out.println("deployed verticle: " + verticle.getName());
            } else {
                System.out.println("error deploying verticle: " + verticle.getName());
            }
            promise.complete();
        });
        return promise.future();
    }
}

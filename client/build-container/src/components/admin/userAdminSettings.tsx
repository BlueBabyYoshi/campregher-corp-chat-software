import MUIDataTable from "mui-datatables";
import * as React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import TextField from "@material-ui/core/TextField";
import Avatar from "@material-ui/core/Avatar";
import SettingsIcon from "@material-ui/icons/Settings";
import DeleteIcon from '@material-ui/icons/Delete';
import {UserInfoDTO} from "../../Dtos/UserInfoDTO";
import {AllUsersState} from "./AllUsersState";
import { confirmAlert } from 'react-confirm-alert'; // Import
import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    IconButton
} from "@material-ui/core";

//import { observable, computed } from "mobx";

class userAdminSettings extends React.Component {

  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
  }

  public state: AllUsersState = {
    users: [],
    isLoading: false
  }

  public async componentDidMount(): Promise<void> {
    this.setState({...this.state, isLoading: true});
    try {
      let response = await fetch("http://localhost:8080/allUsers", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "GET",
        mode: "cors",
        credentials: "include"
      });
      let jsonResult = await response.json();
      let jsonObject: UserInfoDTO = JSON.parse(JSON.stringify(jsonResult));
      this.setState({...this.state, users: jsonObject, isLoading: false});
    } catch(e) {
      console.log("All Users Error");
      this.setState({
        ...this.state,
        result: "Fehler bei Webserverabruf."
      });
      console.log(e);
    }
  }

  private SettingsDialog() {
      const [open, setOpen] = React.useState(false);

      const handleClickOpen = () => {
          setOpen(true);
      };

      const handleClose = () => {
          setOpen(false);
      };

      return (
          <div>
              <IconButton
                  aria-label="delete"
                  color="primary"
                  onClick={handleClickOpen}
              >
                  <SettingsIcon />
              </IconButton>

              <Dialog
                  open={open}
                  aria-labelledby="form-dialog-title"
              >
                  <DialogTitle id="form-dialog-title">Benutzer bearbeiten</DialogTitle>
                  <DialogContent>
                      <TextField
                          autoFocus
                          margin="dense"
                          id="name"
                          label="Name"
                          fullWidth
                      />
                      <TextField
                          autoFocus
                          margin="dense"
                          id="emailAddress"
                          label="Email-Adresse"
                          fullWidth
                      />
                      <TextField
                          autoFocus
                          margin="dense"
                          id="office"
                          label="Büro"
                          fullWidth
                      />
                  </DialogContent>
                  <DialogActions>
                      <Button onClick={handleClose} color="primary">
                          Abbrechen
                      </Button>
                      <Button onClick={handleClose} color="primary">
                          Speichern
                      </Button>
                  </DialogActions>
              </Dialog>
          </div>
      );
  }

    private columns = [
        {
            name: "image",
            label: "Avatar",
            options: {
                filter: false,
                customBodyRender: (value, tableMeta, updateValue) => {
                    return <Avatar alt="Profile" src={value} />;
                }
            }
        },
        {
            name: "username",
            label: "Username",
            options: {
                filter: true
            }
        },
        {
            name: "name",
            label: "Name",
            options: {
                filter: true
            }
        },
        {
            name: "emailAddress",
            label: "Email-Adresse",
            options: {
                filter: true,
            }
        },
        {
            name: "isAdmin",
            label: "Admin",
            options: {
                filter: true,
                customBodyRender: (value) => {
                    return <Checkbox checked={value}/>;
                }
            }
        },
        {
            name: "isUser",
            label: "User",
            options: {
                filter: true,
                customBodyRender: (value) => {
                    return <Checkbox checked={value} />;
                }
            }
        },
        {
            name: "department",
            label: "Abteilung",
            options: {
                filter: true,
            }
        },
        {
            name: "office",
            label: "Büro",
            options: {
                filter: true,
            }
        },
        {
            name: "emailFrequency",
            label: "Emailversand",
            options: {
                filter: true,
            }
        },
        {
            name: "username",
            label: "Settings",
            options: {
                customBodyRender: (value, tableMeta, updateValue) => {
                    function f() {
                        alert(value);
                    };
                    return (
                        <div>
                            <IconButton
                                aria-label="delete"
                                color="primary"
                                onClick={f}
                            >
                                <SettingsIcon />
                            </IconButton>
                        </div>
                    );
                }
            }
        },
        {
            name: "username",
            label: "Löschen",
            options: {
                alertDialog: (value) => {
                    const [open, setOpen] = React.useState(false);
                    const handleClickOpen = () => {
                        setOpen(true);
                    };
                    const handleClose = () => {
                        setOpen(false);
                    };
                    return (
                        <div>
                            <IconButton
                                aria-label="delete"
                                color="primary"
                                onClick={handleClickOpen}
                            >
                                <DeleteIcon />
                            </IconButton>
                        <Dialog
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                        >
                        <DialogTitle id="alert-dialog-title">{"Use Google's location service?"}</DialogTitle>
                        <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                        Let Google help apps determine location. This means sending anonymous location data to
                        Google, even when no apps are running.
                        </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                        <Button onClick={handleClose} color="primary">
                        Disagree
                        </Button>
                        <Button onClick={handleClose} color="primary" autoFocus>
                        Agree
                        </Button>
                        </DialogActions>
                        </Dialog>
                        </div>
                        );
                }
                /*
                customBodyRender: (value, tableMeta, updateValue) => {
                    async function f() {
                        let response = await fetch("http://localhost:8080/deleteUser", {
                            headers: {
                                "Content-Type": "application/json",
                            },
                            method: "POST",
                            body: JSON.stringify(value),
                            mode: "cors",
                            credentials: "include"
                        });
                    };

                    return (
                        <div>
                            <IconButton
                                aria-label="delete"
                                color="primary"
                                onClick={AlertDialog}
                            >
                                <DeleteIcon />
                            </IconButton>
                        </div>
                    );
                }

                     */
            }
        }

    ];

    private options = {
        responsive: "scrollMaxHeight",
        filter: true,
        filterType: "dropdown",
        selectableRows: "none"
    };


  render() {
    if (this.state.isLoading) {
      return <p>Loading ...</p>;
    }
    return (
        <div style={{ padding: "50px" }}>
          <MUIDataTable
              aria-label="simple table"
              columns={this.columns}
              options={this.options}
              data={this.state.users}
          />
        </div>
    );
  }
}
export default userAdminSettings;

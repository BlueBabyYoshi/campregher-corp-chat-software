package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.channels.dtos.ChannelEditDTO;

/**
 * The EditChannelHandler gets channelName, isPublic and isLocked flag via REst Call, reloads the channel from DB via ID and we check if current user is mod and thus allowed to change the settings we then apply the settings and
 * save the channel
 */
@HttpHandler(endpoint = HandlerEndpoints.Channels.EDIT_CHANEL, method = HttpMethod.POST, interfaceType = IEditChannelHandler.class)
public class EditChannelHandler extends ChannelHandler implements IEditChannelHandler {

    @Inject
    private DatabaseHandler handler;

    @Override
    public void handle(RoutingContext context) {
        var newChannelEdit = getGsonService().fromJson(context.getBodyAsString(), ChannelEditDTO.class);
        reloadById(Channel.class, newChannelEdit.getChannelId(), getQryModifier()).setHandler(res -> {
            if (res.succeeded()) {
                var channel = res.result();
                if (!channel.isUserMod(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to edit channel");
                    return;
                }
                channel.setPublic(newChannelEdit.isPublic());
                channel.setName(newChannelEdit.getName());
                channel.setLocked(newChannelEdit.isLocked());
                handler.saveOrUpdateObject(Channel.class, channel).setHandler(saveRes ->
                        handleChannelCompleted(saveRes, context)
                );
            }
        });
    }
}

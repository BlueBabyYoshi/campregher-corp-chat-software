package lib.services.LoggingService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Logging service.
 */
public class LoggingService implements ILoggingService {
    private static final String LOGGER_NAME = "PrototypeLogger";
    private final Logger logger = LoggerFactory.getLogger(LOGGER_NAME);

    /**
     * Instantiates a new Logging service.
     */
    public LoggingService() {
        logDebug("Initializing logger...");
        logDebug("Logger initialized");
    }

    @Override
    public void logInfo(String msg) {
        logger.info(msg);
    }

    @Override
    public void logError(String msg, Exception ex) {
        logger.error(msg, ex);
    }

    @Override
    public void logDebug(String msg) {
        logger.debug(msg);
    }

    @Override
    public void logWarn(String msg) {
        logger.warn(msg);
    }
}


export interface UserInfoDTO {
    username: string;
    name: string;
    emailAddress: string;
    isAdmin: boolean;
    isUser: boolean;
    department: string;
    office: string;
    emailFrequency: string;
    isActiveUser: boolean;
}
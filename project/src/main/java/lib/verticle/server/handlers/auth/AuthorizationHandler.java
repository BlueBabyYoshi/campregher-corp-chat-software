package lib.verticle.server.handlers.auth;

import com.google.gson.Gson;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;
import lib.verticle.server.dtos.AuthorizationDTO;
import lib.verticle.server.dtos.AuthorizationResultDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

@HttpHandler(endpoint = HandlerEndpoints.Authorization.CHECK_AUTH, method = HttpMethod.POST, interfaceType = IAuthorizationHandler.class)
public class AuthorizationHandler extends AbstractHttpHandler implements IAuthorizationHandler {

    /**
     * this method checks if a user is authenticated and authorized
     *
     * @param context
     */
    @Override
    public void handle(RoutingContext context) {
        var gson = new Gson();
        AuthorizationDTO authInfo = gson.fromJson(context.getBodyAsString(), AuthorizationDTO.class);
        AuthorizationResultDTO resultDTO = new AuthorizationResultDTO();
        // user is authenticated
        if (context.session().id().equals(authInfo.getSessionId())) {
            System.out.println("user is authenticated");
            resultDTO.setAuthenticated(true);
            // if no role is required user is authorized
            if (authInfo.getRole() == null || authInfo.getRole().isBlank()) {
                System.out.println("user is authorized since no role is required");
                resultDTO.setIsAuthorized(true);
                writeResponseToContext(context, resultDTO);
            }
            // authorization has to be checked for required role
            else {
                User userToAuthorize = context.session().get("user");
                System.out.println("context session user principals: " + userToAuthorize.principal());
                // always put "role:" statement before role
                userToAuthorize.isAuthorized("role:" + authInfo.getRole(), res -> {
                    if (res.succeeded()) {
                        boolean hasAuthority = res.result();
                        System.out.println("user is authorized for role " + authInfo.getRole() + ": " + hasAuthority);
                        resultDTO.setIsAuthorized(hasAuthority);
                    } else {
                        res.cause().printStackTrace();
                        resultDTO.setIsAuthorized(false);
                    }
                    writeResponseToContext(context, resultDTO);
                });
            }
        }
        // user is not authenticated
        else {
            System.out.println("user is not authenticated");
            context.session().destroy();
            resultDTO.setAuthenticated(false);
            resultDTO.setIsAuthorized(false);
            writeResponseToContext(context, resultDTO);
        }
    }

    private void writeResponseToContext(RoutingContext context, AuthorizationResultDTO dto) {
        var gson = new Gson();
        var resultAsString = gson.toJson(dto);
        System.out.println(resultAsString);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

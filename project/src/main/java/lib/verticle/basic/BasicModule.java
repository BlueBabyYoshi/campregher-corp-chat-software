package lib.verticle.basic;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import lib.Database.datasources.DataSourceFactory;
import lib.Database.datasources.DataSourceFactoryImpl;
import lib.Database.services.IQueryService;
import lib.Database.services.PersistenceReaderService;
import lib.Database.services.PersistenceReaderServiceImpl;
import lib.Database.services.QueryService;
import lib.services.ConfigService.ConfigService;
import lib.services.ConfigService.ConfigServiceImpl;
import lib.services.GsonService.GsonService;
import lib.services.GsonService.GsonServiceImpl;
import lib.services.LoggingService.ILoggingService;
import lib.services.LoggingService.LoggingService;
import lib.services.fileupload.FileUploadService;
import lib.services.fileupload.IFileUploadService;
import lib.verticle.Database.DBVerticle;
import lib.verticle.Database.IDBVerticle;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.Database.handlers.DatabaseHandlerImpl;
import lib.verticle.server.handlers.ExampleHandler;
import lib.verticle.server.handlers.IExampleHandler;
import lib.verticle.server.handlers.auth.*;
import lib.verticle.server.handlers.channels.*;
import lib.verticle.server.handlers.channels.notUsedAnymore.*;
import lib.verticle.server.handlers.fileUpload.GetMessageAttachmentHandler;
import lib.verticle.server.handlers.fileUpload.IGetMessageAttachmentHandler;
import lib.verticle.server.handlers.fileUpload.IUploadAttachmentToMessageHandler;
import lib.verticle.server.handlers.fileUpload.UploadAttachmentToMessageHandler;
import lib.verticle.server.handlers.messages.*;
import lib.verticle.server.handlers.users.*;

/**
 * The type Basic module, basic configuration for GUICE Depenency Injection.
 */
public class BasicModule extends AbstractModule {

    private Vertx vertx;

    /**
     * Instantiates a new Basic module and sets the vertx instance for the application.
     *
     * @param vertxInstance the vertx instance
     */
    public BasicModule(Vertx vertxInstance) {
        vertx = vertxInstance;
    }

    @Override
    protected void configure() {

        /* if you create a Handler do bind the Interface and Implementation */

        bind(DatabaseHandler.class).to(DatabaseHandlerImpl.class);
        bind(IDBVerticle.class).to(DBVerticle.class);
        bind(IExampleHandler.class).to(ExampleHandler.class);
        bind(IGetAllUsersHandler.class).to(GetAllUsersHandler.class);
        bind(EventBus.class).toInstance(vertx.eventBus());
        bind(Vertx.class).toInstance(vertx);
        bind(ILoginHandler.class).to(LoginHandler.class);
        bind(IAuthorizationHandler.class).to(AuthorizationHandler.class);
        bind(DataSourceFactory.class).to(DataSourceFactoryImpl.class);
        bind(PersistenceReaderService.class).to(PersistenceReaderServiceImpl.class);
        bind(IGetAllChannelsForUserHandler.class).to(GetAllChannelsForUserHandler.class);
        bind(ILogoutHandler.class).to(LogoutHandler.class);
        bind(IFileUploadService.class).to(FileUploadService.class);
        bind(IGetMessagesHandler.class).to(GetMessagesHandler.class);
        bind(IResetPasswordHandler.class).to(ResetPasswordHandler.class);
        bind(IQueryService.class).to(QueryService.class);
        bind(ISetMessageHandler.class).to(SetMessageHandler.class);
        bind(IDeleteMessageHandler.class).to(DeleteMessageHandler.class);
        bind(INewUserHandler.class).to(NewUserHandler.class);
        bind(IUserInfoHandler.class).to(UserInfoHandler.class);
        bind(IEditUserHandler.class).to(EditUserHandler.class);
        bind(IRemoveUserFromChannelHandler.class).to(RemoveUserFromChannelHandler.class);
        bind(IAddUserToChannelHandler.class).to(AddUserToChannelHandler.class);
        bind(IAddUserToChannelModeratorsHandler.class).to(AddUserToChannelModeratorsHandler.class);
        bind(IRemoveUserFromChannelModeratorsHandler.class).to(RemoveUsersFromChannelModeratorsHandler.class);
        bind(INewChannelHandler.class).to(NewChannelHandler.class);
        bind(IDeleteChannelHandler.class).to(DeleteChannelHandler.class);
        bind(IEditChannelHandler.class).to(EditChannelHandler.class);
        bind(GsonService.class).to(GsonServiceImpl.class).in(Singleton.class);
        bind(ILockChannelHandler.class).to(LockChannelHandler.class);
        bind(ConfigService.class).to(ConfigServiceImpl.class).in(Singleton.class);
        bind(IDeleteUserHandler.class).to(DeleteUserHandler.class);
        bind(IEditMessageHandler.class).to(EditMessageHandler.class);
        bind(IGetChannelSettingsHandler.class).to(GetChannelSettingsHandler.class);
        bind(IEditChannelUsersHandler.class).to(EditChannelUsersHandler.class);
        bind(IEditChannelModeratorsHandler.class).to(EditChannelModeratorsHandler.class);
        bind(IUploadAttachmentToMessageHandler.class).to(UploadAttachmentToMessageHandler.class);
        bind(IGetMessageAttachmentHandler.class).to(GetMessageAttachmentHandler.class);
        bind(ILoggingService.class).to(LoggingService.class).in(Singleton.class);
    }
}

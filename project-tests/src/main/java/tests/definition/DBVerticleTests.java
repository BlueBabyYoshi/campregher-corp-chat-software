package tests.definition;

import lib.model.Department;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runners.MethodSorters;
import tests.base.testdefinition.AbstractVertxTest;

import java.util.concurrent.atomic.AtomicReference;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * The type Db verticle tests.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DBVerticleTests extends AbstractVertxTest {

    /**
     * Sets database.
     */
    @BeforeEach
    public void setupDatabase() {
        clearDatabaseFrom(User.class);
    }


    /**
     * Test logical delete.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testLogicalDelete() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        createUserSaveOrUpdate(handler);
        var user = assertUserExists(handler);
        runInContext(ctx -> {
            handler.deleteObject(User.class, user);
        }, 5);
        runAndVerifyInContext(ctx -> handler.readObjects(User.class), res -> {
            assertThat(res.result()).isEmpty();
        });
    }

    /**
     * Test save object.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testSaveObject() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        runInContext(ctx -> {
            var entity = new User();
            entity.setName("Test");
            entity.setActiveUser(true);
            return handler.saveObject(User.class, entity);
        });
        assertUserExists(handler);
    }


    /**
     * Test save or update object.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void testSaveOrUpdateObject() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        createUserSaveOrUpdate(handler);
        assertUserExists(handler);
    }

    private User assertUserExists(DatabaseHandler handler) throws Throwable {
        AtomicReference<User> userRef = new AtomicReference<>();
        runAndVerifyInContext(ctx -> handler.readObjects(User.class), res -> {
            assertThat(res.result()).as("Returned List of entities is not null").isNotNull();
            assertThat(res.result()).as("List of test entities should have elements").isNotEmpty();
            var first = res.result().stream()
                    .findFirst().orElse(null);
            assertThat(first).isNotNull();
            assertThat(first.getName()).isEqualTo("Test");
            assertThat(first.getActiveUser()).isTrue();
            userRef.set(first);
        });
        return userRef.get();
    }

    private void createUserSaveOrUpdate(DatabaseHandler handler) throws Throwable {
        runInContext(ctx -> {
            var entity = new User();
            entity.setName("Test");
            entity.setDepartment(Department.HR);
            entity.setActiveUser(true);
            return handler.saveOrUpdateObject(User.class, entity);
        });
    }
}

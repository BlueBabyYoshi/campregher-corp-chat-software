package lib.model;

/**
 * The enum User roles.
 */
public enum UserRoles {
    Admin,
    User,
    Moderator
}

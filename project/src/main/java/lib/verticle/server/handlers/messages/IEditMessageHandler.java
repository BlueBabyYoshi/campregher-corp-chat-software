package lib.verticle.server.handlers.messages;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Edit message handler.
 */
public interface IEditMessageHandler extends IHttpHandler {
}

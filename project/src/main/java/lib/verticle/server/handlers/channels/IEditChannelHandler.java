package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Edit channel handler.
 */
public interface IEditChannelHandler extends IHttpHandler {
}

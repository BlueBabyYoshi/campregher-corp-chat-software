package lib.model;

/**
 * The enum Channel status.
 */
public enum ChannelStatus {
    Active,
    Locked,
    Deleted
}

package tests.base.delegates;

import io.vertx.core.AsyncResult;

/**
 * The interface Test context verify.
 *
 * @param <T> the type parameter
 */
public interface TestContextVerify<T> {

    /**
     * Verify.
     *
     * @param resultToVerify the result to verify
     */
    void verify(AsyncResult<T> resultToVerify);
}

package lib.verticle.server.handlers.users;

import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.User;
import lib.model.UserRole;
import lib.verticle.server.dtos.UserInfoDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

/**
 * The User info handler is called by a user to see her/his profile
 * or by an admin to see the user's profile
 * this handler sends all required information about a user to the frontend
 */
@HttpHandler(endpoint = HandlerEndpoints.Users.USER_INFO, method = HttpMethod.POST, interfaceType = IUserInfoHandler.class)
public class UserInfoHandler extends AbstractHttpHandler implements IUserInfoHandler {

    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        queryService.getUserByUsername(context.session().get("username")).setHandler(checkUser -> {
            if (checkUser.succeeded()) {
                User requestingUser = checkUser.result();
                String usernameToRead = context.getBodyAsString();
                // check if user is allowed to request info about another user
                if (!usernameToRead.isBlank() && !usernameToRead.equals(context.session().get("username")) && !requestingUser.hasRole("Admin")) {
                    System.out.println("WARNING: unauthorized user wanted to request information about another user");
                    return;
                }
                // if username is "" user wants to request her/his information
                if (usernameToRead.isBlank()) usernameToRead = context.session().get("username");
                queryService.getUserByUsername(usernameToRead).setHandler(readUser -> {
                    if (readUser.succeeded()) {
                        User user = readUser.result();
                        userInfoDTO.setUsername(user.getUsername());
                        userInfoDTO.setName(user.getName());
                        userInfoDTO.setEmailAddress(user.getEmailAddress());
                        userInfoDTO.setDepartment(user.getDepartment().toString());
                        userInfoDTO.setOffice(user.getOffice());
                        userInfoDTO.setEmailFrequency(user.getEmailFrequency().toString());
                        userInfoDTO.setActiveUser(user.getActiveUser());
                        if (user.getUsername().equals(context.session().get("username"))) userInfoDTO.setSameUser(true);
                        userInfoDTO.setAdmin(false);
                        userInfoDTO.setUser(false);
                        for (UserRole userRole : user.getRoles()) {
                            if (userRole.getRoleName().equals("Admin")) userInfoDTO.setAdmin(true);
                            if (userRole.getRoleName().equals("User")) userInfoDTO.setUser(true);
                        }
                        System.out.println("User info for requested user: " + userInfoDTO.toString());
                        writeResponseToContext(context, userInfoDTO);
                    }
                });
            }
        });
    }

    private void writeResponseToContext(RoutingContext context, UserInfoDTO dto) {
        var gson = new Gson();
        var resultAsString = gson.toJson(dto);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

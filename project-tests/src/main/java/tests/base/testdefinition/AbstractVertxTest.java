package tests.base.testdefinition;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.junit5.VertxTestContext;
import lib.Database.Connections;
import lib.Database.Persistence;
import lib.services.GsonService.GsonService;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.basic.BasicModule;
import lib.verticle.deploy.VerticleDeployer;
import lib.verticle.deploy.Verticles;
import tests.base.delegates.TestContextRunning;
import tests.base.delegates.TestContextRunningWithResult;
import tests.base.delegates.TestContextVerify;
import tests.base.exceptions.NotAllVerticlesDeployedException;
import tests.base.services.TestRestClient;

import java.util.concurrent.TimeUnit;

/**
 * The type Abstract vertx test.
 */
public abstract class AbstractVertxTest extends AbstractTest {

    /**
     * The constant BASE_URL.
     */
    protected static final String BASE_URL = "http://localhost:8080";
    private static final int TIMEOUT = 30;
    /**
     * Set this to lowest estimation of server starttime because the context will wait for this long before continuing tests.
     * if this is set too low tests will fail because server is not up
     */
    private static final int PREDICTED_SERVER_START_TIME_MAX = 10;

    private static Vertx server;

    private static Injector injector;

    private static TestRestClient restClient;

    /**
     * Gets injector.
     *
     * @return the injector
     */
    protected static Injector getInjector() {
        return injector;
    }

    /**
     * Gets rest client.
     *
     * @return the rest client
     */
    public static TestRestClient getRestClient() {
        return restClient;
    }

    @Override
    protected void setup() throws Throwable {
        bootServer();
    }

    /**
     * Run in context.
     *
     * @param running the running
     * @throws Throwable the throwable
     */
    protected void runInContext(TestContextRunning running) throws Throwable {
        runInContext(running, TIMEOUT);
    }

    /**
     * Run in context.
     *
     * @param <T>     the type parameter
     * @param running the running
     * @throws Throwable the throwable
     */
    protected <T> void runInContext(TestContextRunningWithResult<T> running) throws Throwable {
        runInContext(running, TIMEOUT);
    }

    /**
     * Run and verify in context.
     *
     * @param <T>            the type parameter
     * @param running        the running
     * @param verifyDelegate the verify delegate
     * @throws Throwable the throwable
     */
    protected <T> void runAndVerifyInContext(TestContextRunningWithResult<T> running, TestContextVerify<T> verifyDelegate) throws Throwable {
        runAndVerifyInContext(running, verifyDelegate, TIMEOUT);
    }

    /**
     * Run and verify in context.
     *
     * @param <T>            the type parameter
     * @param running        the running
     * @param verifyDelegate the verify delegate
     * @param timeout        the timeout
     * @throws Throwable the throwable
     */
    protected <T> void runAndVerifyInContext(TestContextRunningWithResult<T> running, TestContextVerify<T> verifyDelegate, long timeout) throws Throwable {
        var ctx = new VertxTestContext();
        Future<T> future = running.run(ctx);
        future.setHandler(r -> ctx.verify(() -> {
            verifyDelegate.verify(r);
            ctx.completeNow();
        }));
        throwIfFailed(ctx, timeout);
    }

    /**
     * Clear database from future.
     *
     * @param <T>  the type parameter
     * @param type the type
     * @return the future
     */
    protected <T> Future<T> clearDatabaseFrom(Class<T> type) {
        var handler = injector.getInstance(DatabaseHandler.class);
        Promise<T> prom = Promise.promise();
        handler.readObjects(type).setHandler(r -> {
            for (var inst : r.result()) {
                handler.deleteObject(type, inst);
            }
            prom.complete();
        });
        return prom.future();
    }

    /**
     * Run in context.
     *
     * @param running       the running
     * @param secondTimeout the second timeout
     * @throws Throwable the throwable
     */
    protected void runInContext(TestContextRunning running, long secondTimeout) throws Throwable {
        var ctx = new VertxTestContext();
        running.run(ctx);
        throwIfFailed(ctx, secondTimeout);
    }

    /**
     * Run in context.
     *
     * @param <T>           the type parameter
     * @param running       the running
     * @param secondTimeout the second timeout
     * @throws Throwable the throwable
     */
    protected <T> void runInContext(TestContextRunningWithResult<T> running, long secondTimeout) throws Throwable {
        var ctx = new VertxTestContext();
        Future<T> future = running.run(ctx);
        future.setHandler(r -> ctx.completeNow());
        throwIfFailed(ctx, secondTimeout);
    }

    /**
     * Throw if failed.
     *
     * @param ctx     the ctx
     * @param timeout the timeout
     * @throws Throwable the throwable
     */
    protected void throwIfFailed(VertxTestContext ctx, long timeout) throws Throwable {
        ctx.awaitCompletion(timeout, TimeUnit.SECONDS);
        if (ctx.failed()) {
            throwPlease(ctx.causeOfFailure());
        }
    }

    private void bootServer() throws Throwable {
        runInContext(ctx -> {
            Persistence.initializeDataBroker(Connections.DEFAULT);
            server = Vertx.vertx();
            injector = Guice.createInjector(new BasicModule(server));
            restClient = new TestRestClient(BASE_URL, injector.getInstance(GsonService.class));
            VerticleDeployer.DeployVerticles(server, Verticles.getAppVerticles(injector)).setHandler(r -> {
                if (!r.succeeded()) {
                    throwPlease(new NotAllVerticlesDeployedException());
                }
                ctx.completeNow();
            });
        }, PREDICTED_SERVER_START_TIME_MAX);
    }
}

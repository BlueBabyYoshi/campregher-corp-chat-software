package lib.services.ConfigService;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * The interface Config service.
 */
public interface ConfigService {

    /**
     * Load properties.
     *
     * @throws IOException        the io exception
     * @throws URISyntaxException the uri syntax exception
     */
    void loadProperties() throws IOException, URISyntaxException;

    /**
     * Sets property.
     *
     * @param key   the key
     * @param value the value
     */
    void setProperty(String key, String value);

    /**
     * Gets property.
     *
     * @param key the key
     * @return the property
     */
    String getProperty(String key);

    /**
     * Gets property or default value.
     *
     * @param key          the key
     * @param defaultValue the default value
     * @return the property
     */
    String getProperty(String key, String defaultValue);

    /**
     * Save properties to file.
     *
     * @throws IOException        the io exception
     * @throws URISyntaxException the uri syntax exception
     */
    void saveProperties() throws IOException, URISyntaxException;
}

package lib.verticle.server.handlers.channels.dtos;

public class ChannelIdDTO {

    private int channelId;

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}

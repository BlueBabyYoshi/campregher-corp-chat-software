package lib.verticle.server.handlers.channels.notUsedAnymore;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Add user to channel handler.
 */
public interface IAddUserToChannelHandler extends IHttpHandler {
}

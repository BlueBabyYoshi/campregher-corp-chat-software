package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.channels.dtos.ChannelIdDTO;

/**
 * The LockChannelHandler gets Channelid via Rest Call and checks if user is allowed to change the setting, if so sets the isLocked flag to true.
 */
@HttpHandler(endpoint = HandlerEndpoints.Channels.LOCK_CHANNEL, method = HttpMethod.POST, interfaceType = ILockChannelHandler.class)
public class LockChannelHandler extends ChannelHandler implements ILockChannelHandler {

    @Inject
    private DatabaseHandler handler;

    @Override
    public void handle(RoutingContext context) {
        var channelIdDTO = getGsonService().fromJson(context.getBodyAsString(), ChannelIdDTO.class);
        reloadById(Channel.class, channelIdDTO.getChannelId(), getQryModifier()).setHandler(result -> {
            if (result.succeeded()) {
                var channel = result.result();
                if (!channel.isUserMod(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to lock channel");
                    return;
                }
                channel.setLocked(true);
                handler.saveOrUpdateObject(Channel.class, channel).setHandler(r -> handleChannelCompleted(r, context));
            }
        });
    }
}

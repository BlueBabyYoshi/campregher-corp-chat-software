package lib.verticle.server.handlers;

import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;


/**
 * The type Example handler.
 */
@HttpHandler(endpoint = "/example", method = HttpMethod.POST, interfaceType = IExampleHandler.class)
public class ExampleHandler extends AbstractHttpHandler implements IExampleHandler {

    @Override
    public void handle(RoutingContext context) {

        System.out.println("Example Post was called!");
        context.response().end("Answer for example post rest call!");
    }
}

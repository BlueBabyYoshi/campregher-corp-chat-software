package lib.verticle.Database.handlers;

import io.vertx.core.Future;
import org.jinq.jpa.JPAJinqStream;

import java.util.List;

/**
 * The interface Database handler.
 */
public interface DatabaseHandler {

    /**
     * Save object.
     *
     * @param <T>    the type parameter
     * @param type   the type
     * @param object the object
     * @return the future of the saved object
     */
    <T> Future<T> saveObject(Class<T> type, T object);

    /**
     * Save or update object.
     *
     * @param <T>    the type parameter
     * @param type   the type
     * @param object the object
     * @return the future of the saved object
     */
    <T> Future<T> saveOrUpdateObject(Class<T> type, T object);

    /**
     * Delete object.
     *
     * @param <T>    the type parameter
     * @param type   the type
     * @param object the object
     * @return the future of the deleted object
     */
    <T> Future<T> deleteObject(Class<T> type, T object);

    /**
     * Read object future.
     *
     * @param <T>    the type parameter
     * @param type   the type
     * @param object the object
     * @return the future of the to be read object
     */
    <T> Future<T> readObject(Class<T> type, T object);

    /**
     * Read objects.
     *
     * @param <T>         the type parameter
     * @param objectClass the object class
     * @return the future of the to be read objects
     */
    <T> Future<List<T>> readObjects(Class<T> objectClass);

    /**
     * Jinq query.
     *
     * @param <T>         the type parameter
     * @param objectClass the object class
     * @return the future of the jinq query
     */
    <T> Future<JPAJinqStream<T>> jinqQuery(Class<T> objectClass);
}

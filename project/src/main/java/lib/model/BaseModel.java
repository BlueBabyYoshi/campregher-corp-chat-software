package lib.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The type Base model.
 */
@MappedSuperclass
public class BaseModel implements Serializable {

    protected int id;
    private boolean isDeleted;

    @Override
    public boolean equals(Object obj) {
        if (obj != null && getClass() == obj.getClass()) {
            return getId() == ((BaseModel) obj).getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        return 7 + 5 * getId(); // 5 and 7 are random prime numbers
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
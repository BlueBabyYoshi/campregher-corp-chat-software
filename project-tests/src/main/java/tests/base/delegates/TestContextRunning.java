package tests.base.delegates;

import io.vertx.junit5.VertxTestContext;

/**
 * The interface Test context running.
 */
public interface TestContextRunning {

    /**
     * Run.
     *
     * @param ctx the ctx
     */
    void run(VertxTestContext ctx);
}

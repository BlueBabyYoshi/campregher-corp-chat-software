package lib.verticle.server.dtos;

public class MessageDTO {
    //test only with text same in Frontend with DTO
    private int id;
    private String text;
    private String username;
    private boolean myMessage;
    private int channelId;


    public MessageDTO(int id, String text, String username, boolean myMessage, int channelId) {
        this.id = id;
        this.text = text;
        this.username = username;
        this.myMessage = myMessage;
        this.channelId = channelId;
    }

    public MessageDTO() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isMyMessage() {
        return myMessage;
    }

    public void setMyMessage(boolean myMessage) {
        this.myMessage = myMessage;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}

package lib.verticle.server.handlers.auth;

import com.google.gson.Gson;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;

/**
 * Handler for logout. It takes no input, removes contend of session and destroys session.
 */
@HttpHandler(endpoint = HandlerEndpoints.Authorization.LOGOUT, method = HttpMethod.POST, interfaceType = ILogoutHandler.class)
public class LogoutHandler extends AbstractHttpHandler implements ILogoutHandler {

    @Override
    public void handle(RoutingContext context) {
        if (!context.session().isDestroyed()) {
            context.session().remove("user");
            context.session().remove("username");
            context.clearUser();
            context.session().destroy();
            writeResponseToContext(context, true);
            System.out.println("Logout erfolgreich");
        } else {
            System.out.println("Logout fehlgeschlagen");
            writeResponseToContext(context, false);
        }

    }

    private void writeResponseToContext(RoutingContext context, boolean bool) {
        var gson = new Gson();
        var resultAsString = gson.toJson(bool);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

package lib.verticle.server.handlers.fileUpload.dtos;

public class MessageIdDTO {

    private int messageId;

    public int getMessageId() {
        return messageId;
    }

    public void setMessageId(int messageId) {
        this.messageId = messageId;
    }
}

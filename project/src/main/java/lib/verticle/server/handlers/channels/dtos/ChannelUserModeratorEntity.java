package lib.verticle.server.handlers.channels.dtos;

public class ChannelUserModeratorEntity {

    private int userId;
    private String username;
    private boolean isModerator;
    private boolean isUser;

    public ChannelUserModeratorEntity(int userId, String username, boolean isModerator, boolean isUser) {
        this.userId = userId;
        this.username = username;
        this.isModerator = isModerator;
        this.isUser = isUser;
    }

    public ChannelUserModeratorEntity(int userId, String username) {
        this.userId = userId;
        this.username = username;
    }

    public ChannelUserModeratorEntity() {

    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isModerator() {
        return isModerator;
    }

    public void setModerator(boolean moderator) {
        isModerator = moderator;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean user) {
        isUser = user;
    }
}

package lib.verticle.server.dtos;

/**
 * this class is used by:
 * UserInfoHandler
 * EditUserHandler
 */
public class UserInfoDTO {
    private String username;
    private String name;
    private String emailAddress;
    private boolean isAdmin;
    private boolean isUser;
    private String department;
    private String office;
    private String emailFrequency;
    private boolean isActiveUser;
    private boolean isSameUser;

    public UserInfoDTO() {

    }

    public UserInfoDTO(String username, String name, String emailAddress, boolean isAdmin, boolean isUser, String department, String office, String emailFrequency, boolean isActiveUser) {
        this.username = username;
        this.name = name;
        this.emailAddress = emailAddress;
        this.isAdmin = isAdmin;
        this.isUser = isUser;
        this.department = department;
        this.office = office;
        this.emailFrequency = emailFrequency;
        this.isActiveUser = isActiveUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean user) {
        isUser = user;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getEmailFrequency() {
        return emailFrequency;
    }

    public void setEmailFrequency(String emailFrequency) {
        this.emailFrequency = emailFrequency;
    }

    public boolean isActiveUser() {
        return isActiveUser;
    }

    public void setActiveUser(boolean activeUser) {
        isActiveUser = activeUser;
    }

    public boolean isSameUser() {
        return isSameUser;
    }

    public void setSameUser(boolean sameUser) {
        isSameUser = sameUser;
    }

    @Override
    public String toString() {
        return "User {" +
                "username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", isAdmin='" + isAdmin + '\'' +
                ", isUser='" + isUser + '\'' +
                ", emailFrequency='" + emailFrequency + '\'' +
                ", department='" + department.toString() + '\'' +
                ", office='" + office + '\'' +
                '}';
    }

    public boolean isAnyValueEmpty() {
        return (username.isBlank() || name.isBlank() || emailAddress.isBlank() || office.isBlank());
    }

}

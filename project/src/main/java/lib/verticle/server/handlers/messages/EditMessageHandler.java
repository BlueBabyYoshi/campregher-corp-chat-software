package lib.verticle.server.handlers.messages;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Message;
import lib.model.User;
import lib.services.GsonService.GsonService;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.PushMessageService;
import lib.verticle.server.dtos.EditMessageDTO;
import lib.verticle.server.handlers.HandlerEndpoints;

/**
 * The EditMessageHandler gets messageId and new MessagesTExt send via rest call, retrieves the "old" message and updates it saves it and sends answer to the call.
 */
@HttpHandler(endpoint = HandlerEndpoints.Messages.EDIT_MESSAGE, method = HttpMethod.POST, interfaceType = IEditMessageHandler.class)
public class EditMessageHandler extends MessageHandler implements IEditMessageHandler {

    @Inject
    private IQueryService queryService;
    @Inject
    private DatabaseHandler handler;
    @Inject
    private GsonService gsonService;
    @Inject
    private PushMessageService pushMessageService;
    // private AuthenticationController controller;

    @Override
    public void handle(RoutingContext context) {

        var messageDTO = gsonService.fromJson(context.getBodyAsString(), EditMessageDTO.class);
        queryService.getMessageById(messageDTO.getId()).setHandler(getMessage -> {
            if (getMessage.succeeded()) {
                Message message = getMessage.result();
                User fromUser = message.getFromUser();
                // user is only allowed to edit her/his own message
                if (!fromUser.getUsername().equals(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to edit a message of another user");
                    return;
                }
                message.setText(messageDTO.getText());
                handler.saveOrUpdateObject(Message.class, message).setHandler(end -> context.response().end());
            }
        });
    }
}

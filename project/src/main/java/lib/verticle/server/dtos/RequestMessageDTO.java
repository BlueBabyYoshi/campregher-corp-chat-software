package lib.verticle.server.dtos;

public class RequestMessageDTO {

    private int channelId;

    public RequestMessageDTO(int channelId) {
        this.channelId = channelId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}

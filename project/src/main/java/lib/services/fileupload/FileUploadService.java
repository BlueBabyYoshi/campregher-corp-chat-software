package lib.services.fileupload;

import com.google.inject.Inject;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import lib.Database.AsyncJinq;
import lib.model.Message;
import lib.model.fileUpload.UploadedFile;
import lib.model.fileUpload.UploadedFileContent;
import lib.model.fileUpload.UploadedFileExtension;
import lib.model.fileUpload.UploadedFileType;
import lib.verticle.Database.handlers.DatabaseHandler;
import org.apache.commons.io.FilenameUtils;
import org.jinq.orm.stream.JinqStream;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * The File upload service implementation.
 */
public class FileUploadService implements IFileUploadService {

    @Inject
    private DatabaseHandler handler;
    @Inject
    private Vertx vertx;

    private Map<String, UploadedFileExtension> fileExtensionsMap = initialiseExtensionMap();
    private Map<UploadedFileExtension, UploadedFileType> fileTypeMap = initialiseTypeMap();

    private static Map<String, UploadedFileExtension> initialiseExtensionMap() {
        var map = new HashMap<String, UploadedFileExtension>();
        for (var val : UploadedFileExtension.values()) {
            var lowerCaseText = val.toString();
            lowerCaseText = lowerCaseText.toLowerCase();
            map.put(lowerCaseText, val);
        }
        return map;
    }

    private static Map<UploadedFileExtension, UploadedFileType> initialiseTypeMap() {
        var map = new HashMap<UploadedFileExtension, UploadedFileType>();
        map.put(UploadedFileExtension.JPEG, UploadedFileType.Picture);
        map.put(UploadedFileExtension.JPG, UploadedFileType.Picture);
        map.put(UploadedFileExtension.PNG, UploadedFileType.Picture);
        map.put(UploadedFileExtension.TXT, UploadedFileType.Document);
        map.put(UploadedFileExtension.PDF, UploadedFileType.Document);
        map.put(UploadedFileExtension.MP3, UploadedFileType.Audio);
        map.put(UploadedFileExtension.MP4, UploadedFileType.Video);
        return map;
    }

    private String getBasePath() throws URISyntaxException {
        String path = new File(FileUploadService.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParent();
        return Paths.get(path, "UploadedFiles").toString();
    }


    @Override
    public void saveMessageAttachment(Message message, String fileName, byte[] content) throws IOException, URISyntaxException {
        var newFile = new UploadedFile();
        newFile.setFileName(fileName);
        var ext = getFileExtension(fileName);
        newFile.setFileExtension(ext);
        newFile.setFileType(getFileType(ext));
        newFile.setUploadDateTime(LocalDateTime.now());
        newFile.setRelativePath(getRelativePathForWrite(message, fileName));
        newFile.setMessage(message);
        //message.setAttatchment(newFile);
        var filecontent = new UploadedFileContent();
        filecontent.setData(content);
        newFile.setFileContent(filecontent);
        filecontent.setFile(newFile);

        handler.saveObject(UploadedFile.class, newFile);

        var completePath = Paths.get(getBasePath(), newFile.getRelativePath()).toString();

        File file = new File(completePath);
        file.getParentFile().mkdirs();
        file.createNewFile();
        try (FileOutputStream fileOuputStream = new FileOutputStream(completePath)) {
            fileOuputStream.write(content);
        }

    }

    // if it exists we read if from file system, otherwise we read it from the database

    @Override
    public Future<byte[]> readFile(Message message, boolean takeFromFileSystem) throws IOException, URISyntaxException {
        var completePath = Paths.get(getBasePath(), getRelativePathForRead(message)).toString();

        File tempdir = new File(completePath);
        if (tempdir.exists() && takeFromFileSystem) {
            return readFileFromFileSystem(tempdir.getPath());
        } else {
            return readFileFromFileDatabase(message);
        }
    }


    private Future<byte[]> readFileFromFileDatabase(Message message) {
        Promise<byte[]> promise = Promise.promise();
        var messageId = message.getId();
        handler.jinqQuery(UploadedFile.class).setHandler(r -> {
            if (r.succeeded()) {
                JinqStream<UploadedFile> stream = r.result();
                JinqStream<UploadedFile> fileJinqStream = stream.where(file -> file.getMessage().getId() == messageId);
                AsyncJinq.asyncFirstOrDefault(fileJinqStream, vertx).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        promise.complete(r2.result().getFileContent().getData());
                    } else {
                        promise.fail("failed!");
                    }
                });
            } else {
                promise.fail("failed2!");
            }
        });
        return promise.future();
    }

    private Future<byte[]> readFileFromFileSystem(String filePath) {
        Promise<byte[]> promise = Promise.promise();
        vertx.executeBlocking(r -> {
            try {
                var file = actuallyReadFile(filePath);
                r.complete(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }, res -> promise.complete((byte[]) res.result()));
        return promise.future();
    }

    private byte[] actuallyReadFile(String filePath) throws IOException {
        Path fileLocation = Paths.get(filePath);
        return Files.readAllBytes(fileLocation);
    }


    private String getRelativePathForWrite(Message message, String fileName) {
        /*if (message.getInChannel().isSubChannel()) {
            var parentChannelId = message.getInChannel().getParentChannel().getId();
            var channelId = message.getInChannel().getId();
            var userId = message.getFromUser().getId();
            var messageId = message.getId();
            return Paths.get("channels", Integer.toString(parentChannelId), Integer.toString(channelId), Integer.toString(userId), Integer.toString(messageId), fileName).toString();
        } else {*/
        var channelId = message.getInChannel().getId();
        var userId = message.getFromUser().getId();
        var messageId = message.getId();
        return Paths.get("channels", Integer.toString(channelId), Integer.toString(userId), Integer.toString(messageId), fileName).toString();
        //}

    }

    private String getRelativePathForRead(Message message) {
        var channelId = message.getInChannel().getId();
        var userId = message.getFromUser().getId();
        var messageId = message.getId();
        return Paths.get("channels", Integer.toString(channelId), Integer.toString(userId), Integer.toString(messageId), message.getText()).toString();
    }

    private UploadedFileExtension getFileExtension(String fileName) {
        String extension = FilenameUtils.getExtension(fileName);
        return fileExtensionsMap.get(extension);
    }

    private UploadedFileType getFileType(UploadedFileExtension extension) {
        return fileTypeMap.get(extension);
    }
}

package lib.verticle.server.handlers.channels.notUsedAnymore;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Remove user from channel handler.
 */
public interface IRemoveUserFromChannelHandler extends IHttpHandler {
}

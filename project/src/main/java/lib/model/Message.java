package lib.model;

import lib.model.fileUpload.UploadedFile;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * The type Message.
 */
@Entity
@Table(name = "Message")
@Access(AccessType.PROPERTY)
public class Message extends BaseModel {

    private String text;
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
    private User fromUser;
    private Channel inChannel;
    private UploadedFile attatchment;

    public Message() {
    }

    public Message(String text) {
        this.text = text;
    }

    public Message(String text, LocalDateTime createDate, LocalDateTime updateDate, User fromUser, Channel inChannel) {
        this.createDate = createDate;
        this.text = text;
        this.updateDate = updateDate;
        this.fromUser = fromUser;
        this.inChannel = inChannel;
    }

    public Message(String text, LocalDateTime createDate, LocalDateTime updateDate) {
        this.text = text;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public Message(String text, LocalDateTime createDate, LocalDateTime updateDate, User fromUser, Channel inChannel, UploadedFile attatchment) {
        this.text = text;
        this.createDate = createDate;
        this.updateDate = updateDate;
        this.fromUser = fromUser;
        this.inChannel = inChannel;
        this.attatchment = attatchment;
    }

    public Message(int messageid) {
        super();
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(LocalDateTime createDate) {
        this.createDate = createDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    @ManyToOne(cascade = CascadeType.ALL)
    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User fromUser) {
        this.fromUser = fromUser;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public Channel getInChannel() {
        return inChannel;
    }

    public void setInChannel(Channel inChannel) {
        this.inChannel = inChannel;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public UploadedFile getAttatchment() {
        return attatchment;
    }

    public void setAttatchment(UploadedFile attatchment) {
        this.attatchment = attatchment;
    }

}

package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;


/**
 * The NewChannelHandler gets ChannelName via Context and creates a new Channel with Default Settings
 */
@HttpHandler(endpoint = "/newChannel", method = HttpMethod.POST, interfaceType = INewChannelHandler.class)
public class NewChannelHandler extends ChannelHandler implements INewChannelHandler {

    @Inject
    private DatabaseHandler handler;
    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {
        System.out.println("in NewChannelHandler");
        String channelName = context.getBodyAsString();
        queryService.getUserByUsername(context.session().get("username")).setHandler(getUser -> {
            if (getUser.succeeded()) {
                User currentUser = getUser.result();
                Channel newChannel = new Channel();
                newChannel.setPublic(true);
                newChannel.setLocked(false);
                newChannel.setName(channelName);
                newChannel.getModerator().add(currentUser);
                newChannel.getChannelMembers().add(currentUser);
                System.out.println("new channel created: " + channelName);
                handler.saveOrUpdateObject(Channel.class, newChannel).setHandler(r -> handleChannelCompleted(r, context));
            }
        });

    }
}

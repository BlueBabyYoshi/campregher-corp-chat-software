import { Login } from "../components/Login/Login";

export interface LoginResultDTO {
    success: boolean;
    message: string;
}

package lib.verticle.server.handlers.messages;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.model.Message;
import lib.model.User;
import lib.services.GsonService.GsonService;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.dtos.RemoveMessageDTO;
import lib.verticle.server.handlers.HandlerEndpoints;

/**
 * The DeleteMessageHandler gets messageID via Rest call, we check if the current calling user is allowed to delete the message/mark it as deleted
 */
@HttpHandler(endpoint = HandlerEndpoints.Messages.DELETE_MESSAGE, method = HttpMethod.DELETE, interfaceType = IDeleteMessageHandler.class)
public class DeleteMessageHandler extends MessageHandler implements IDeleteMessageHandler {

    @Inject
    private DatabaseHandler handler;
    @Inject
    private GsonService gsonService;
    @Inject
    private IQueryService queryService;

    @Override
    public void handle(RoutingContext context) {
        var messageDTO = gsonService.fromJson(context.getBodyAsString(), RemoveMessageDTO.class);
        queryService.getMessageById(messageDTO.getId()).setHandler(getMessage -> {
            if (getMessage.succeeded()) {
                Message message = getMessage.result();
                User fromUser = message.getFromUser();
                queryService.getChannelById(messageDTO.getChannelId()).setHandler(getChannel -> {
                    if (getChannel.succeeded()) {
                        Channel channel = getChannel.result();
                        // user is only allowed to delete her/his own message of if she/he is mod of the channel in which the message has been sent
                        if (!fromUser.getUsername().equals(context.session().get("username")) && !channel.isUserMod(context.session().get("username"))) {
                            System.out.println("WARNING: unauthorized user wanted to delete a message of another user");
                            return;
                        }
                        handler.deleteObject(Message.class, message).setHandler(end -> context.response().end());
                    }
                });
            }
        });
    }


}

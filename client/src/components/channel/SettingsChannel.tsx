import { ChannelUser } from './ChannelUser';

export interface SettingsChannel {
    channelName: string,
    isPublic: boolean,
    isLocked: boolean,
    userModList: ChannelUser[],
    currentUserIsMod: boolean
}
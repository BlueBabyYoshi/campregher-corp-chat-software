package lib.verticle.server.handlers.users;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Delete user handler.
 */
public interface IDeleteUserHandler extends IHttpHandler {

}

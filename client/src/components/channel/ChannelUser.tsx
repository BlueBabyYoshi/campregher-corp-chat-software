export interface  ChannelUser{
    userId: number,
    username: string,
    isModerator: boolean,
    isUser: boolean
}
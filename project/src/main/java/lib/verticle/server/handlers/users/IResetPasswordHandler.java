package lib.verticle.server.handlers.users;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Reset password handler.
 */
public interface IResetPasswordHandler extends IHttpHandler {
}

import { Message } from "./Message";
import { Channel } from "./Channel";
import { ChannelUser } from './ChannelUser';

export interface State {
   /*------------------ for channels---------------------*/ 
   //list channels 
   channels: Channel[];
   channelsTypeSelection: Channel[];
   selectedChannelId: number;

    //channel settings  
    channelName: string;
    isChannelPublic: boolean;
    isChannelLocked: boolean;
    channelUsers: ChannelUser[];
    currentUserIsMod: boolean;


    /*----------------- for messages ---------------------*/

   //list messages 
   messages: Message[];

   //edit messages 
   editMessageId: number;
   editMessageText: string;
   editMessageChannelId: number;
   deleteMessageId: number;
    deleteMessageChannelId: number;
   //creat new messages 
   messageToSend: string;

   /*------------------view&dialogs ---------------------*/ 
   screenWidth: number;
   viewChannels: boolean;
   desktopView: boolean;
   switchViewSmartPhone: boolean;
   uploadCompleting: boolean;

   //Dialogs
   openSettingsDialog: boolean;
   openConfirmDialogDeleteChannel: boolean;
   openConfirmDialogDeleteMessage: boolean; 
   openEditDialog: boolean;
}
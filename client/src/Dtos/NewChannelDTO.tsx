
export interface NewChannelDTO {
    name: string;
    isLocked: boolean;
    isPublic: boolean;
    userIds: string[];
    modIds: string[];
}
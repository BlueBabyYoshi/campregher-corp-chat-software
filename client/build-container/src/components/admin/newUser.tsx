import * as React from "react";
import { NewUserState } from "./newUserState";
import {NewUserDTO} from "../../Dtos/NewUserDTO";
import {NewUserResultDTO} from "../../Dtos/newUserResultDTO";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import * as CSS from "csstype";
import {Checkbox, createStyles, makeStyles, TextField, Theme} from "@material-ui/core";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from '@material-ui/core/FormGroup';

const centered: React.CSSProperties = {
    textAlign: "center" as "center"
};

export class newUser extends React.Component<NewUserState> {

    public state: NewUserState = {
        username: "",
        name: "",
        emailAddress: "",
        isAdmin: false,
        isUser: false,
        department: "Other",
        office: "",
        emailFrequency: "Monthly", //default value
        result: ""
    };

    private newUserDTO: NewUserDTO | undefined;

    constructor(props) {
        super(props);
        this.onUsernameChanged = this.onUsernameChanged.bind(this);
        this.onNameChanged = this.onNameChanged.bind(this);
        this.onEmailAddressChanged = this.onEmailAddressChanged.bind(this);
        this.onAdminChanged = this.onAdminChanged.bind(this);
        this.onUserChanged = this.onUserChanged.bind(this);
        this.onDepartmentChanged = this.onDepartmentChanged.bind(this);
        this.onOfficeChanged = this.onOfficeChanged.bind(this);
        this.onEmailFrequencyChanged = this.onEmailFrequencyChanged.bind(this);
        this.onRegisterClick = this.onRegisterClick.bind(this);
        this.onResultsAreIn = this.onResultsAreIn.bind(this);
    }


    public async onRegisterClick(): Promise<void> {
        this.newUserDTO = {
            username: this.state.username,
            name: this.state.name,
            emailAddress: this.state.emailAddress,
            isAdmin: this.state.isAdmin,
            isUser: this.state.isUser,
            department: this.state.department,
            office: this.state.office,
            emailFrequency: this.state.emailFrequency
        };
        var stringVal = JSON.stringify({
            username: this.state.username,
            name: this.state.name,
            emailAddress: this.state.emailAddress,
            isAdmin: this.state.isAdmin,
            isUser: this.state.isUser,
            department: this.state.department,
            office: this.state.office,
            emailFrequency: this.state.emailFrequency
        });
        console.log(stringVal);
        try {
            let response = await fetch("http://localhost:8080/newUser", {
                headers: {
                    "Content-Type": "application/json",
                },
                method: "POST",
                body: JSON.stringify(this.newUserDTO),
                mode: "cors",
                credentials: "include"
            });
            let jsonResult = await response.json();
            let jsonObject: NewUserResultDTO = JSON.parse(JSON.stringify(jsonResult));
            this.onResultsAreIn(jsonObject);
        } catch(e) {
            console.log("Registration Error");
            this.setState({
                ...this.state,
                result: "Fehler bei Webserverabruf."
            });
            console.log(e);
        }
    }

    public onResultsAreIn(dto: NewUserResultDTO): void {
        this.setState({
            ...this.state,
            result: dto.message
        });
    }

    public onUsernameChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            username: event.target.value
        });
    }

    public onNameChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            name: event.target.value
        });
    }

    public onEmailAddressChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            emailAddress: event.target.value
        });
    }

    public onAdminChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            isAdmin: event.target.checked
        });
    }

    public onUserChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            isUser: event.target.checked
        });
    }

    public onDepartmentChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            department: event.target.value
        });
    }

    public onOfficeChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            office: event.target.value
        });
    }

    public onEmailFrequencyChanged(event: React.ChangeEvent<HTMLInputElement>): void {
        this.setState({
            ...this.state,
            emailFrequency: event.target.value
        });
    }

    render() {
        console.log("newUser");

        const formStyle: CSS.Properties = {
            padding: '10px 5px 5px 10px',
            textAlign:'center'
        };

        const useStyles = makeStyles((theme: Theme) =>
            createStyles({
                formControl: {
                    margin: theme.spacing(3),
                },
            }),
        );

        return(
            <form style={formStyle}>
                <Typography component="h1" variant="h5">
                    Neuen Benutzer anlegen
                </Typography>
                <div>
                    <h5>Username:</h5>
                    <TextField value={this.state.username} variant="outlined" margin="normal" onChange={this.onUsernameChanged} />
                </div>
                <div>
                    <h5>Name:</h5>
                    <TextField value={this.state.name} variant="outlined" margin="normal" onChange={this.onNameChanged} />
                </div>
                <div>
                    <h5>Email-Adresse:</h5>
                    <TextField value={this.state.emailAddress} variant="outlined" margin="normal" onChange={this.onEmailAddressChanged} />
                </div>
                <div>
                <FormControl component="fieldset">
                    <FormLabel component="legend">Rechte:</FormLabel>
                    <FormGroup>
                        <FormControlLabel label="Admin" control={<Checkbox />} value={this.state.isAdmin} onChange={this.onAdminChanged} />
                        <FormControlLabel label="Benutzer" control={<Checkbox />} value={this.state.isUser} onChange={this.onUserChanged} />
                    </FormGroup>
                </FormControl>
                </div>
                <div>
                    <FormControl component="fieldset" >
                        <FormLabel component="legend">Abteilung:</FormLabel>
                        <RadioGroup aria-label="department" name="department">
                            <FormControlLabel value="Other" control={<Radio />} label="Andere" checked={this.state.department === "Other"} onChange={this.onDepartmentChanged} />
                            <FormControlLabel value="HR" control={<Radio />} label="Personal" checked={this.state.department === "HR"} onChange={this.onDepartmentChanged} />
                            <FormControlLabel value="Marketing" control={<Radio />} label="Marketing" checked={this.state.emailFrequency === "Marketing"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Sales" control={<Radio />} label="Verkauf" checked={this.state.department === "Sales"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Production" control={<Radio />} label="Produktion" checked={this.state.department === "Production"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="IT" control={<Radio />} label="IT" checked={this.state.department === "IT"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Finance" control={<Radio />} label="Finanz" checked={this.state.department === "Finance"} onChange={this.onDepartmentChanged}/>
                            <FormControlLabel value="Management" control={<Radio />} label="Management" checked={this.state.department === "Management"} onChange={this.onDepartmentChanged}/>
                        </RadioGroup>
                    </FormControl>
                </div>
                <div>
                    <h5>Büro:</h5>
                    <TextField value={this.state.office} variant="outlined" margin="normal" onChange={this.onOfficeChanged} />
                </div>
                <div>
                <FormControl component="fieldset" >
                    <FormLabel component="legend">Wie oft sollen Mails verschickt werden:</FormLabel>
                    <RadioGroup aria-label="email-frequency" name="email-frequency">
                        <FormControlLabel value="Monthly" control={<Radio />} label="Monatlich" checked={this.state.emailFrequency === "Monthly"} onChange={this.onEmailFrequencyChanged} />
                        <FormControlLabel value="Weekly" control={<Radio />} label="Wöchentlich" checked={this.state.emailFrequency === "Weekly"} onChange={this.onEmailFrequencyChanged}/>
                        <FormControlLabel value="Never" control={<Radio />} label="Nie" checked={this.state.emailFrequency === "Never"} onChange={this.onEmailFrequencyChanged}/>
                    </RadioGroup>
                </FormControl>
                </div>

                <Button onClick={this.onRegisterClick} >Speichern</Button>

                <div>
                    <h3>{this.state.result}</h3>
                </div>
            </form>
        );
    }

}

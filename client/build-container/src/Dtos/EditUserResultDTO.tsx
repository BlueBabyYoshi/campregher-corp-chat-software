
export interface EditUserResultDTO {
    success: boolean;
    message: string;
}
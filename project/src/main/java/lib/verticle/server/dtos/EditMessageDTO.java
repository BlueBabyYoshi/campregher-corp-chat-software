package lib.verticle.server.dtos;

public class EditMessageDTO {
    //test only with text same in Frontend with DTO
    private int id;
    private int channelId;
    private String text;


    public EditMessageDTO(int id, String text) {
        this.id = id;
        this.text = text;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }
}

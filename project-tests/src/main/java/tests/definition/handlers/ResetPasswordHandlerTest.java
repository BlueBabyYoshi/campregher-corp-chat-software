package tests.definition.handlers;

import lib.Database.services.QueryService;
import lib.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import tests.base.testdefinition.AbstractVertxTest;

/**
 * The type Reset password handler test.
 */
public class ResetPasswordHandlerTest extends AbstractVertxTest {

    /**
     * Sets database.
     */
    @BeforeEach
    public void setupDatabase() {
        clearDatabaseFrom(User.class);
    }

    /**
     * Reset password test.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void resetPasswordTest() throws Throwable {
        QueryService handler = getInjector().getInstance(QueryService.class);
        var client = getRestClient();
        String password = "password";
        String salt = "salt";
        String eMail = "test@test.at";
        String username = "Testuser";
        User user = new User();
        user.setPassword(password);
        user.setSalt(salt);
        user.setEmailAddress(eMail);
        user.setUsername(username);
        handler.saveUser(user).setHandler(userS -> {
            if (userS.succeeded()) {
                try {
                    client.post("/resetPassword", eMail, String.class);
                    handler.getUserByUsername(username).setHandler(userQ -> {
                        if (userQ.succeeded()) {
                            User savedUser = userQ.result();
                            Assert.assertNotEquals(savedUser.getPassword(), password);
                        }
                    });
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            }
        });
    }

    /**
     * Reset password fail test.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void resetPasswordFailTest() throws Throwable {
        var client = getRestClient();
        String result = client.post("/resetPassword", "totallyNotAeMail@test.at", String.class);
        Assert.assertEquals("Benutzer konnte nicht gefunden werden", result);
    }
}

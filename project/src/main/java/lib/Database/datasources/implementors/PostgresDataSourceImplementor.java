package lib.Database.datasources.implementors;

import org.postgresql.ds.PGSimpleDataSource;

import javax.sql.DataSource;

/**
 * The POSTGRES data source implementor.
 */
public class PostgresDataSourceImplementor extends BaseDataSourceFunctionalityImplementor<PGSimpleDataSource> {

    @Override
    protected Class<PGSimpleDataSource> getAssumedDataSourceType() {
        return PGSimpleDataSource.class;
    }

    @Override
    protected void setUrlInternal(PGSimpleDataSource source, String url) {
        source.setUrl(url);
    }

    @Override
    protected void setUserInternal(PGSimpleDataSource source, String user) {
        source.setUser(user);
    }

    @Override
    protected void setPasswordInternal(PGSimpleDataSource source, String pw) {
        source.setPassword(pw);
    }

    @Override
    public DataSource create() {
        return new PGSimpleDataSource();
    }
}

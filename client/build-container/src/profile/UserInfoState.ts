
export interface UserInfoState {
    username: string;
    name: string;
    emailAddress: string;
    isAdmin: boolean;
    isUser: boolean;
    department: string;
    office: string;
    emailFrequency: string;
    isActiveUser: boolean;
    isLoading: boolean;
    result: string;
}
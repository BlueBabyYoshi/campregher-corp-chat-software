package lib.verticle.server.handlers.channels;


import com.google.gson.Gson;
import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.channels.dtos.ChannelIdDTO;
import lib.verticle.server.handlers.channels.dtos.ChannelSettingsDTO;
import lib.verticle.server.handlers.channels.dtos.ChannelUserModeratorEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * The GetChannelSettingsHandler gets channelId via REst Call, retrieves settings, retrieves all users, checks if users are in User or modlist for the channel and creates Response Data and sends it.
 */
@HttpHandler(endpoint = "/channels/settings", method = HttpMethod.POST, interfaceType = IGetChannelSettingsHandler.class)
public class GetChannelSettingsHandler extends AbstractHttpHandler implements IGetChannelSettingsHandler {

    @Inject
    private IQueryService queryService;
    @Inject
    private DatabaseHandler handler;

    @Override
    public void handle(RoutingContext context) {

        Gson gson = new Gson();
        var channel = gson.fromJson(context.getBodyAsString(), ChannelIdDTO.class);
        queryService.getChannelById(channel.getChannelId()).setHandler(r -> {
            if (r.succeeded()) {
                var retrievedChannel = r.result();
                var channelSettings = new ChannelSettingsDTO(retrievedChannel.getName(), retrievedChannel.getPublic(), retrievedChannel.isLocked());
                handler.readObjects(User.class).setHandler(userRetrieve -> {
                    if (userRetrieve.succeeded()) {
                        List<User> userList = userRetrieve.result();
                        List<ChannelUserModeratorEntity> usermodList = new ArrayList<>();
                        channelSettings.setCurrentUserIsMod(false);
                        for (User user : userList) {
                            var channelUserMod = new ChannelUserModeratorEntity(user.getId(), user.getUsername());
                            channelUserMod.setUser(retrievedChannel.getChannelMembers().contains(user));
                            channelUserMod.setModerator(retrievedChannel.getModerator().contains(user));
                            usermodList.add(channelUserMod);
                            // check if current logged in user is moderator
                            if (user.getUsername().equals(context.session().get("username")) && retrievedChannel.isUserMod(user.getUsername())) {
                                channelSettings.setCurrentUserIsMod(true);
                            }
                        }
                        channelSettings.setUserModList(usermodList);
                        writeResponseToContext(context, channelSettings);
                    }
                });
            }
        });
    }

    private void writeResponseToContext(RoutingContext context, ChannelSettingsDTO message) {
        Gson gson = new Gson();
        String resultString = gson.toJson(message);
        System.out.println(resultString);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultString);
    }
}

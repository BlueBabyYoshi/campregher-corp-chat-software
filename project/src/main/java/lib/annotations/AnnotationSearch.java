package lib.annotations;

import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;

import java.util.ArrayList;
import java.util.List;

/**
 * The Annotation search to search for any Annotation in the Project.
 * We use it to look for the @HTTPHandler Annotation to attatch Handler to the RoutingContext
 */
public class AnnotationSearch {

    private List<IDefinedAnnotation> definedAnnotations;

    /**
     * Instantiates a new Annotation search.
     */
    public AnnotationSearch() {
        this.definedAnnotations = new ArrayList<>();
    }

    /**
     * Add annotation that should be searched for.
     *
     * @param annot the annot
     */
    public void addAnnotation(IDefinedAnnotation annot) {

        definedAnnotations.add(annot);
    }

    /**
     * Search annotations in lib Folder.
     */
    public void searchAnnotations() {

        var reflections = new Reflections("lib", new SubTypesScanner(false));
        var classes = reflections.getSubTypesOf(Object.class);
        for (var cls : classes) {

            for (var annot : cls.getAnnotations()) {

                definedAnnotations.stream()
                        .filter(r -> r.getAnnotationType().equals(annot.annotationType()))
                        .forEach(r -> r.getFoundHandler().annotationFound(cls, annot));
            }
        }
    }
}

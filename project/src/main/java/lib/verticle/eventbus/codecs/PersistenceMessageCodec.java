package lib.verticle.eventbus.codecs;

import com.google.gson.Gson;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.MessageCodec;

import java.util.Collection;

/**
 * The PersistenceMessageCodec used on our messagebus to en- and decode messages/objects sent over the bus.
 * Without it we would be able to send custom Datatyped over the bus.
 */
public class PersistenceMessageCodec implements MessageCodec<Object, Object> {

    @Override
    public void encodeToWire(Buffer buffer, Object s) {
        Gson gson = new Gson();
        String entityAsJson = gson.toJson(s);
        EntityDeliveryDTO deliveryDTO = new EntityDeliveryDTO();
        deliveryDTO.setJsonData(entityAsJson);
        deliveryDTO.setObjectType(s.getClass());
        deliveryDTO.setCollection(s instanceof Collection<?>);
        String deliveryJson = gson.toJson(deliveryDTO);
        int length = deliveryJson.getBytes().length;
        buffer.appendInt(length);
        buffer.appendString(deliveryJson);
    }

    @Override
    public Object decodeFromWire(int i, Buffer buffer) {
        int length = buffer.getInt(i);
        String rawDeliveryString = buffer.getString(i += 4, i += length);
        Gson gson = new Gson();
        EntityDeliveryDTO deliveryDTO = gson.fromJson(rawDeliveryString, EntityDeliveryDTO.class);
        if (deliveryDTO.isCollection()) {
            return gson.fromJson(deliveryDTO.getJsonData(), deliveryDTO.getObjectType());
        }
        return gson.fromJson(deliveryDTO.getJsonData(), deliveryDTO.getObjectType());
    }

    @Override
    public Object transform(Object s) {
        return s;
    }

    @Override
    public String name() {
        return MessageCodecNames.PERSISTENCE;
    }

    @Override
    public byte systemCodecID() {
        return -1;
    }
}

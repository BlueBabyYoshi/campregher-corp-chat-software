package tests.definition.handlers;

import controller.AuthenticationController;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import lib.Database.services.QueryService;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.verticle.server.dtos.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import tests.base.testdefinition.AbstractVertxTest;


/**
 * The type Auth handler test.
 */
public class AuthHandlerTest extends AbstractVertxTest {

    /**
     * Sets database.
     */
    @BeforeEach
    public void setupDatabase() {
        clearDatabaseFrom(User.class);
        AuthenticationController controller = new AuthenticationController();
        NewUserDTO userInfo = new NewUserDTO("user1", "name1", "emailAddress", false, false, "IT", "office", EmailFrequency.valueOf("Weekly").toString());
        controller.registerNewUser(userInfo);
    }

    /**
     * Check auth test.
     */
    @Test
    public void checkAuthTest() {
        var client = getRestClient();
        loginUser("user1").setHandler(res -> {
            if (res.succeeded()) {
                AuthorizationDTO authorizationDTO = new AuthorizationDTO();
                authorizationDTO.setSessionId(null);
                authorizationDTO.setRole("");
                try {
                    AuthorizationResultDTO authorizationResultDTO = client.post("/checkAuthorization", authorizationDTO, AuthorizationResultDTO.class);
                    Assert.assertNotNull(authorizationResultDTO);
                    Assert.assertFalse(authorizationResultDTO.isAuthenticated());
                    Assert.assertFalse(authorizationResultDTO.getIsAuthorized());
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
    }

    private Future<LoginResultDTO> loginUser(String username) {
        var client = getRestClient();
        QueryService handler = getInjector().getInstance(QueryService.class);
        AuthenticationController controller = new AuthenticationController();
        Promise<LoginResultDTO> promise = Promise.promise();
        LoginDTO loginInfo = new LoginDTO();
        loginInfo.setPassword("password");
        loginInfo.setUsername("username");
        handler.getUserByUsername("username").setHandler(userQ -> {
            if (userQ.succeeded()) {
                User user = userQ.result();
                String newSalt = controller.generateSalt();
                String newPw = controller.hashPassword("password", newSalt);
                user.setSalt(newSalt);
                user.setPassword(newPw);
                handler.saveUser(user).setHandler(res -> {
                    if (res.succeeded()) {
                        try {
                            LoginResultDTO loginResultDTO = client.post("/login", loginInfo, LoginResultDTO.class);
                            promise.complete(loginResultDTO);
                        } catch (Throwable throwable) {
                            promise.fail("fail");
                            throwable.printStackTrace();
                        }

                    }
                });
            }
        });
        return promise.future();
    }

}

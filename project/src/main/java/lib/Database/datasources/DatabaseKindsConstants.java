package lib.Database.datasources;

/**
 * The Database kinds constants.
 */
public class DatabaseKindsConstants {

    public static final String MYSQL = "mysql";
    public static final String POSTGRES = "postgresql";
    public static final String MSSQL = "sqlserver";
}

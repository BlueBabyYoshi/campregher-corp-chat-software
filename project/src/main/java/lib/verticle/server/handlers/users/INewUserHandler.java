package lib.verticle.server.handlers.users;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface New user handler.
 */
public interface INewUserHandler extends IHttpHandler {
}

package lib.verticle.server.handlers.auth;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Login handler.
 */
public interface ILoginHandler extends IHttpHandler {
}

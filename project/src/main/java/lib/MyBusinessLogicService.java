package lib;

import com.google.inject.Guice;
import com.google.inject.Injector;
import controller.AuthenticationController;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;
import lib.Database.Connections;
import lib.Database.Persistence;
import lib.Database.services.DatasourceService;
import lib.Database.services.IQueryService;
import lib.model.Channel;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.model.fileUpload.UploadedFile;
import lib.services.LoggingService.ILoggingService;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.basic.BasicModule;
import lib.verticle.deploy.VerticleDeployer;
import lib.verticle.deploy.Verticles;
import lib.verticle.server.dtos.NewUserDTO;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.time.LocalDateTime;
import java.util.*;

//for websocket


public class MyBusinessLogicService extends WebSocketServer {
    public static DatabaseHandler handler;
    /****************************WEBSOCKETS END************************************/
    public static lib.model.Message message;
    private static Injector injector;
    private static IQueryService queryService;
    private static ILoggingService loggingService;
    private static Vertx myvertx;
    private AuthenticationController controller;
    private List<WebSocket> webSockets = new LinkedList<>();
    private HashMap<Integer, WebSocket> webSocketConnectionMap = new HashMap<>();


    /*************************************WEBSOCKET*******************************************/
    //tutorial: https://github.com/TooTallNate/Java-WebSocket/blob/master/src/main/example/ChatServer.java
    public MyBusinessLogicService(int port) throws UnknownHostException {
        super(new InetSocketAddress(port));
    }

    public static void main(String[] args) throws InterruptedException, IOException, URISyntaxException {
        Persistence.initializeDataBroker(Connections.DEFAULT);
        myvertx = Vertx.vertx();
        injector = Guice.createInjector(new BasicModule(myvertx));
        handler = injector.getInstance(DatabaseHandler.class);
        int port = 8887; // 843 flash policy port
        try {
            port = Integer.parseInt(args[0]);
        } catch (Exception ex) {
        }
        MyBusinessLogicService service = new MyBusinessLogicService(port);
        queryService = injector.getInstance(IQueryService.class);
        loggingService = injector.getInstance(ILoggingService.class);
        loggingService.logInfo("Start Verticle Deployment");
        VerticleDeployer.DeployVerticles(myvertx, Verticles.getAppVerticles(injector)).setHandler(r -> {
            System.out.println("Alle verticles deployed.");
            if (r.succeeded()) {
                //System.out.println("Yay!");
                service.prefillDatabase();
                loggingService.logInfo("Verticles succesfully deployed!");

            } else {
                //System.out.println("Nay!");
                loggingService.logError("Error Deploying Verticles!", null);
            }
        });


        /*************************** WEBSOCKETS start***************************************/

        service.start();
        System.out.println("Opened WebSocket on port: " + service.getPort());


    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        webSockets.add(conn);

        System.out.println(handshake.getFieldValue("Cookie"));
        for (int i = 0; i < webSockets.size(); i++) {
            webSockets.get(i).send("Hello Nr. " + i);
        }
        System.out.println(webSockets);
        conn.send("Welcome to the server!"); //This method sends a message to the new client
        broadcast("new connection: " + handshake.getResourceDescriptor()); //This method sends a message to all clients connected
        System.out.println(conn.getRemoteSocketAddress().getAddress().getHostAddress() + " entered the room!");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        webSockets.remove(conn);
        System.out.println(webSockets);
        broadcast(conn + " has left the room!");
        System.out.println(conn + " has left the room!");
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        int channelId = 0;
        try {
            channelId = Integer.parseInt(message.trim());
        } catch (NumberFormatException e) {
            System.out.println("NumberFormatException: " + e.getMessage());
        }
        webSocketConnectionMap.put(channelId, conn);
        //broadcast(message);
        System.out.println(conn + ": " + message);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        broadcast(message.array());
        System.out.println("message gesendet?: " + conn + ": " + message);
    }


    /********************************MAP for storing websocket connection per Channel (key = Channel is)************************************/

    @Override
    public void onError(WebSocket conn, Exception ex) {
        ex.printStackTrace();
        if (conn != null) {
            // some errors like port binding failed may not be assignable to a specific websocket
        }
    }

    @Override
    public void onStart() {
        System.out.println("Server started!");
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);
    }

    private void prefillDatabase() {
        prefillUsers().setHandler(r -> {
            if (r.succeeded()) {
                prefillChannels(r.result());
            }
        });
        //prefillTags();

    }

    private void prefillChannels(User result) {
        Set<User> setUser = new HashSet<User>();
        /* Channel 1*/
        Channel channel = new Channel("Allgemeines", true, setUser, false);
        queryService.saveChannel(channel).setHandler(res -> {
            if (res.succeeded()) {
                addChannelModerator("Allgemeines", "user3");
                addChannelModerator("Allgemeines", "user1");
                addUserToChannel("Allgemeines", "user1");
                addUserToChannel("Allgemeines", "user2");
                addUserToChannel("Allgemeines", "user3");
                addUserToChannel("Allgemeines", "user4");
                addUserToChannel("Allgemeines", "user5");
                addUserToChannel("Allgemeines", "user6");
                addUserToChannel("Allgemeines", "user7");
                addUserToChannel("Allgemeines", "user8");
                addUserToChannel("Allgemeines", "zGrantigerGrantler");
                addChannelMessages("Allgemeines", "Yo listen up, here's the story", "user1", null);
                addChannelMessages("Allgemeines", "About a little guy that lives in a blue world", "user3", null);
                addChannelMessages("Allgemeines", "And all day and all night and everything he sees is just blue", "user6", null);
                addChannelMessages("Allgemeines", "Like him, inside and outside", "user7", null);
                addChannelMessages("Allgemeines", "Blue his house with a blue little window", "user3", null);
                addChannelMessages("Allgemeines", "And a blue Corvette", "user2", null);
                addChannelMessages("Allgemeines", "And everything is blue for him", "user1", null);
                addChannelMessages("Allgemeines", "And himself and everybody around", "user6", null);
                addChannelMessages("Allgemeines", "'Cause he ain't got nobody to listen", "user4", null);
                addChannelMessages("Allgemeines", "I'm blue da ba dee da ba daa\n" +
                        "Da ba dee da ba daa, da ba dee da ba daa, da ba dee da ba daa\n" +
                        "Da ba dee da ba daa, da ba dee da ba daa, da ba dee da ba daa", "user7", null);
                addChannelMessages("Allgemeines", "Heast, des is a Frechheit. Wos soll denn dea Mist?\" +\n" +
                        "\" I bin der oanzige der richtig ghagglt hot in der Firma...und wos hot ma davu? Aussi gschmissen wird ma und des nur weil i a poor mol bissal zvui Schnops im mein Kaffee gschittet hun", "zGrantigerGrantler", null);
                addChannelMessages("Allgemeines", "Es weads scho no sechn wohin des geht. Nimma long und i moch as große Geld auf Ibiza!!", "zGrantigerGrantler", null);
            }
        });


        /* Channel 2*/
        Set<User> setUser2 = new HashSet<User>();
        Channel channel2 = new Channel("Wichtiges", false, setUser2, true);
        queryService.saveChannel(channel2).setHandler(res -> {
            if (res.succeeded()) {
                addChannelModerator("Wichtiges", "user3");
                addChannelModerator("Wichtiges", "user1");
                addUserToChannel("Wichtiges", "user1");
                addUserToChannel("Wichtiges", "user2");
                addUserToChannel("Wichtiges", "user3");
                addUserToChannel("Wichtiges", "user4");
                addUserToChannel("Wichtiges", "user5");
                addUserToChannel("Wichtiges", "user6");
                addUserToChannel("Wichtiges", "user7");
                addUserToChannel("Wichtiges", "user8");
                addChannelMessages("Wichtiges", "Last Christmas, I gave you my heart", "user1", null);
                addChannelMessages("Wichtiges", "But the very next day you gave it away", "user2", null);
                addChannelMessages("Wichtiges", "This year, to save me from tears", "user3", null);
                addChannelMessages("Wichtiges", "I'll give it to someone special", "user4", null);
                addChannelMessages("Wichtiges", "Last Christmas, I gave you my heart", "user5", null);
                addChannelMessages("Wichtiges", "But the very next day you gave it away", "user6", null);
                addChannelMessages("Wichtiges", "This year, to save me from tears", "user7", null);
                addChannelMessages("Wichtiges", "I'll give it to someone special", "user8", null);
                addChannelMessages("Wichtiges", "Once bitten and twice shy\n" +
                        "I keep my distance\n" +
                        "But you still catch my eye\n" +
                        "Tell me, baby\n" +
                        "Do you recognize me?\n" +
                        "Well, it's been a year\n" +
                        "It doesn't surprise me\n" +
                        "(Merry Christmas!) I wrapped it up and sent it\n" +
                        "With a note saying, \"I love you, \" I meant it\n" +
                        "Now, I know what a fool I've been\n" +
                        "But if you kissed me now\n" +
                        "I know you'd fool me again ÄÖÜß", "user2", null);
            }

        });


        /* Channel 3*/
        Set<User> setUser3 = new HashSet<User>();
        Channel channel3 = new Channel("Meetings", true, setUser3, false);
        queryService.saveChannel(channel3).setHandler(res -> {
            if (res.succeeded()) {
                addChannelModerator("Meetings", "user3");
                addChannelModerator("Meetings", "user1");
                addUserToChannel("Meetings", "user1");
                addUserToChannel("Meetings", "user2");
                addUserToChannel("Meetings", "user4");
                addUserToChannel("Meetings", "user5");
                addUserToChannel("Meetings", "user6");
                addUserToChannel("Meetings", "user7");
                addUserToChannel("Meetings", "user8");
                addChannelMessages("Meetings", "Hello Wolrd", "user1", null);
                addChannelMessages("Meetings", "???", "user2", null);
                addChannelMessages("Meetings", "You don get it or dont you", "user3", null);
                addChannelMessages("Meetings", "Remember, this should be uses for meetings?", "user4", null);
                addChannelMessages("Meetings", "and?", "user5", null);
                addChannelMessages("Meetings", "This is not how this software should be used...", "user6", null);
                addChannelMessages("Meetings", "im sorry", "user1", null);

            }
        });



        /* Channel 4*/
        Set<User> setUser4 = new HashSet<User>();
        Channel channel4 = new Channel("Internes", false, setUser4, false);
        queryService.saveChannel(channel4).setHandler(res -> {
            if (res.succeeded()) {
                addUserToChannel("Internes", "user2");
                addUserToChannel("Internes", "user4");

            }
        });




        /* Channel 5*/
        Set<User> setUser5 = new HashSet<User>();
        Channel channel5 = new Channel("Projektinfos Kunden", false, setUser5, true);
        queryService.saveChannel(channel5).setHandler(res -> {
            if (res.succeeded()) {
                addUserToChannel("Projektinfos Kunden", "user1");
                addUserToChannel("Projektinfos Kunden", "user2");
                addUserToChannel("Projektinfos Kunden", "user4");
            }
        });


        /* Channel 7*/
        Set<User> setUser7 = new HashSet<User>();
        Channel channel7 = new Channel("Speiseplan", true, setUser7, true);
        queryService.saveChannel(channel7).setHandler(res -> {
            if (res.succeeded()) {
                addChannelMessages("Speiseplan", "Das ist nur ein Test", "user1", null);
                addChannelMessages("Speiseplan", "Schöner test", "user2", null);
            }
        });
    }

    private void addUserToChannel(String channelname, String username) {
        queryService.getUserByUsername(username).setHandler(userQ -> {
            if (userQ.succeeded()) {
                User user = userQ.result();
                queryService.getChannelByChannelname(channelname).setHandler(channelQ -> {
                    if (channelQ.succeeded()) {
                        Channel channel = channelQ.result();
                        Set<User> userSet;
                        if(channel.getChannelMembers() == null){
                            userSet = new HashSet<User>();
                        }
                        else {
                            userSet = channel.getChannelMembers();
                        }
                        userSet.add(user);
                        channel.setChannelMembers(userSet);
                        queryService.saveChannel(channel);
                    }
                });
            }
        });
    }

    private void editUserPassword(String username, String password, AuthenticationController controller) {
        String salt = controller.generateSalt();
        String hasehedPw = controller.hashPassword(password, salt);
        queryService.getUserByUsername(username).setHandler(userQ -> {
            if (userQ.succeeded()) {
                User user = userQ.result();
                user.setPassword(hasehedPw);
                user.setSalt(salt);
                queryService.saveUser(user);
            }
        });
    }

    private void addChannelModerator(String channelName, String username) {
        queryService.getChannelByChannelname(channelName).setHandler(channelQuery -> {
            if (channelQuery.succeeded()) {
                queryService.getUserByUsername(username).setHandler(userQ -> {
                    if (userQ.succeeded()) {
                        User user = userQ.result();
                        Channel channel = channelQuery.result();
                        Set<User> moderators = channel.getModerator();
                        if (moderators != null) {
                            moderators.add(user);
                        } else {
                            moderators = new HashSet<>();
                            moderators.add(user);
                        }
                        channel.setModerator(moderators);
                        queryService.saveChannel(channel);
                    }
                });
            }
        });
    }

    private void addParentChannel(String parentName, String childName) {
        queryService.getChannelByChannelname(parentName).setHandler(parent -> {
            if (parent.succeeded()) {
                Channel parentImpl = parent.result();
                queryService.getChannelByChannelname(childName).setHandler(child -> {
                    if (child.succeeded()) {
                        Channel childImpl = child.result();
                        childImpl.setParentChannel(parentImpl);
                        //childImpl.setSubChannel(true);
                        queryService.saveChannel(childImpl);
                    }
                });
            }
        });
    }

    private void addChannelMessages(String channelname, String message, String username, UploadedFile file) {
        queryService.getUserByUsername(username).setHandler(userQ -> {
            if (userQ.succeeded()) {
                User user = userQ.result();
                queryService.getChannelByChannelname(channelname).setHandler(channel -> {
                    if (channel.succeeded()) {
                        LocalDateTime time = LocalDateTime.now();
                        lib.model.Message messageImpl = new lib.model.Message(message, time, time, user, channel.result(), file);
                        queryService.saveMessage(messageImpl);
                    }

                });
            }
        });
    }

    private Future<User> prefillUsers() {
        Promise<User> promise = Promise.promise();
        DatasourceService dataSource = new DatasourceService();
        AuthenticationController controller = new AuthenticationController(myvertx, queryService, dataSource.createDataSource());
        NewUserDTO userInfo1 = new NewUserDTO("user1", "Peter", "user1@test.at", true, true, "IT", "1", EmailFrequency.valueOf("Weekly").toString());
        NewUserDTO userInfo2 = new NewUserDTO("user2", "Anna", "user2@test.at", true, false, "IT", "2", EmailFrequency.valueOf("Monthly").toString());
        NewUserDTO userInfo3 = new NewUserDTO("user3", "Johanna", "user3@test.at", false, true, "HR", "3", EmailFrequency.valueOf("Never").toString());
        NewUserDTO userInfo4 = new NewUserDTO("user4", "Luciano", "user4@test.at", false, true, "Other", "4", EmailFrequency.valueOf("Monthly").toString());
        NewUserDTO userInfo5 = new NewUserDTO("user5", "Jenifer", "user5@test.at", false, true, "Marketing", "2", EmailFrequency.valueOf("Never").toString());
        NewUserDTO userInfo6 = new NewUserDTO("user6", "Alexis", "user6@test.at", false, true, "IT", "3", EmailFrequency.valueOf("Monthly").toString());
        NewUserDTO userInfo7 = new NewUserDTO("user7", "Eleanor", "user7@test.at", false, true, "Marketing", "1", EmailFrequency.valueOf("Weekly").toString());
        NewUserDTO userInfo8 = new NewUserDTO("user8", "Rhoda", "user8@test.at", false, true, "Production", "1", EmailFrequency.valueOf("Never").toString());
        NewUserDTO userInfo9 = new NewUserDTO("user9", "Shirley", "user9@test.at", false, true, "HR", "3", EmailFrequency.valueOf("Monthly").toString());
        NewUserDTO userInfo10 = new NewUserDTO("zGrantigerGrantler", "Alfred", "freddy@test.at", false, false, "Sales", "2", EmailFrequency.valueOf("Weekly").toString());
        NewUserDTO[] newUserDTOS = {userInfo1, userInfo2, userInfo3, userInfo4, userInfo5, userInfo6, userInfo7, userInfo8, userInfo9, userInfo10};
        for (NewUserDTO dto : newUserDTOS) {
            controller.registerNewUser(dto).setHandler(res -> {
                if (res.succeeded()) {
                    System.out.println("registration of " + res.result().getUsername() + " successful");
                    if (res.result().getUsername().equals("user3")) {
                        promise.complete(res.result());
                    }
                } else {
                    System.out.println("ERROR registration of " + dto.getUsername());
                    promise.fail("User return failed!");
                }
            });
        }
        editUserPassword("user1", "password", controller);
        editUserPassword("user2", "password", controller);
        editUserPassword("user3", "password", controller);
        return promise.future();
    }


    public Future<List<User>> getAllUsersForRest() {
        return readAllObjectsOfAClass(User.class);
    }

    public <T> Future<List<T>> readAllObjectsOfAClass(Class<T> type) {
        return handler.readObjects(type);
    }

    private <T> Class getEntityType(Message<T> msg) throws ClassNotFoundException {
        return Class.forName(msg.headers().get("entityType"));
    }

}


package lib.verticle.server.handlers;

/**
 * The Handler endpoints for all Handlers.
 */
public class HandlerEndpoints {

    public static class FileUpload {
        public static final String PREFIX = "/files";
        public static final String ADD = PREFIX + "/uploadFile";
        public static final String GET = PREFIX + "/getAttachmentForMessage";
    }

    public static class Channels {
        public static final String PREFIX = "/channels";
        public static final String LOCK_CHANNEL = PREFIX + "/lock";
        public static final String NEW_CHANNEL = PREFIX + "/new";
        public static final String EDIT_CHANEL = PREFIX + "/edit";
        public static final String DELETE_CHANNEL = PREFIX + "/delete";
        public static final String ADD_USER = PREFIX + "/addUser";
        public static final String REMOVE_USER = PREFIX + "/removeUser";
        public static final String ADD_MODERATOR = PREFIX + "/addMod";
        public static final String REMOVE_MODERATOR = PREFIX + "/removeMod";
        public static final String EDIT_USERS = PREFIX + "/editUsers";
        public static final String EDIT_MODS = PREFIX + "/editMods";
    }

    public static class Users {
        public static final String DELETE_USER = "/deleteUser";
        public static final String EDIT_USER = "/editUser";
        public static final String GET_ALL_USER = "/allUsers";
        public static final String NEW_USER = "/newUser";
        public static final String RESET_PASSWORD = "/resetPassword";
        public static final String USER_INFO = "/userInfo";
    }

    public static class Messages {
        public static final String DELETE_MESSAGE = "/deleteMessage";
        public static final String EDIT_MESSAGE = "/editMessage";
        public static final String GET_ALL_MESSAGES = "/allMessages";
        public static final String SET_MESSAGE = "/setMessage";
    }

    public static class Authorization {
        public static final String CHECK_AUTH = "/checkAuthorization";
        public static final String LOGIN = "/login";
        public static final String LOGOUT = "/logout";
    }
}

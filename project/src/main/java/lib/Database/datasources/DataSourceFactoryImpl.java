package lib.Database.datasources;

import javax.sql.DataSource;

/**
 * The Data source factory implementation.
 */
public class DataSourceFactoryImpl implements DataSourceFactory {

    @Override
    public DataSource createDatasource(String url, String username, String password) {
        var dbIdentifier = readIdentifier(url);
        var implementor = DataSourceFunctionalityImplementorPicker.pickImplementor(dbIdentifier);
        var instance = implementor.create();

        implementor.setPassword(instance, password);
        implementor.setUser(instance, username);
        implementor.setUrl(instance, url);

        return instance;
    }

    private String readIdentifier(String url) {
        var values = url.split(":");
        if (values.length > 2) {
            return values[1];
        }
        throw new IllegalArgumentException("url must be formatted: 'jdbc:<databaseKind>:<rest>'");
    }
}

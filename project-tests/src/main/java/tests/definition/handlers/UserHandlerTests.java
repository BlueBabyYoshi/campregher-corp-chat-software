package tests.definition.handlers;

import com.google.gson.JsonParser;
import lib.model.Department;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.model.UserRole;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.dtos.EditUserResultDTO;
import lib.verticle.server.dtos.NewUserResultDTO;
import lib.verticle.server.dtos.UserInfoDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import tests.base.testdefinition.AbstractVertxTest;

import java.util.HashSet;
import java.util.List;


/**
 * The type User handler tests.
 */
public class UserHandlerTests extends AbstractVertxTest {

    /**
     * Sets database.
     */
    @BeforeEach
    public void setupDatabase() {
        clearDatabaseFrom(User.class);
    }

    /**
     * New user test.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void newUserTest() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var client = getRestClient();
        NewUserResultDTO newUserResultDTO = client.post("/newUser", JsonParser.parseString(getNewUser()), NewUserResultDTO.class);
        Assert.assertNotNull(newUserResultDTO);
        Assert.assertEquals("Neuer Benutzer user1 wurde angelegt", newUserResultDTO.getMessage());
        Assert.assertTrue(newUserResultDTO.isSuccess());
        runAndVerifyInContext(ctx -> handler.readObjects(User.class), res -> {
            Assert.assertFalse(res.result().isEmpty());
            var readUser = res.result().stream().filter(r -> r.getUsername().equals("user1")).findFirst().orElse(null);
            Assert.assertNotNull(readUser);
            Assert.assertEquals("name1", readUser.getName());
        });
    }

    /**
     * User info test.
     */
    @Test
    public void userInfoTest() {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var client = getRestClient();
        User user = new User("user3", "name3", "password", "salt", "emailAddress3", new HashSet<UserRole>(), Department.IT, "office3", true, EmailFrequency.Monthly);
        handler.saveObject(User.class, user).setHandler(res -> {
            if (res.succeeded()) {
                System.out.println("User saved for test");
                try {
                    UserInfoDTO userInfo = client.post("/userInfo", "user3", UserInfoDTO.class);
                    Assert.assertNotNull(userInfo);
                    Assert.assertEquals("user3", userInfo.getUsername());
                    Assert.assertEquals("name3", userInfo.getName());
                    Assert.assertFalse(userInfo.isAdmin());
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
    }

    /**
     * Edit user test.
     */
    @Test
    public void editUserTest() {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var client = getRestClient();
        User user = new User("user2", "oldName2", "password", "salt", "emailAddress2", new HashSet<UserRole>(), Department.IT, "office2", true, EmailFrequency.Monthly);
        handler.saveObject(User.class, user).setHandler(res -> {
            if (res.succeeded()) {
                System.out.println("User saved for test");
                try {
                    EditUserResultDTO userResultDTO = client.post("/editUser", JsonParser.parseString(getUserInfo2()), EditUserResultDTO.class);
                    Assert.assertNotNull(userResultDTO);
                    Assert.assertEquals("Änderungen wurden gespeichert", userResultDTO.getMessage());
                    Assert.assertTrue(userResultDTO.isSuccess());
                    runAndVerifyInContext(ctx -> handler.readObjects(User.class), res2 -> {
                        Assert.assertFalse(res2.result().isEmpty());
                        var readUser = res2.result().stream().filter(r -> r.getUsername().equals("user2")).findFirst().orElse(null);
                        Assert.assertNotNull(readUser);
                        Assert.assertEquals("name2", readUser.getName());
                    });
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
    }

    /**
     * Delete user test.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void deleteUserTest() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var client = getRestClient();
        User user = new User("user4", "name4", "password", "salt", "emailAddress4", new HashSet<UserRole>(), Department.IT, "office4", true, EmailFrequency.Monthly);
        handler.saveObject(User.class, user).setHandler(res -> {
            if (res.succeeded()) {
                System.out.println("User saved for test");
                try {
                    client.post("/deleteUser", "user4", null);
                    runAndVerifyInContext(ctx -> handler.readObjects(User.class), res2 -> {
                        Assert.assertFalse(res2.result().isEmpty());
                        var readUser = res2.result().stream().filter(r -> r.getUsername().equals("user4")).findFirst().orElse(null);
                        Assert.assertNotNull(readUser);
                        Assert.assertFalse(readUser.getActiveUser());
                    });
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        });
    }

    /**
     * Gets all users test.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void getAllUsersTest() throws Throwable {
        var handler = getInjector().getInstance(DatabaseHandler.class);
        var client = getRestClient();
        User activeUser = new User("user5", "name5", "password", "salt", "emailAddress5", new HashSet<UserRole>(), Department.IT, "office5", true, EmailFrequency.Monthly);
        User nonActiveUser = new User("user6", "name6", "password", "salt", "emailAddress6", new HashSet<UserRole>(), Department.IT, "office6", false, EmailFrequency.Monthly);
        handler.saveObject(User.class, activeUser).setHandler(res -> {
            if (res.succeeded()) {
                System.out.println("User saved for test");
                handler.saveObject(User.class, nonActiveUser).setHandler(res2 -> {
                    if (res2.succeeded()) {
                        System.out.println("User saved for test");
                        try {
                            List<UserInfoDTO> activeUsers = client.get("/allUsers", (Class<List<UserInfoDTO>>) (Class) List.class);
                            Assert.assertFalse(activeUsers.isEmpty());
                            Assert.assertTrue(activeUsers.size() == 1);
                            boolean containsUser5 = false;
                            boolean containsUser6 = false;
                            for (UserInfoDTO dto : activeUsers) {
                                if (dto.getName().equals("user5")) containsUser5 = true;
                                if (dto.getName().equals("user6")) containsUser6 = true;
                            }
                            Assert.assertTrue(containsUser5);
                            Assert.assertFalse(containsUser6);
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    }
                });
            }
        });
    }

    private String getNewUser() {
        return "{" +
                "\"username\": user1," +
                "\"name\": name1," +
                "\"emailAddress\": emailAddress1," +
                "\"isAdmin\": true," +
                "\"isUser\": false," +
                "\"department\": IT," +
                "\"office\": office1," +
                "\"emailFrequency\": Monthly}";
    }

    private String getUserInfo2() {
        return "{" +
                "\"username\": user2," +
                "\"name\": name2," +
                "\"emailAddress\": emailAddress2," +
                "\"isAdmin\": true," +
                "\"isUser\": false," +
                "\"department\": IT," +
                "\"office\": office2," +
                "\"emailFrequency\": Monthly," +
                "\"isActiveUser\": true}";
    }

}

package lib.model;

import javax.persistence.*;

/**
 * The type User role.
 */
@Entity
@Table(name = "UserRole")
@Access(AccessType.PROPERTY)
public class UserRole extends BaseModel {

    private String roleName;
    private transient User user;

    public UserRole() {

    }

    public UserRole(String roleName) {
        this.roleName = roleName;
    }

    public UserRole(String roleName, User user) {
        this.roleName = roleName;
        this.user = user;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    /**
     * This method checks whether two UserRole objects are equal
     * Without overriding this method it would not be possible to add more than one UserRole to a set
     *
     * @param obj
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (obj == null || obj.getClass() != this.getClass()) return false;
        UserRole role = (UserRole) obj;
        if (this.id == role.getId() && this.roleName.equals(role.getRoleName()) && this.user.equals(role.getUser())) {
            return true;
        } else {
            return false;
        }
    }
}

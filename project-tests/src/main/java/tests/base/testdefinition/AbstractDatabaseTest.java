package tests.base.testdefinition;

import lib.Database.Connections;
import lib.Database.IDataBroker;
import lib.Database.Persistence;

/**
 * The type Abstract database test.
 */
public abstract class AbstractDatabaseTest extends AbstractTest {

    private IDataBroker broker;

    @Override
    protected void setup() {
        broker = Persistence.getDataBroker(Connections.DEFAULT);
    }
}

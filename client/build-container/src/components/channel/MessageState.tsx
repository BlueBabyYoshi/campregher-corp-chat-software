import { Message } from "./Message";
import { Channel } from "./Channel";
import { ChannelTemp } from './ChannelTemp';

export interface MessageState {
   channels: Channel[];
   channelsSelected: Channel[]; 
   channelsTemp: ChannelTemp[];
   messages: Message[];
   messageToSend: string;
   editDialogOpen: boolean;
   editMessage: Message;
   screenWidth: number;
}
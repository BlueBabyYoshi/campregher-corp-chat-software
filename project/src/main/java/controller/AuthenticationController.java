package controller;

import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.jdbc.JDBCAuth;
import io.vertx.ext.auth.jdbc.JDBCHashStrategy;
import io.vertx.ext.jdbc.JDBCClient;
import lib.Database.services.IQueryService;
import lib.model.Department;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.model.UserRole;
import lib.verticle.server.dtos.LoginDTO;
import lib.verticle.server.dtos.NewUserDTO;

import javax.sql.DataSource;
import java.util.HashSet;

/*
for more session handling, authentication and authorization see:
https://vertx.io/docs/vertx-web/java/
 */
public class AuthenticationController {

    private static final String authenticationQuery = "select password, salt from Users where (username = ? and activeUser = 1)";
    private static final String rolesQuery = "select roleName from UserRole r join Users u on r.user_Id = u.Id where username = ?";
    private Vertx vertx;
    private IQueryService queryService;
    private JDBCAuth authProvider;
    private JDBCClient client;
    //private static final String permissionsQuery = "select permission from roles_permissions rp, user_roles ur where ur.name = ? and ur.role = rp.role";


    public AuthenticationController() {
    }

    public AuthenticationController(Vertx vertx, IQueryService queryService, DataSource source) {
        this.vertx = vertx;
        this.queryService = queryService;
        client = JDBCClient.create(vertx, source);
        authProvider = JDBCAuth.create(vertx, client);
        authProvider.setAuthenticationQuery(authenticationQuery);
        authProvider.setRolesQuery(rolesQuery);
        //authProvider.setPermissionsQuery(permissionsQuery);
        authProvider.setHashStrategy(JDBCHashStrategy.createPBKDF2(vertx));
    }

    /**
     * this method authenticates the user
     * the user is logged in after calling this method
     *
     * @param loginInfo
     * @return
     */
    public Future<io.vertx.ext.auth.User> loginUser(LoginDTO loginInfo) {
        Promise<io.vertx.ext.auth.User> promise = Promise.promise();
        JsonObject authInfo = new JsonObject().put("username", loginInfo.getUsername()).put("password", loginInfo.getPassword());
        authProvider.authenticate(authInfo, res -> {
            if (res.succeeded()) {
                io.vertx.ext.auth.User user = res.result();
                System.out.println("User " + user.principal().getString("username") + " is now authenticated");
                promise.complete(user);
            } else {
                System.out.println("User login failed for user " + loginInfo.getUsername());
                promise.fail("failed!");
            }
        });
        return promise.future();
    }

    /**
     * This method creates a salt for Authentication
     *
     * @return random String
     */
    public String generateSalt() {
        return authProvider.generateSalt();
    }

    /**
     * This method creates a random Password
     *
     * @param plainTextPassword Simple unencoded String with a password
     * @param salt              random String, Use generateSalt() for this
     * @return hashed password
     */
    public String hashPassword(String plainTextPassword, String salt) {
        return authProvider.computeHash(plainTextPassword, salt);
    }

    /**
     * this method saves a new user with the given information
     *
     * @param newUserInfo the information with is passed from the frontend to the backend
     * @return the fully saved user with id and everything
     */
    public Future<User> registerNewUser(NewUserDTO newUserInfo) {
        Promise<User> promise = Promise.promise();
        String salt = authProvider.generateSalt();
        PasswordGenerator passGen = new PasswordGenerator();
        String plainPassword = passGen.generateRandomPassword(10);
        System.out.println("temporary password for new user " + newUserInfo.getUsername() + ": " + plainPassword);
        String password = authProvider.computeHash(plainPassword, salt);
        User user = new User(newUserInfo.getUsername(), newUserInfo.getName(), password, salt, newUserInfo.getEmailAddress(), new HashSet<UserRole>(), Department.valueOf(newUserInfo.getDepartment()), newUserInfo.getOffice(), true, EmailFrequency.valueOf(newUserInfo.getEmailFrequency()));
        if (newUserInfo.getAdmin()) user.getRoles().add(new UserRole("Admin", user));
        if (newUserInfo.getUser()) user.getRoles().add(new UserRole("User", user));
        queryService.saveUser(user).setHandler(createUser -> {
            if (createUser.succeeded()) {
                User newUser = createUser.result();
                promise.complete(newUser);
                System.out.println(newUser.getUsername() + " has been saved in database");

                MailController.sendEmail(user.getEmailAddress(), "Ihre Zugangsdaten",
                        "Username: " + user.getUsername() + "/n" +
                                "Passwort: " + plainPassword + "/n" +
                                "Bitte ändern Sie Ihr Passwort schnellstmöglich in Ihrem Profil.", vertx);

            } else {
                promise.fail("failed");
                System.out.println("An Error occurred when saving new user");
            }
        });
        return promise.future();
    }
}

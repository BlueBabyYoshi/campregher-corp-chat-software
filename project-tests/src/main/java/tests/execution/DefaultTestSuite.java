package tests.execution;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import tests.base.controller.EmailControllerTest;
import tests.definition.DBVerticleTests;
import tests.definition.handlers.*;

/**
 * The type Default test suite.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses(
        {DBVerticleTests.class,
                ChannelHandlerTests.class,
                LoginHandlerTest.class,
                ResetPasswordHandlerTest.class,
                EmailControllerTest.class,
                UserHandlerTests.class, AuthHandlerTest.class}
)
public class DefaultTestSuite {
}

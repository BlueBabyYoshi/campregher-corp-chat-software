package lib.verticle.server.handlers.users;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Edit user handler.
 */
public interface IEditUserHandler extends IHttpHandler {
}

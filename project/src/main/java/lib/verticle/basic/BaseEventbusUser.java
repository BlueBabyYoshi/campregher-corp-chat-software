package lib.verticle.basic;

import com.google.inject.Inject;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;

import javax.persistence.EntityManager;

/**
 * The type Base eventbus user.
 */
public abstract class BaseEventbusUser {

    @Inject
    private EventBus eventBus;

    /**
     * normal Request.
     *
     * @param <T>     the type parameter
     * @param address the address
     * @param obj     the obj
     * @param options the options
     * @param handler the handler
     */
    protected <T> void request(String address, T obj, DeliveryOptions options, Handler<AsyncResult<Message<T>>> handler) {
        eventBus.request(address, obj, options, handler);
    }

    /**
     * Jinq request.
     *
     * @param <T>     the type parameter
     * @param address the address
     * @param em      the em
     * @param obj     the obj
     * @param options the options
     * @param handler the handler
     */
    protected <T> void jinqRequest(String address, EntityManager em, T obj, DeliveryOptions options, Handler<AsyncResult<Message<T>>> handler) {
        eventBus.request(address, em, options, handler);
    }
}

package lib.Database.datasources.implementors;

import lib.Database.datasources.DataSourceFunctionalityImplementor;

import javax.sql.DataSource;

/**
 * The Base data source functionality implementor.
 *
 * @param <T> the type parameter
 */
public abstract class BaseDataSourceFunctionalityImplementor<T extends DataSource> implements DataSourceFunctionalityImplementor {

    /**
     * Gets assumed data source type.
     *
     * @return the assumed data source type
     */
    protected abstract Class<T> getAssumedDataSourceType();

    /**
     * Sets url internal.
     *
     * @param source the source
     * @param url    the url
     */
    protected abstract void setUrlInternal(T source, String url);

    /**
     * Sets user internal.
     *
     * @param source the source
     * @param user   the user
     */
    protected abstract void setUserInternal(T source, String user);

    /**
     * Sets password internal.
     *
     * @param source the source
     * @param pw     the pw
     */
    protected abstract void setPasswordInternal(T source, String pw);

    @Override
    public void setUrl(DataSource source, String url) {
        ensureCorrectType(source);
        setUrlInternal((T) source, url);
    }

    @Override
    public void setUser(DataSource source, String user) {
        ensureCorrectType(source);
        setUserInternal((T) source, user);
    }

    @Override
    public void setPassword(DataSource source, String password) {
        ensureCorrectType(source);
        setPasswordInternal((T) source, password);
    }

    private void ensureCorrectType(DataSource source) {
        Class<T> assumedType = getAssumedDataSourceType();
        if (!assumedType.isAssignableFrom(source.getClass())) {
            throw new IllegalArgumentException("source does not inherit from assumed type:" + assumedType.getName() + ". This indicates a mismatch between picked implementor and actual implementation type.");
        }
    }
}

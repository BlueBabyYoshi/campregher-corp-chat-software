export interface NewUserState {
    username: string;
    name: string;
    emailAddress: string;
    isAdmin: boolean;
    isUser: boolean;
    department: string;
    office: string;
    emailFrequency: string;
    result: string;
}
import * as React from "react";
import { Select, InputAdornment, FormControl, Fab } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import SendIcon from "@material-ui/icons/Send";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import SettingsIcon from "@material-ui/icons/Settings";
import ToggleButton from "@material-ui/lab/ToggleButton";
import ToggleButtonGroup from "@material-ui/lab/ToggleButtonGroup";
import KeyboardBackspaceIcon from "@material-ui/icons/KeyboardBackspace";
import Avatar from "@material-ui/core/Avatar";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";
import InputLabel from "@material-ui/core/InputLabel";
import DeleteIcon from '@material-ui/icons/Delete';
import SaveIcon from '@material-ui/icons/Save';
import MenuItem from '@material-ui/core/MenuItem';
import Checkbox from '@material-ui/core/Checkbox';
import GroupAddIcon from '@material-ui/icons/GroupAdd';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import EditIcon from '@material-ui/icons/Edit';
import { SettingsChannel } from './SettingsChannel';
import AddIcon from '@material-ui/icons/Add';
import CloseIcon from '@material-ui/icons/Close';

//Interfaces
import { Message } from "./Message";
import { MessageDTO } from "../../Dtos/MessageDTO";
import { MessageProperties } from "./MessageProperties";
import { State } from "./State";
import { ChannelUser } from "./ChannelUser";
import { ChannelDTO } from '../../Dtos/ChannelDTO';

//API
const APIallMessages = "http://localhost:8080/allMessages";
const APISetMessages = "http://localhost:8080/setMessage";
const APIDeleteMessage = "http://localhost:8080/deleteMessage";
const APIMessageEdit = "http://localhost:8080/editMessage"

const APIChannels = "http://localhost:8080/channels"
const APIRequestChannelMessages = "http://localhost:8080/allMessages"
const APIChannelDelete = "http://localhost:8080/channels/delete"
const APIRequestChannelSettings = "http://localhost:8080/channels/settings"

const APIeditUsersInChannel = "http://localhost:8080/channels/editUsers"
const APIeditAModeratorInChannel = "http://localhost:8080/channels/editMods"

const APIEeditOhterSettings = "http://localhost:8080/channels/edit";
const APIfilesUpload = "http://localhost:8080/files/uploadFile"

//Websocket
//var client; 

/**
 * This class contains the whole conversation logic: 
 * - display all channels
 * - display all  messages 
 * - edit channel and message
 * - write new message 
 * 
 * The class has the following structure 
 * - inizialization  
 * - for loading
 * - for responsive Design
 * - channels 
 *  -- handler 
 *  -- setter
 *  -- dialogs
 *  -- views 
 * - messages 
 *  -- handler 
 *  -- setter
 *  -- dialogs
 *  -- upload
 *  -- views  
 * - render everyting
 */
export class MonisMessages extends React.Component<MessageProperties, State> {

  /*------------------------------------------------------------------------------------------------------------------------------
   Initiliziation 
  -------------------------------------------------------------------------------------------------------------------------------- */

  public state: State = {
    /*------------------ for channels---------------------*/
    //list channels 
    channels: [],
    channelsTypeSelection: [],
    selectedChannelId: -1,

    //channel settings  
    channelName: "Test",
    isChannelPublic: false,
    isChannelLocked: false,
    channelUsers: [],
    currentUserIsMod: false,
    editMessageChannelId: -1,

    /*----------------- for messages ---------------------*/
    //list messages 
    messages: [],

    //edit messages 
    editMessageId: -1,
    editMessageText: "",
    deleteMessageId: -1,
    deleteMessageChannelId: -1,

    //creat new messages 
    messageToSend: "",

    /*------------------view&dialogs ---------------------*/
    viewChannels: true,
    switchViewSmartPhone: true,
    desktopView: false,
    screenWidth: window.innerWidth,
    uploadCompleting: false,

    //Dialogs
    openSettingsDialog: false,
    openConfirmDialogDeleteChannel: false,
    openConfirmDialogDeleteMessage: false,
    openEditDialog: false,
  }


  constructor(props: MessageProperties) {
    super(props);
    this.getAllChannels = this.getAllChannels.bind(this);
    this.onButtonClickSendMessages = this.onButtonClickSendMessages.bind(this);
    this.onMessageEnteredToEdit = this.onMessageEnteredToEdit.bind(this);
    this.chatConversation = this.chatConversation.bind(this)
    this.inputMessage = this.inputMessage.bind(this);
    this.setOpenDialogEdit = this.setOpenDialogEdit.bind(this);
    this.setCloseDialogEdit = this.setCloseDialogEdit.bind(this);
    this.listChannels = this.listChannels.bind(this);
    this.setOnlyPrivateChannels = this.setOnlyPrivateChannels.bind(this);
    this.setAllChannels = this.setAllChannels.bind(this);
    this.handleResize = this.handleResize.bind(this);
    this.toggleBettweenChannelsListAndMessageSmartphoneView = this.toggleBettweenChannelsListAndMessageSmartphoneView.bind(this);
    this.getMessageFromSelectedChannel = this.getMessageFromSelectedChannel.bind(this);
    this.channelSettingsBar = this.channelSettingsBar.bind(this);
    this.setOpenSettingsDialog = this.setOpenSettingsDialog.bind(this);
    this.setCloseSettingsDialog = this.setCloseSettingsDialog.bind(this);
    this.setChannelPublic = this.setChannelPublic.bind(this);
    this.setChangeChannelName = this.setChangeChannelName.bind(this);
    this.setOpenConfirmDeleteChannelDialog = this.setOpenConfirmDeleteChannelDialog.bind(this);
    this.setCloseConfirmDeleteChannelDialog = this.setCloseConfirmDeleteChannelDialog.bind(this);
    this.handleDeleteChannel = this.handleDeleteChannel.bind(this);
    this.dialogDeleteMessages = this.dialogDeleteMessages.bind(this);
    this.setOpenConfirmDeleteMessageDialog = this.setOpenConfirmDeleteMessageDialog.bind(this);
    this.setCloseConfirmDeleteMessageDialog = this.setCloseConfirmDeleteMessageDialog.bind(this);
    this.toRGB = this.toRGB.bind(this);
    this.saveEditMessage = this.saveEditMessage.bind(this);
    this.editMessageDialog.bind(this);
    this.handleDeleteMessage = this.handleDeleteMessage.bind(this);
    this.setChannelLocked = this.setChannelLocked.bind(this);
    this.getSettingsFromSelectedChannel = this.getSettingsFromSelectedChannel.bind(this);
    this.setChanelAdmins = this.setChanelAdmins.bind(this);
    this.setChanelUsers = this.setChanelUsers.bind(this);
    this.saveSettingsInBackend = this.saveSettingsInBackend.bind(this);
    this.saveOtherSettings = this.saveOtherSettings.bind(this);
    this.setNewMessageChangehandler = this.setNewMessageChangehandler.bind(this);
    this.userListSettings = this.userListSettings.bind(this);
    this.adminsListSettings = this.adminsListSettings.bind(this);
    this.newChannel = this.newChannel.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
    this.readFileAsync = this.readFileAsync.bind(this);
    this.processFile = this.processFile.bind(this);
  }

  /**
   * Inizializiation event handlers 
   * --> every 1.1 sec: update for new messages 
   * --> every 20 sec: update for new channels 
   * --> window event handler -> switching between desktop and smartphone view 
   */
  componentDidMount() {
    this.getAllChannels();
    setInterval(() => this.getMessageFromSelectedChannel(), 1100);
    setInterval(() => this.getAllChannels(), 20000);
    window.addEventListener("resize", this.handleResize);
    this.handleResize();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleResize);
  }

  /*------------------------------------------------------------------------------------------------------------------------------
    for responsive Design
  -------------------------------------------------------------------------------------------------------------------------------- */

  //URL: https://gist.github.com/0x263b/2bdd90886c2036a1ad5bcf06d6e6fb37
  toRGB(a: string) {
    var hash = 0;
    if (a.length === 0) return hash;
    for (var i = 0; i < a.length; i++) {
      hash = a.charCodeAt(i) + ((hash << 5) - hash);
      hash = hash & hash;
    }
    var rgb = [0, 0, 0];
    for (var i = 0; i < 3; i++) {
      var value = (hash >> (i * 8)) & 255;
      rgb[i] = value;
    }
    return "backgroundColor: rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
  }

  /**
   * Switching between the smartphone and the desktop view 
   * @var desktopView is true if screen size > 600
   * @var switchViewSmartPhone --> switch between MESSAGES and CHANELLS view 
   */
  handleResize() {
    if (window.innerWidth < 600) {
      this.setState({
        ...this.state,
        screenWidth: window.innerWidth,
        desktopView: false,
        switchViewSmartPhone: true,
      });

    } else {
      this.setState({
        ...this.state,
        screenWidth: window.innerWidth,
        switchViewSmartPhone: true,
        desktopView: true,
      });
    }
  }

  /**
   * switch between MESSAGES and CHANELLS view 
   */
  toggleBettweenChannelsListAndMessageSmartphoneView() {
    this.setState({
      ...this.state,
      switchViewSmartPhone: !this.state.switchViewSmartPhone,
    });

  }

  /*------------------------------------------------------------------------------------------------------------------------------
     channels 
  -------------------------------------------------------------------------------------------------------------------------------- */

  //--------------------------------------------------HANDLER---------------------------------------

  /**
   * receive all channels from the logged in user 
   */
  public async getAllChannels(): Promise<void> {
    let response = await fetch(APIChannels, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "GET",
      mode: "cors",
      credentials: "include"
    });
    let jsonResponse = await response.json(); //parsed JSON object
    let jsonObject: ChannelDTO[] = JSON.parse(JSON.stringify(jsonResponse));
    this.setState({
      ...this.state,
      channels: jsonObject,
      channelsTypeSelection: jsonObject,
    });
    this.getMessageFromSelectedChannel();
  }

  /**
   * receive all messages for a selected channel in the channelList 
   */
  public async getMessageFromSelectedChannel(): Promise<void> {
    var requestChannel = ({
      channelId: this.state.selectedChannelId
    });
    /* FOR WEBSOCKETS OLD 
    console.log("Webserver wo bist du?");
    //https://blog.logrocket.com/websockets-tutorial-how-to-go-real-time-with-node-and-react-8e4693fbf843/ --> for websocket handshake with backend

    / client.send(myChannelId);
    client = W3CWebSocket('ws://127.0.0.1:8887');
    client.onopen = () => {
      console.log('WebSocket Client Connected');
      client.send(myChannelId);
    };

    client.onmessage = (message) => {
      console.log(message);
    };
    */
    let response = await fetch(APIRequestChannelMessages, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(requestChannel),
      mode: "cors",
      credentials: "include"
    });

    let jsonResponse = await response.json();
    let jsonObject: MessageDTO[] = JSON.parse(JSON.stringify(jsonResponse));


    this.setState({
      ...this.state,
      messages: jsonObject
    });
  }


  //send selected channel to backend and receive settings 
  public async getSettingsFromSelectedChannel(): Promise<void> {

    var requestChannel = ({
      channelId: this.state.selectedChannelId
    });

    let response = await fetch(APIRequestChannelSettings, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(requestChannel),
      mode: "cors",
      credentials: "include"
    });

    let jsonResponse = await response.json();
    let jsonObject: SettingsChannel = JSON.parse(JSON.stringify(jsonResponse));

    this.setState({
      ...this.state,
      channelName: jsonObject.channelName,
      isChannelPublic: jsonObject.isPublic,
      isChannelLocked: jsonObject.isLocked,
      channelUsers: jsonObject.userModList,
      currentUserIsMod: jsonObject.currentUserIsMod,
    });
  }

  public async handleDeleteChannel(): Promise<void> {
    var deletedChannelId = ({
      channelId: this.state.selectedChannelId
    });

    await fetch(APIChannelDelete, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
      body: JSON.stringify(deletedChannelId),
      mode: "cors",
      credentials: "include"
    })

    this.setState({
      ...this.state,
      selectedChannelId: -1
    });

    this.getAllChannels();
    this.setCloseConfirmDeleteChannelDialog();
  }

  /**
   * Method to save edited setting for a selected channel 
   * --> save all users 
   * --> save all moderators 
   * --> save other settings: channel name, public and locked  
   */
  async saveSettingsInBackend() {
    await this.userListSettings();
    await this.saveOtherSettings();
    await this.adminsListSettings();

    this.getAllChannels();
    this.setCloseSettingsDialog();
  };

  /**
  * save other users in settings 
  */
  public async userListSettings() {
    var simpleUsersInChannelList: number[] = [];
    let usersInChannel: ChannelUser[] = this.state.channelUsers;

    for (let user of usersInChannel) {
      if (user.isUser) {
        simpleUsersInChannelList.push(user.userId);
      }
    }

    var settingsChannelUsers = ({
      channelId: this.state.selectedChannelId,
      userIds: simpleUsersInChannelList
    });

    await fetch(APIeditUsersInChannel, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(settingsChannelUsers),
      mode: "cors",
      credentials: "include"
    });
  };


  /**
   * save moderators in settings 
   */
  public async adminsListSettings() {
    var simpleModeratorsInChannelList: number[] = [];
    let usersInChannel: ChannelUser[] = this.state.channelUsers;

    for (let user of usersInChannel) {
      if (user.isModerator) {
        simpleModeratorsInChannelList.push(user.userId);
      }
    }

    var settingsChannelUsers = ({
      channelId: this.state.selectedChannelId,
      userIds: simpleModeratorsInChannelList
    });

    await fetch(APIeditAModeratorInChannel, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(settingsChannelUsers),
      mode: "cors",
      credentials: "include"
    });
  };


  /**
   * saver other settings (not users and administrators --> extra function): name, public and locked 
   */
  public async saveOtherSettings() {
    var settings = ({
      name: this.state.channelName,
      isPublic: this.state.isChannelPublic,
      isLocked: this.state.isChannelLocked,
      channelId: this.state.selectedChannelId
    });

    await fetch(APIEeditOhterSettings, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(settings),
      mode: "cors",
      credentials: "include"
    });

  };
  /**
   * create a new channel 
   */
  async newChannel() {
    var channelName = "Neuer Channel";

    await fetch("http://localhost:8080/newChannel", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: channelName,
      mode: "cors",
      credentials: "include"
    });
    window.location.reload();
  }

  //----------------------------------------------SETTER---------------------------------------
  setChanelAdmins(id: number) {
    var temporaryChannelUsers: ChannelUser[] = this.state.channelUsers;
    var i = temporaryChannelUsers.findIndex((obj => obj.userId == id));

    temporaryChannelUsers[i].isModerator = !temporaryChannelUsers[i].isModerator;

    this.setState({
      ...this.state,
      channelUsers: temporaryChannelUsers
    });
  }

  setChanelUsers(id: number) {
    var temporaryChannelUsers: ChannelUser[] = this.state.channelUsers;
    var i = temporaryChannelUsers.findIndex((obj => obj.userId == id));

    temporaryChannelUsers[i].isUser = !temporaryChannelUsers[i].isUser;

    this.setState({
      ...this.state,
      channelUsers: temporaryChannelUsers
    });
  }

  setOpenSettingsDialog() {
    this.getSettingsFromSelectedChannel();
    this.getSettingsFromSelectedChannel();

    this.setState({
      ...this.state,
      openSettingsDialog: true,
    });
  }

  setChangeChannelName = event => {
    this.setState({
      ...this.state,
      channelName: event.target.value
    });
  }

  setCloseSettingsDialog() {
    this.setState({
      ...this.state,
      openSettingsDialog: false
    });
  }

  setChannelPublic() {
    this.setState({
      ...this.state,
      isChannelPublic: !this.state.isChannelPublic
    });
  }

  setChannelLocked() {
    this.setState({
      ...this.state,
      isChannelLocked: !this.state.isChannelLocked
    });
  }

  setOpenConfirmDeleteChannelDialog() {
    this.setState({
      ... this.state,
      openConfirmDialogDeleteChannel: true,
    });
  }

  setCloseConfirmDeleteChannelDialog() {
    this.setState({
      ... this.state,
      openConfirmDialogDeleteChannel: false
    });
  }

  setOnlyPrivateChannels() {
    this.setState({
      ...this.state,
      channelsTypeSelection: this.state.channels.filter(x => x.isPublic)
    });
  }

  setAllChannels() {
    this.setState({
      ...this.state,
      channelsTypeSelection: this.state.channels
    });
  }

  async setNewChannel(id: number) {
    this.setState({
      ...this.state,
      selectedChannelId: id
    })

    await this.getMessageFromSelectedChannel();
    this.getSettingsFromSelectedChannel();

    if (this.state.screenWidth < 800) {
      this.toggleBettweenChannelsListAndMessageSmartphoneView();
    }
  }

  //----------------------------------------------VIEWS---------------------------------------

  /**
 * Returns the view for the settings bar of a channel 
 */
  channelSettingsBar = () => {
    //list for users to add/remove from channel 
    var listUserChannel = this.state.channelUsers.map((obj, i = 0) => (
      <MenuItem id={"userSettingsUsers" + obj.username + obj.userId} value={obj.username} onClick={() => this.setChanelAdmins(obj.userId)}  >
        <Checkbox checked={obj.isUser} onClick={() => this.setChanelUsers(obj.userId)} />
        <ListItemText primary={obj.username} />
      </MenuItem>
    ));

    //list for users to add/remove from channel 
    var listModeratorsChannel = this.state.channelUsers.map((obj, i = 0) => (
      <MenuItem id={"userSettingsAdmin" + obj.username + obj.userId} value={obj.username} onClick={() => this.setChanelAdmins(obj.userId)}  >
        <Checkbox checked={obj.isModerator} onClick={() => this.setChanelAdmins(obj.userId)} />
        <ListItemText primary={obj.username} />
      </MenuItem>
    ));

    //
    var settingsForm = (
      <div style={{ margin: "10px" }}>
        <div>
          <FormControlLabel style={{ margin: "10px" }}
            control={<Switch checked={this.state.isChannelPublic} onClick={this.setChannelPublic} value="gilad" />}
            label="Zuordnung: Privat?"
          />
        </div>
        <div>
          <FormControlLabel style={{ margin: "10px" }}
            control={<Switch checked={this.state.isChannelLocked} onClick={this.setChannelLocked} value="gilad" />}
            label="Gesperrt"
          />
        </div>
        <div>
          <TextField style={{ margin: "10px" }} variant="outlined" label="Channel Name"
            defaultValue={this.state.channelName} onChange={this.setChangeChannelName} fullWidth />
        </div>
        <div>
          <FormControl fullWidth style={{ margin: "10px" }}>
            <InputLabel id="demo-mutiple-name-label">Users in Channel</InputLabel>
            <Select
              labelId="usersListSettings"
              startAdornment={
                <InputAdornment position="start">
                  <GroupAddIcon />
                </InputAdornment>
              }
            >
              {listUserChannel}
            </Select>
          </FormControl>
        </div>
        <div>
          <FormControl fullWidth style={{ margin: "10px" }}>
            <InputLabel id="demo-mutiple-name-label">Admins in Channel</InputLabel>
            <Select
              labelId="adminsListSettings"
              startAdornment={
                <InputAdornment position="start">
                  < LockOpenIcon />
                </InputAdornment>
              }
              defaultValue="Admins"
            >
              {listModeratorsChannel}
            </Select>
          </FormControl>
        </div>
        <div style={{ margin: "10px" }}>
          <Fab variant="extended" aria-label="delete" size="medium" onClick={this.saveSettingsInBackend} style={{ margin: "10px" }}>
            <SaveIcon />
            Speichern
          </Fab>
          <Fab variant="extended" aria-label="delete" size="medium" style={{ margin: "10px" }} onClick={this.setCloseSettingsDialog}>
            <CloseIcon />
            Close
          </Fab>
        </div>
      </div >
    );

    return (
      <div>
        <IconButton
          disabled={!this.state.currentUserIsMod}
          aria-label="delete"
          color="primary"
          onClick={this.setOpenSettingsDialog}
        >
          <SettingsIcon />
        </IconButton>
        <IconButton
          disabled={!this.state.currentUserIsMod}
          aria-label="delete"
          color="primary"
          onClick={this.setOpenConfirmDeleteChannelDialog}
        >
          <DeleteIcon />
        </IconButton>

        <Dialog
          open={this.state.openSettingsDialog}
          onClose={this.setCloseSettingsDialog}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="form-dialog-title">Settings</DialogTitle>
          <DialogContent>
            {settingsForm}
          </DialogContent>
        </Dialog>

        <Dialog open={this.state.openConfirmDialogDeleteChannel} >
          <DialogTitle>Bestätigen</DialogTitle>
          <DialogContent>
            <div>Möchten Sie diesen Channel löschen?</div>
            <div style={{ margin: "10px", textAlign: "center" }}>
              <Button color="secondary" variant="outlined" onClick={this.handleDeleteChannel}>
                Ja
              </Button>
              <Button onClick={this.setCloseConfirmDeleteChannelDialog} variant="outlined" color="secondary" >
                Nein
              </Button>
            </div>
          </DialogContent>
        </Dialog>

      </div>
    );
  }

  /**
   * Returns the view for list all channels 
   */
  listChannels = () => {
    const listChannels = this.state.channelsTypeSelection.map((obj, i = 0) => (
      <ListItem id={"listChannels" + obj.id} button style={{ backgroundColor: `${obj.id == this.state.selectedChannelId ? "rgb(176, 196, 222)" : "transparent"}` }}>
        <ListItemText primary={obj.channelName} onClick={() => this.setNewChannel(obj.id)} />
      </ListItem>
    ));

    return (
      <div arial-label="text formatting">
        <div style={{ textAlign: "center", padding: "10px" }}>
          <ToggleButtonGroup
            exclusive
            color="prigmary"
            aria-label="outlined primary button group"
          >
            <ToggleButton value="all" aria-label="left aligned" onClick={() => this.setAllChannels()} >
              All
            </ToggleButton>
            <ToggleButton value="private" aria-label="left aligned" onClick={() => this.setOnlyPrivateChannels()} >
              Private
            </ToggleButton>
          </ToggleButtonGroup>
          <IconButton
            aria-label="delete"
            color="primary"
            onClick={this.newChannel}
          >
            <AddIcon />
          </IconButton>
        </div>
        <List component="nav" aria-label="secondary mailbox folders">
          {listChannels}
        </List>
      </div>
    );
  };


  /*------------------------------------------------------------------------------------------------------------------------------
    MESSAGES 
  --------------------------------------------------------------------------------------------------------------------------------*/

  //----------------------------------------------Handlers---------------------------------------


  /**
   * send a new Message 
   */
  public async onButtonClickSendMessages(): Promise<void> {

    var messageSend = {
      text: this.state.messageToSend,
      channelid: this.state.selectedChannelId
    };
    var response = await fetch(APISetMessages, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(messageSend),
      mode: "cors",
      credentials: "include"
    })

    this.setState({
      ...this.state,
      messageToSend: ""
    });

  }

  public onMessageEnteredToEdit(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({
      ...this.state,
      editMessageText: event.target.value
    });
  }


  /**
   * deletes the selected message  
   */
  public async handleDeleteMessage(): Promise<void> {

    var deletedMessageId = ({
      id: this.state.deleteMessageId,
      channelId: this.state.deleteMessageChannelId
    });

    await fetch(APIDeleteMessage, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "DELETE",
      body: JSON.stringify(deletedMessageId),
      mode: "cors",
      credentials: "include"
    })

    this.setCloseConfirmDeleteMessageDialog();
  }


  /**
 * save the selected, edited message  
 */
  public async saveEditMessage(): Promise<void> {
    var editMessage = ({
      id: this.state.editMessageId,
      text: this.state.editMessageText
    });

    await fetch(APIMessageEdit, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(editMessage),
      mode: "cors",
      credentials: "include"
    })

    this.setCloseDialogEdit();
  }

  /**
 * handler to upload the file to backend 
 * @param filename 
 * @param binary the data from the file encoded in Base64 
 */
  async uploadFile(filename: string, binary: string) {
    var sendData = ({
      channelId: this.state.selectedChannelId,
      name: filename,
      data: binary
    });

    await fetch(APIfilesUpload, {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify(sendData),
      mode: "cors",
      credentials: "include"
    });
  }

  //----------------------------------------------Setters ---------------------------------------
  setOpenConfirmDeleteMessageDialog(message: Message) {
    this.setState({
      ... this.state,
      openConfirmDialogDeleteMessage: true,
      deleteMessageId: message.id,
      deleteMessageChannelId: message.channelId
    });
  }

  setCloseConfirmDeleteMessageDialog() {
    this.setState({
      ... this.state,
      openConfirmDialogDeleteMessage: false
    });
  }

  public setOpenDialogEdit(message: Message) {
    this.setState({
      ...this.state,
      openEditDialog: true,
      editMessageId: message.id,
      editMessageText: message.text
    });
  }

  public setCloseDialogEdit() {
    this.setState({
      ...this.state,
      openEditDialog: false,
    });
  }

  setNewMessageChangehandler(event: React.ChangeEvent<HTMLInputElement>): void {
    this.setState({
      ...this.state,
      messageToSend: event.target.value
    });
  }

  //----------------------------------------------Dialogs ---------------------------------------

  /**
   * This dialog is to confirm to delte the selected message 
   */
  public dialogDeleteMessages() {
    return (
      <div>
        <Dialog open={this.state.openConfirmDialogDeleteMessage}>
          <DialogTitle>Bestätigen</DialogTitle>
          <DialogContent>
            <div>Möchten Sie diese Nachricht löschen?</div>
            <div style={{ margin: "10px", textAlign: "center" }}>
              <Button color="secondary" variant="outlined" onClick={this.handleDeleteMessage}>
                Ja
             </Button>
              <Button onClick={this.setCloseConfirmDeleteMessageDialog} variant="outlined" color="secondary" >
                Nein
              </Button>
            </div>
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  /**
   * This dialog to edit the selected message 
   */
  public editMessageDialog() {
    return (
      <div>
        <Dialog open={this.state.openEditDialog}>
          <DialogContent>
            Bearbeiten sie folgenden Text
            <TextField
              defaultValue={this.state.editMessageText}
              //value --> set a value and later replace the old message with this value
              onChange={this.onMessageEnteredToEdit}
              rows={3}
              multiline
              aria-label="maximum height"
              placeholder="Maximum 4 rows"
              variant="outlined"
              fullWidth={true}
            />
            <Button
              variant="outlined"
              color="primary"
              onClick={this.saveEditMessage}
            >
              Speichern
              </Button>
            <Button
              variant="outlined"
              color="primary"
              onClick={this.setCloseDialogEdit}
            >Abbrechen</Button>
          </DialogContent>
        </Dialog>
      </div>
    );
  }

  //----------------------------------------------File upload---------------------------------------

  /**
 * read the input file and encode it into Base64 
 * @param filename 
 * @param binary the data from the file encoded in Base64 
 */
  readFileAsync(file) {
    return new Promise((resolve, reject) => {
      let reader = new FileReader();

      reader.onload = () => {
        resolve(reader.result);
      };

      reader.onerror = reject;

      reader.readAsDataURL(file);
    })
  }


/**
 * This method is to monitor the file upload, it starts the upload progress displaying a message and ends the progress. 
 * @param files 
 */
  async processFile(files: FileList) {
    try {
      let file = files[0];

      this.setState({
        ...this.state,
        uploadCompleting: true
      })

      let contentBuffer = await this.readFileAsync(file).then((binary: string) => {
        this.uploadFile(file.name, binary);
      });

      this.setState({
        ...this.state,
        uploadCompleting: false
      })

    } catch (err) {
      console.log(err);
    }
  }

  //----------------------------------------------Views---------------------------------------
  /**
   * This "view" displays all messages for the selected channel.
   */
  chatConversation = () => {
    var chatBubbles = this.state.messages.map((obj, i = 0) => (
      <div
        style={{
          justifyContent: `${obj.myMessage ? "flex-end" : "flex-start"}`,
          display: "flex",
        }}
        key={i}
      >

        <div>
          <Avatar style={{ backgroundColor: `${this.toRGB(obj.username)}` }} >
            {obj.username.substring(0, 2)}
          </Avatar>
        </div>

        <div
          key={i++}
          style={{
            border: "0.5px solid black",
            background: "rgb(176, 196, 222)",
            borderRadius: "10px",
            margin: "5px",
            padding: "10px",
            display: "inline-block",
            maxWidth: "60%"
          }}
        >

          <div>
            <b>{obj.username}</b><br></br>
            {obj.text}
            <IconButton
              onClick={() => this.setOpenDialogEdit(obj)}
              disabled={!obj.myMessage} //save obj as a temporary Message for edit in dialog
              size="small"
            >


              <EditIcon />
            </IconButton>
            <IconButton
              disabled={!obj.myMessage && !this.state.currentUserIsMod}
              size="small"
              onClick={() => this.setOpenConfirmDeleteMessageDialog(obj)}
            >
              <DeleteIcon />
            </IconButton>
          </div>
          <div style={{ textAlign: "end" }}>
          </div>
        </div>
      </div>
    ))

    return (
      <div>
        {chatBubbles}
        {this.editMessageDialog()}
        {this.dialogDeleteMessages()}
      </div>
    );
  }


  /**
   * This view is needed to for the input of a new message. 
   */
  inputMessage = () => {
    return (
      <div style={{ paddingTop: "20px" }}>
        <div style={{
          display: "flex",
          alignItems: "flex-start"
        }}>
          <TextField
            onChange={this.setNewMessageChangehandler}
            rows={3}
            multiline
            aria-label="maximum height"
            placeholder="Maximum 4 rows"
            value={this.state.messageToSend}
            variant="outlined"
            fullWidth={true}
            disabled={this.state.selectedChannelId == -1}
          />
          <IconButton aria-label="delete" color="primary" onClick={this.onButtonClickSendMessages}>
            <SendIcon />
          </IconButton>
        </div>
        <div style={{ padding: "20px" }}>
          Select and upload: <input type="file" onChange={(e) => e.target.files != null ? this.processFile(e.target.files) : console.log("ok")} disabled={this.state.selectedChannelId == -1} />
        </div>
        <Dialog id="dialoguploading" open={this.state.uploadCompleting}>
          <DialogContent>
            Uploading in progress ...
          </DialogContent>
        </Dialog>
      </div>
    );
  };


  /*------------------------------------------------------------------------------------------------------------------------------
    Render everything
  -------------------------------------------------------------------------------------------------------------------------------- */
  render() {
    return (
      <div>
        <div style={{
          display: "flex",
          justifyContent: "center",
          minHeight: "80vh"
        }}>

          <div style={{ flexGrow: 1 }}>
            {(this.state.desktopView || this.state.switchViewSmartPhone) && this.listChannels()}
          </div>

          {(this.state.desktopView || !this.state.switchViewSmartPhone) && (

            <div style={{
              flexGrow: 8,
              padding: "20px",
              display: "block",
              maxHeight: "80vh",
              overflowX: "hidden",
              scrollBehavior: "smooth"
            }}>
              <div>
                <div style={{ float: "left", width: "95%" }}>
                  {!this.state.desktopView && (
                    <IconButton color="primary" onClick={() => this.toggleBettweenChannelsListAndMessageSmartphoneView()}>
                      <KeyboardBackspaceIcon />
                    </IconButton>
                  )}
                  {this.state.selectedChannelId == -1 && (<div><h1 style={{color: 'skyblue'}}>Wählen sie einen Channel!</h1></div>)
                  }
                  {this.chatConversation()}
                  {this.inputMessage()}
                </div>
                <div style={{ float: "left", width: "5%" }}>
                  {this.channelSettingsBar()}
                </div>
              </div>

            </div>
          )}
        </div>
      </div >
    );
  }

}
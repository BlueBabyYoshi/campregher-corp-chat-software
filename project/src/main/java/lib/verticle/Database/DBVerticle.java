package lib.verticle.Database;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.eventbus.Message;
import lib.Database.*;
import lib.model.BaseModel;
import lib.verticle.eventbus.codecs.PersistenceMessageCodec;
import org.jinq.jpa.JPAJinqStream;
import org.jinq.orm.stream.JinqStream;

import javax.persistence.EntityManager;

/**
 * The DBBVerticle.
 */
public class DBVerticle extends AbstractVerticle implements IDBVerticle {

    private IDataBroker service = Persistence.getDataBroker(Connections.DEFAULT);

    @Override
    public void start(Future<Void> startFuture) {
        vertx.eventBus().registerCodec(new PersistenceMessageCodec());
        vertx.eventBus().consumer("database-call", this::onMessage);
        System.out.println("Verticle Deployed!");
        startFuture.complete();
    }


    private <T extends BaseModel> Class<?> getEntityType(Message<T> msg) throws ClassNotFoundException {
        return Class.forName(msg.headers().get("entityType"));
    }

    /**
     * On message, hand over message to right method depending on attached CrudAction to the message head.
     *
     * @param <T> the type parameter
     * @param msg the msg
     */
    public <T extends BaseModel> void onMessage(Message msg) {
        Message<T> castedMessage = msg;
        String action = getAction(castedMessage);
        switch (action) {
            case CRUDActions.SAVE:
                saveObject(castedMessage);
                break;
            case CRUDActions.SAVE_OR_UPDATE:
                saveOrUpdateObject(castedMessage);
                break;
            case CRUDActions.FORCE_DELETE:
                deleteObject(castedMessage);
                break;
            case CRUDActions.READ:
                read(castedMessage);
                break;
            case CRUDActions.READ_LIST:
                readList(castedMessage);
                break;
            case CRUDActions.DELETE:
                logicalDeleteObject(castedMessage);
                break;
            case CRUDActions.JINQ:
                jinqQuery(castedMessage);
                break;
        }
    }

    private <T extends BaseModel> void readList(Message<T> castedMessage) {
        getJinqQuery(castedMessage).setHandler(r -> {
            var list = r.result().toList();
            try {
                castedMessage.reply(list, PersistenceOption.createCRUDOption(CRUDActions.READ_LIST, getEntityType(castedMessage)));
            } catch (ClassNotFoundException ex) {
                System.out.println("class not found " + ex.toString());
                ex.printStackTrace();
            }
        });
    }

    private <T extends BaseModel> void read(Message<T> castedMessage) {
        var objId = castedMessage.body().getId();
        getJinqQuery(castedMessage).setHandler(r -> {
            JinqStream<T> objectStream = r.result();
            var object = objectStream.where(r2 -> r2.getId() == objId).findFirst().orElse(null);
            try {
                castedMessage.reply(object, PersistenceOption.createCRUDOption(CRUDActions.READ, getEntityType(castedMessage)));
            } catch (ClassNotFoundException ex) {
                System.out.println("class not found " + ex.toString());
                ex.printStackTrace();
            }
        });
    }

    private <T extends BaseModel> void saveObject(Message<T> msg) {
        try {
            T newObject = getObject(msg);
            vertx.executeBlocking(promise -> promise.complete(service.saveOrUpdate(newObject)), res -> msg.reply(res.result(), PersistenceOption.createCRUDOption(CRUDActions.SAVE, newObject.getClass())));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            msg.reply("Error!");
        }
    }


    private <T extends BaseModel> void saveOrUpdateObject(Message<T> msg) {
        try {
            T newObject = msg.body();
            vertx.executeBlocking(promise -> promise.complete(service.saveOrUpdate(newObject)), res -> msg.reply(res.result(), PersistenceOption.createCRUDOption(CRUDActions.SAVE_OR_UPDATE, newObject.getClass())));
        } catch (Exception ex) {
            msg.reply("Error!");
        }
    }

    private <T extends BaseModel> void logicalDeleteObject(Message<T> msg) {
        T obj = msg.body();
        vertx.executeBlocking(promise -> {
            obj.setDeleted(true);
            service.saveOrUpdate(obj);
            promise.complete("Object of type " + obj.getClass().getSimpleName() + " has been set to deleted.");
        }, r -> msg.reply(r.result().toString() + " is now marked as deleted."));
    }

    private <T extends BaseModel> void deleteObject(Message<T> msg) {
        T newObject = msg.body();
        try {
            vertx.executeBlocking(promise -> {
                service.deleteObject(newObject.getClass(), newObject.getId());
                promise.complete((newObject.getClass() + " with ID: " + newObject.getId()));
            }, res -> msg.reply(res.result().toString() + "got Deleted!"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    private <T extends BaseModel> void jinqQuery(Message<T> msg) {

        try {
            Class<T> entityType = (Class<T>) getEntityType(msg);
            getJinqQuery(msg).setHandler(res -> msg.reply(res.result(), PersistenceOption.createCRUDOption(CRUDActions.JINQ, entityType)));
        } catch (Exception ex) {
            msg.reply(null);
        }
    }

    private <T extends BaseModel> Future<JPAJinqStream<T>> getJinqQuery(Message<T> msg) {
        Promise<JPAJinqStream<T>> pro = Promise.promise();
        try {
            Class<T> entityType = (Class<T>) getEntityType(msg);
            vertx.executeBlocking(promise -> promise.complete(executeQuery(entityType, service.getEntityManager())), res -> {
                if (JPAJinqStream.class.isAssignableFrom(res.result().getClass())) {
                    pro.complete((JPAJinqStream<T>) res.result());
                }
            });
        } catch (Exception ex) {
            msg.reply(null);
        }
        return pro.future();
    }

    private <T extends BaseModel> T getObject(Message<T> msg) {
        try {
            T obj = msg.body();
            return obj;
        } catch (Exception ex) {
            System.out.println("Could not get Object from msg.");
        }
        return null;
    }

    private <T extends BaseModel> JPAJinqStream<T> executeQuery(Class<T> type, EntityManager em) {
        return service.getQuery(type, em).where(r -> r.isDeleted() == false);
    }

    private <T> String getAction(Message<T> msg) {
        return msg.headers().get("action");
    }
}

package lib.verticle.server.handlers.channels.notUsedAnymore;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import lib.model.Channel;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.channels.ChannelHandler;
import lib.verticle.server.handlers.channels.dtos.ChannelUserActionDTO;

/**
 * The type Remove users from channel moderators handler.
 */
//@HttpHandler(endpoint = HandlerEndpoints.Channels.REMOVE_MODERATOR, method = HttpMethod.POST, interfaceType = IRemoveUserFromChannelModeratorsHandler.class)
public class RemoveUsersFromChannelModeratorsHandler extends ChannelHandler implements IRemoveUserFromChannelModeratorsHandler {

    @Inject
    private DatabaseHandler handler;

    @Override
    public void handle(RoutingContext context) {
        var dto = getGsonService().fromJson(context.getBodyAsString(), ChannelUserActionDTO.class);
        reloadById(Channel.class, dto.getChannelId(), getQryModifier()).setHandler(r -> {
            if (r.succeeded()) {
                var channel = r.result();
                channel.getModerator().removeIf(user -> user.getId() == dto.getUserId());
                handler.saveOrUpdateObject(Channel.class, channel).setHandler(end -> handleChannelCompleted(end, context));
            }
        });
    }
}

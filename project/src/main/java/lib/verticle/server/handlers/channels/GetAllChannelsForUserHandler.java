package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.services.GsonService.GsonService;
import lib.verticle.server.dtos.RequestChannelResponseDTO;
import lib.verticle.server.handlers.AbstractHttpHandler;

import java.util.ArrayList;
import java.util.List;


/**
 * The GetAllChannelsForUserHandler gets called and retrieves username from context, then gets all channels
 * for this user and puts the id, name and ispublic flag in a DTO this DTO gets put in a List and the list gets sent as response
 */
@HttpHandler(endpoint = "/channels", method = HttpMethod.GET, interfaceType = IGetAllChannelsForUserHandler.class)
public class GetAllChannelsForUserHandler extends AbstractHttpHandler implements IGetAllChannelsForUserHandler {

    @Inject
    private IQueryService queryService;
    @Inject
    private GsonService gsonService;

    @Override
    public void handle(RoutingContext context) {

        queryService.getUserByUsername(context.session().get("username")).setHandler(r -> {
            if (r.succeeded()) {
                queryService.getChannelForUser(r.result().getId()).setHandler(r2 -> {
                    if (r2.succeeded()) {
                        List<Channel> channelList = r2.result();
                        List<RequestChannelResponseDTO> requestChannelResponseList = new ArrayList<>();
                        for (Channel c : channelList) {
                            requestChannelResponseList.add(new RequestChannelResponseDTO(c.getId(), c.getName(), c.getPublic()));
                        }
                        String json = gsonService.toJson(requestChannelResponseList);
                        HttpServerResponse response = context.response();
                        response.putHeader("content-type", "application/json");
                        response.end(json);
                    }
                });
            }
        });
    }
}




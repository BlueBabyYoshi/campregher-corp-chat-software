package lib.verticle.server.handlers.channels.dtos;

import java.util.List;

public class ChannelSettingsDTO {

    private String channelName;
    private boolean isPublic;
    private boolean isLocked;
    private List<ChannelUserModeratorEntity> userModList;
    private boolean currentUserIsMod;

    public ChannelSettingsDTO(String channelName, boolean isPublic, boolean isLocked) {
        this.channelName = channelName;
        this.isPublic = isPublic;
        this.isLocked = isLocked;
    }

    public ChannelSettingsDTO(String channelName, boolean isPublic, boolean isLocked, List<ChannelUserModeratorEntity> userModList) {
        this.channelName = channelName;
        this.isPublic = isPublic;
        this.isLocked = isLocked;
        this.userModList = userModList;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }

    public List<ChannelUserModeratorEntity> getUserModList() {
        return userModList;
    }

    public void setUserModList(List<ChannelUserModeratorEntity> userModList) {
        this.userModList = userModList;
    }

    public boolean isCurrentUserIsMod() {
        return currentUserIsMod;
    }

    public void setCurrentUserIsMod(boolean currentUserIsMod) {
        this.currentUserIsMod = currentUserIsMod;
    }
}

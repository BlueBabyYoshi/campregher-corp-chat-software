package lib.annotations;

import io.vertx.core.http.HttpMethod;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The interface HttpHandler Annotation.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface HttpHandler {

    /**
     * Endpoint string/URL endpoint the handler can be called with.
     *
     * @return the string
     */
    String endpoint();

    /**
     * Sets the HttpMethod the handler has to be called with.
     *
     * @return the http method
     */
    HttpMethod method();

    /**
     * Sets the Interface type for the Handler which is important for Dependency Injection
     *
     * @return the class
     */
    Class interfaceType() default void.class;
}

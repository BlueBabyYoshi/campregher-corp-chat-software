package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;
import lib.model.BaseModel;
import lib.model.Channel;
import lib.model.User;
import lib.services.GsonService.GsonService;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.channels.dtos.ChannelUserListActionDTO;

import java.util.stream.Collectors;

/**
 * The EditChannelModeratorsHandler gets channelid, and a list of users that are set as moderators for a channel via Rest Call,
 * we retrieve the channel via id, check if current calling user is allowed to change the settings,
 * then compare the lists and create a list for users IDs to be added and users to be deleted from the current moderators Set on the channel
 * we just remove the UserIds that in the delete list, then we retreive all users, create a list of full users entitites to be added, add them to the channelUsers Set and save the channel in the DB
 */
@HttpHandler(endpoint = HandlerEndpoints.Channels.EDIT_MODS, method = HttpMethod.POST, interfaceType = IEditChannelModeratorsHandler.class)
public class EditChannelModeratorsHandler extends ChannelHandler implements IEditChannelModeratorsHandler {

    @Inject
    private DatabaseHandler handler;
    @Inject
    private GsonService gsonService;

    @Override
    public void handle(RoutingContext context) {
        var newChannelEdit = gsonService.fromJson(context.getBodyAsString(), ChannelUserListActionDTO.class);
        reloadById(Channel.class, newChannelEdit.getChannelId(), getQryModifier()).setHandler(res -> {
            if (res.succeeded()) {
                var channel = res.result();
                var newModeratorList = newChannelEdit.getUserId();
                var moderatorSet = channel.getModerator();
                var deletelist = moderatorSet.stream().filter(x -> !newModeratorList.contains(x.getId())).collect(Collectors.toList());
                var currentUsersIdList = moderatorSet.stream().map(BaseModel::getId).collect(Collectors.toList());
                var addList = newModeratorList.stream().filter(y -> !currentUsersIdList.contains(y)).collect(Collectors.toList());
                if (deletelist.size() > 0) {
                    for (User user : deletelist) {
                        channel.getModerator().removeIf(u -> u.getId() == user.getId());
                    }
                }
                handler.readObjects(User.class).setHandler(userListRetrieve -> {
                    if (userListRetrieve.succeeded()) {
                        var allUsers = userListRetrieve.result();
                        var fullModAddList = allUsers.stream().filter(x -> addList.contains(x.getId())).collect(Collectors.toList());
                        fullModAddList.forEach(user -> channel.getModerator().add(user));

                        handler.saveOrUpdateObject(Channel.class, channel).setHandler(saveRes -> handleChannelSettingSaveCompleted("ModList for Channel:" + saveRes.result().getName() + " got saved!", context));
                    }
                });
            }
        });
    }
}

package lib.verticle.server.handlers.channels.delegates;

import lib.model.Channel;
import lib.model.User;

/**
 * The interface Channel manipulation.
 */
public interface ChannelManipulation {

    /**
     * Manipulate.
     *
     * @param inst the inst
     * @param user the user
     */
    void manipulate(Channel inst, User user);
}

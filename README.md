# Team4: Satsuma

Das Projekt von Team4 mit dem Fokus auf Reactive Programming

## Docker Deployment
 * Make sure that the ports 80 (Frontend), 3306 (db) and 8080 REST are not used on your machine and the docker service is running
 * Install docker and docker-compose
 * Go to the base directory of the Project where the "docker-compose.yml" file is located.
 * Run the command: 
```
sudo docker-compose build
```
 * if everying was built start the container with: 
```
sudo docker-compose up
```
 * Especially the first startup could take some time.
 * The Project Page should be avalible on: "http://localhost/" no port
 * If done, shut down docker-compose with ctl + C and follow it up with a 
```
sudo docker-compose down
``` 

## Login Data
To ensure a easy login the test users id and password gets printed out in the console. For confort the first 3 Users credentials were changed to:
* id: user1 pw: password Role: [Admin and User]
* id: user2 pw: password Role: [Only Admin]
* id: user3 pw: password Role: [Only User]

Username and password of other users gets printed out on console.


### Technologie Stack
* Maven
* Hibernate
* Vertx,Vertx Web, Vertx Auth
* Test: JUnit
* ReactJS (& Material UI)
* Dokumentation: Javadoc
* Deployment; Docker



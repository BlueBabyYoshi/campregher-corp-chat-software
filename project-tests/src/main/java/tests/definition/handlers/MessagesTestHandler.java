package tests.definition.handlers;

import controller.AuthenticationController;
import io.vertx.core.Vertx;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import lib.Database.services.DatasourceService;
import lib.Database.services.QueryService;
import lib.model.Channel;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.verticle.server.dtos.LoginDTO;
import lib.verticle.server.dtos.MessageNewMessageDTO;
import lib.verticle.server.dtos.NewUserDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import tests.base.testdefinition.AbstractVertxTest;

import java.util.HashSet;
import java.util.Set;

/**
 * The type Messages test handler.
 */
@RunWith(VertxUnitRunner.class)
public class MessagesTestHandler extends AbstractVertxTest {

    /**
     * Sets database.
     *
     * @param context the context
     */
    @BeforeEach
    public void setupDatabase(TestContext context) {
        QueryService service = getInjector().getInstance(QueryService.class);
        clearDatabaseFrom(User.class);
        clearDatabaseFrom(Channel.class);
        Set<User> setUser = new HashSet<>();
        Channel channel = new Channel("Allgemeines", true, setUser, false);
        service.saveChannel(channel);
    }

    /**
     * Login user test.
     *
     * @param context the context
     * @throws Throwable the throwable
     */
    @Test
    public void loginUserTest(TestContext context) throws Throwable {
        QueryService service = getInjector().getInstance(QueryService.class);
        DatasourceService dataSource = new DatasourceService();
        AuthenticationController controller = new AuthenticationController(Vertx.vertx(), service, dataSource.createDataSource());
        NewUserDTO userInfo = new NewUserDTO("user1", "Peter", "user1@test.at", true, true, "IT", "1", EmailFrequency.valueOf("Weekly").toString());
        runInContext(ctx -> {
            controller.registerNewUser(userInfo).setHandler(regQ -> {
                if (regQ.succeeded()) {
                    service.getUserByUsername("user1").setHandler(userQ -> {
                        if (userQ.succeeded()) {
                            User user = userQ.result();
                            user.setSalt("salt");
                            String password = controller.hashPassword("password", "salt");
                            user.setPassword(password);
                            service.saveUser(user).setHandler(userS -> {
                                if (userS.succeeded()) {
                                    LoginDTO loginInfo = new LoginDTO();
                                    loginInfo.setUsername("user1");
                                    loginInfo.setPassword("password");
                                    controller.loginUser(loginInfo).setHandler(loginQ -> {
                                        if (loginQ.succeeded()) {
                                            io.vertx.ext.auth.User vertxUser = loginQ.result();
                                            context.put("user", vertxUser);
                                            context.put("username", "user1");
                                            var client = getRestClient();
                                            MessageNewMessageDTO messageDTO = new MessageNewMessageDTO("Test text", 1);
                                            try {
                                                String actual = client.post("/setMessage", messageDTO, String.class);
                                                String expected = "test";
                                                Assert.assertEquals(expected, actual);
                                            } catch (Throwable throwable) {
                                                throwable.printStackTrace();
                                            }
                                        } else {
                                            Assert.assertEquals(true, false);
                                        }
                                    });
                                }
                            });
                        }
                    });


                }
            });
        }, 7);

    }

}

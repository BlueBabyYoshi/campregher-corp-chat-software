package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Delete channel handler.
 */
public interface IDeleteChannelHandler extends IHttpHandler {
}

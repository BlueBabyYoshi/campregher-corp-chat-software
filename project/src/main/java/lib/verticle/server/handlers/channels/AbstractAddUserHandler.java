package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import lib.Database.AsyncJinq;
import lib.model.Channel;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.channels.delegates.ChannelManipulation;

/**
 * The Abstract add user handler.
 */
public abstract class AbstractAddUserHandler extends ChannelHandler {

    @Inject
    private DatabaseHandler handler;

    /**
     * Add user.
     *
     * @param userId        the user id
     * @param channelId     the channel id
     * @param context       the context
     * @param changeChannel the change channel
     */
    protected void addUser(int userId, int channelId, RoutingContext context, ChannelManipulation changeChannel) {
        reloadById(Channel.class, channelId, getQryModifier()).setHandler(r -> {
            if (r.succeeded()) {
                var channel = r.result();
                handler.jinqQuery(User.class).setHandler(qryRequest -> {
                    if (qryRequest.succeeded()) {
                        var qry = qryRequest.result();
                        var idQry = qry.where(user -> user.getId() == userId);
                        AsyncJinq.asyncFirstOrDefault(idQry, getVertx()).setHandler(qryResult -> {
                            if (qryResult.succeeded()) {
                                var user = qryResult.result();
                                changeChannel.manipulate(channel, user);
                                handler.saveOrUpdateObject(Channel.class, channel).setHandler(end -> context.response().end());
                            }
                        });
                    }
                });
            }
        });
    }
}

package lib.services.fileupload;

import io.vertx.core.Future;
import lib.model.Message;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * The interface File upload service.
 */
public interface IFileUploadService {
    /**
     * Save message attachment.
     *
     * @param message  the message
     * @param fileName the file name
     * @param content  the content
     * @throws IOException        the io exception
     * @throws URISyntaxException the uri syntax exception
     */
    void saveMessageAttachment(Message message, String fileName, byte[] content) throws IOException, URISyntaxException;

    /**
     * Read file from directory if not available there read it from database
     *
     * @param message            the message
     * @param takeFromFileSystem the take from file system
     * @return the future
     * @throws IOException        the io exception
     * @throws URISyntaxException the uri syntax exception
     */
    Future<byte[]> readFile(Message message, boolean takeFromFileSystem) throws IOException, URISyntaxException;
}

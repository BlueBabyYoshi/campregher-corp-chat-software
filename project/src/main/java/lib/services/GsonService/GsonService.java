package lib.services.GsonService;

/**
 * The interface Gson service.
 */
public interface GsonService {

    /**
     * To json string.
     *
     * @param <T> the type parameter
     * @param obj the obj
     * @return the string
     */
    <T> String toJson(T obj);

    /**
     * From json t.
     *
     * @param <T>  the type parameter
     * @param json the json
     * @param type the type
     * @return the t
     */
    <T> T fromJson(String json, Class<T> type);
}

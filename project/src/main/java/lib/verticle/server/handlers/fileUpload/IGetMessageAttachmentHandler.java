package lib.verticle.server.handlers.fileUpload;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Get message attachment handler.
 */
public interface IGetMessageAttachmentHandler extends IHttpHandler {
}

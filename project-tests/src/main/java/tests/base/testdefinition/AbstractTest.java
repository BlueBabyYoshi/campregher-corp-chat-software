package tests.base.testdefinition;

import org.junit.Before;

/**
 * The type Abstract test.
 */
public abstract class AbstractTest {

    private static Class<? extends AbstractTest> testClass;

    /**
     * Run setup.
     *
     * @throws Throwable the throwable
     */
    @Before
    public void runSetup() throws Throwable {
        if (!this.getClass().equals(testClass)) {
            setup();
            testClass = this.getClass();
        }
    }

    /**
     * Throw please.
     *
     * @param ex the ex
     */
    protected void throwPlease(Throwable ex) {
        throw new RuntimeException(ex);
    }

    /**
     * Sets .
     *
     * @throws Throwable the throwable
     */
    protected abstract void setup() throws Throwable;
}

package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Edit channel users handler.
 */
public interface IEditChannelUsersHandler extends IHttpHandler {
}

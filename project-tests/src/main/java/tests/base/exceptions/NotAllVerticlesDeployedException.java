package tests.base.exceptions;

/**
 * The type Not all verticles deployed exception.
 */
public class NotAllVerticlesDeployedException extends Exception {

    /**
     * Instantiates a new Not all verticles deployed exception.
     */
    public NotAllVerticlesDeployedException() {
        super("Not all verticles could be deployed.");
    }
}

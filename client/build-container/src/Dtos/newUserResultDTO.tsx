
export interface NewUserResultDTO {
    success: boolean;
    message: string;
}
package lib.verticle.server.handlers;

import com.google.inject.Inject;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import lib.Database.AsyncJinq;
import lib.model.BaseModel;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.basic.BaseEventbusUser;
import org.jinq.jpa.JPAJinqStream;

import java.util.function.Function;

/**
 * The type Abstract http handler.
 */
public abstract class AbstractHttpHandler extends BaseEventbusUser implements IHttpHandler {

    /**
     * The Handler.
     */
    @Inject
    protected DatabaseHandler handler;
    private Vertx vertx;

    @Override
    public void setVertxInstance(Vertx vertx) {

        this.vertx = vertx;
    }

    /**
     * Gets vertx.
     *
     * @return the vertx
     */
    protected Vertx getVertx() {
        return vertx;
    }

    /**
     * Reload by id future.
     *
     * @param <T>         the type parameter
     * @param type        the type
     * @param id          the id
     * @param qryModifier the qry modifier
     * @return the future
     */
    protected <T extends BaseModel> Future<T> reloadById(Class<T> type, int id, Function<JPAJinqStream<T>, JPAJinqStream<T>> qryModifier) {
        Promise<T> promise = Promise.promise();
        handler.jinqQuery(type).setHandler(r -> {
            if (r.succeeded()) {
                var qry = r.result();
                var fetchedQry = qryModifier.apply(qry);
                var idQry = fetchedQry.where(obj -> obj.getId() == id);
                AsyncJinq.asyncFirstOrDefault(idQry, getVertx()).setHandler(queryResult -> {
                    if (queryResult.succeeded()) {
                        promise.complete(queryResult.result());
                    }
                });
            }
        });
        return promise.future();
    }
}

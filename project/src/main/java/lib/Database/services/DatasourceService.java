package lib.Database.services;

import com.google.inject.Inject;
import lib.Database.datasources.DataSourceFactory;
import lib.Database.datasources.DataSourceFactoryImpl;
import lib.Database.persistenceModel.MetaInfPersistence;

import javax.sql.DataSource;

/**
 * The Datasource service.
 */
public class DatasourceService {

    @Inject
    private DataSourceFactory dataSourceFactory = new DataSourceFactoryImpl();
    @Inject
    private PersistenceReaderService readerService = new PersistenceReaderServiceImpl();

    /**
     * Create data source from persistence.xml configuration.
     *
     * @return the data source
     */
    public DataSource createDataSource() {
        try {
            var persistenceInfo = readerService.readPersistence();
            var user = getValueFor(persistenceInfo, "user");
            var pw = getValueFor(persistenceInfo, "password");
            var url = getValueFor(persistenceInfo, "url");
            return dataSourceFactory.createDatasource(url, user, pw);
        } catch (Exception ex) {
            System.out.println("Error creating datasource: " + ex.toString());
        }
        return null;
    }

    private String getValueFor(MetaInfPersistence metaInfPersistence, String key) {
        var fullKey = "javax.persistence.jdbc." + key;
        var property = metaInfPersistence.getPersistenceUnit().getProperties().stream().filter(r -> r.getName().equals(fullKey)).findFirst().orElse(null);
        if (property != null) {
            return property.getValue();
        }
        return null;
    }
}

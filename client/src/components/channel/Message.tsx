export interface Message {
   id: number;
   text: string;
   username: string;
   myMessage: boolean;
   channelId: number;
}
package lib.Database;

import java.util.HashMap;

/**
 * The type Persistence.
 */
public class Persistence {

    private static HashMap<String, IDataBroker> dataBrokers = new HashMap<>();

    /**
     * Gets data broker.
     *
     * @param persistenceUnit the persistence unit
     * @return the data broker
     */
    public static IDataBroker getDataBroker(String persistenceUnit) {
        initializeDataBroker(persistenceUnit);
        return dataBrokers.get(persistenceUnit);
    }

    /**
     * Initialize data broker.
     *
     * @param persistenceUnit the persistence unit
     */
    public static void initializeDataBroker(String persistenceUnit) {
        if (!dataBrokers.containsKey(persistenceUnit)) {
            var broker = new DataBroker(persistenceUnit);
            dataBrokers.put(persistenceUnit, broker);
        }
    }

    /**
     * Close databroker/connection to database.
     */
    public static void Close() {
        for (var pair : dataBrokers.entrySet()) {
            pair.getValue().close();
        }
    }
}

package lib.verticle.server.handlers.users;

import com.google.gson.Gson;
import com.google.inject.Inject;
import controller.AuthenticationController;
import controller.MailController;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.auth.VertxContextPRNG;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.DatasourceService;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.User;
import lib.verticle.server.handlers.AbstractHttpHandler;
import lib.verticle.server.handlers.HandlerEndpoints;


/**
 * The Reset password handler.
 * It takes the E-Mail Adress as input, looks for the user, if found it changes the password and salt to a random one and sends it to the user mail.
 */
@HttpHandler(endpoint = HandlerEndpoints.Users.RESET_PASSWORD, method = HttpMethod.POST, interfaceType = IResetPasswordHandler.class)
public class ResetPasswordHandler extends AbstractHttpHandler implements IResetPasswordHandler {

    @Inject
    private IQueryService queryService;
    private AuthenticationController controller;

    @Override
    public void handle(RoutingContext context) {

        var gson = new Gson();
        String body = context.getBodyAsString();
        System.out.println(body);
        if (!body.equals("")) {
            queryService.getUserByEmail(body, getVertx()).setHandler(getUser -> {
                if (getUser.succeeded()) {
                    User result = getUser.result();
                    if (result != null) {
                        DatasourceService datasourceService = new DatasourceService();
                        controller = new AuthenticationController(getVertx(), queryService, datasourceService.createDataSource());
                        String plainPassword = VertxContextPRNG.current(getVertx()).nextString(12);
                        String salt = controller.generateSalt();
                        String newPassword = controller.hashPassword(plainPassword, salt);
                        result.setPassword(newPassword);
                        result.setSalt(salt);
                        queryService.updateUser(result).setHandler(updateUser -> {
                            if (updateUser.succeeded()) {
                                writeResponseToContext(context, "Das neue Passwort für "
                                        + result.getUsername() + " wurde an " + body + " versendet.");

                                MailController.sendEmail(result.getEmailAddress(), "Ihr neues Passwort", "Auf Ihre E-Mail Adresse wurde ein neues Passwort bestellt. /n" +
                                        "Username: " + result.getUsername() + "/n" +
                                        "Passwort: " + plainPassword, getVertx());

                                System.out.println("neues Passwort: " + plainPassword);
                            } else {
                                writeResponseToContext(context, "Benutzer konnte nicht gefunden werden");
                            }
                        });
                    } else {
                        writeResponseToContext(context, "Benutzer konnte nicht gefunden werden");
                    }
                } else {
                    System.out.println("Error");
                }
            });
        } else {
            System.out.println("error");
            writeResponseToContext(context, "No data recived");
        }
    }

    private void writeResponseToContext(RoutingContext context, String answer) {
        var gson = new Gson();
        var resultAsString = gson.toJson(answer);
        context.response()
                .putHeader("Content-Type", "application/json")
                .end(resultAsString);
    }
}

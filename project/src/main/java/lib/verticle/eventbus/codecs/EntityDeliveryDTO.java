package lib.verticle.eventbus.codecs;

import java.io.Serializable;

/**
 * The EntityDeliveryDTO used in the PersistenceMessageCodec for en and decoding Objects.
 */
public class EntityDeliveryDTO implements Serializable {

    private Class<?> objectType;
    private boolean isCollection;
    private String jsonData;

    /**
     * Gets json data.
     *
     * @return the json data
     */
    public String getJsonData() {
        return jsonData;
    }

    /**
     * Sets json data.
     *
     * @param jsonData the json data
     */
    public void setJsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    /**
     * Gets object type.
     *
     * @return the object type
     */
    public Class<?> getObjectType() {
        return objectType;
    }

    /**
     * Sets object type.
     *
     * @param objectType the object type
     */
    public void setObjectType(Class<?> objectType) {
        this.objectType = objectType;
    }

    /**
     * Is collection boolean.
     *
     * @return the boolean
     */
    public boolean isCollection() {
        return isCollection;
    }

    /**
     * Sets collection.
     *
     * @param collection the collection
     */
    public void setCollection(boolean collection) {
        isCollection = collection;
    }
}

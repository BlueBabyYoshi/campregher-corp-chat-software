package lib.verticle.server.handlers.fileUpload;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Upload attachment to message handler.
 */
public interface IUploadAttachmentToMessageHandler extends IHttpHandler {
}

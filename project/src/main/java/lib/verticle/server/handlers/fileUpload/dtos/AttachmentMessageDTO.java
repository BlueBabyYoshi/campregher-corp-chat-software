package lib.verticle.server.handlers.fileUpload.dtos;

public class AttachmentMessageDTO {

    private byte[] data;

    /***
     * if this is true, there was an attachment found for the message and data is not empty
     */
    private boolean attachmentExists;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public boolean isAttachmentExists() {
        return attachmentExists;
    }

    public void setAttachmentExists(boolean attachmentExists) {
        this.attachmentExists = attachmentExists;
    }
}

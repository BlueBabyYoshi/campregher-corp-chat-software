package lib.Database;


import org.jinq.jpa.JPAJinqStream;
import org.jinq.jpa.JinqJPAStreamProvider;

import javax.persistence.Persistence;
import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The Data broker implementation.
 */
public class DataBroker implements IDataBroker {
    private final String persistenceUnit;
    @PersistenceContext(unitName = "EmplService")
    private EntityManagerFactory emf;
    private EntityManager em;
    private JinqJPAStreamProvider streamProvider;


    /**
     * Instantiates a new Data broker.
     *
     * @param unitName the unit name
     */
    public DataBroker(String unitName) {
        persistenceUnit = unitName;
        emf = Persistence.createEntityManagerFactory(persistenceUnit);
        streamProvider = new JinqJPAStreamProvider(emf);
    }

    @Override
    public void apply(EntityManager em) {
        try {
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    @Override
    public <T> T saveOrUpdate(T t) {
        em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            T inst = em.merge(t);
            em.getTransaction().commit();
            return inst;
        } catch (Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public <T> void saveOrUpdate(T t, EntityManager em) {
        try {
            em.merge(t);
        } catch (Exception e) {
            throw e;
        }
    }

    public <T> JPAJinqStream<T> getQuery(Class<T> i_Type, EntityManager em) {
        return streamProvider.streamAll(em, i_Type);
    }

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    @Override
    public <T> List<T> getAllObjects(Class<T> i_Type) {
        em = emf.createEntityManager();
        List<T> res = new ArrayList<>();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T> qry = builder.createQuery(i_Type);
            Root<T> rootEntry = qry.from(i_Type);
            CriteriaQuery<T> all = qry.select(rootEntry).distinct(true);
            TypedQuery<T> allQuery = em.createQuery(all);
            res = allQuery.getResultList();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            em.close();
        }
        return res;
    }

    @Override
    public <T> List<T> getAllObjects(Class<T> i_Type, String[] propertyNames) {
        em = emf.createEntityManager();
        List<T> res = new ArrayList<>();
        try {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<T> qry = builder.createQuery(i_Type);
            Root<T> rootEntry = qry.from(i_Type);
            for (String propName : propertyNames) {
                rootEntry.fetch(propName);
            }
            CriteriaQuery<T> all = qry.select(rootEntry).distinct(true);
            TypedQuery<T> allQuery = em.createQuery(all);
            res = allQuery.getResultList();
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            em.close();
        }
        return res;
    }

    @Override
    public <T> T getObjectById(Class<T> i_Type, int i_ID) {
        em = emf.createEntityManager();
        T temp = null;
        try {
            em.getTransaction().begin();
            temp = em.find(i_Type, i_ID);
            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Exception in getObjectById!");

        } finally {
            em.close();
        }
        return temp;
    }

    @Override
    public <T> T getObjectById(Class<T> i_Type, int i_ID, String[] params) {
        em = emf.createEntityManager();
        T temp = null;
        try {
            em.getTransaction().begin();
            EntityGraph graph = em.createEntityGraph(i_Type);
            Map<String, Object> hints = new HashMap<>();
            for (String param : params) {
                graph.addSubgraph(param);
            }
            hints.put("javax.persistence.loadgraph", graph);
            temp = em.find(i_Type, i_ID, hints);

            em.getTransaction().commit();
        } catch (Exception e) {
            em.getTransaction().rollback();
            System.out.println("Exception in getObjectById!");
        } finally {
            em.close();
        }
        return temp;
    }

    @Override
    public <T> T getObjectbyEmail(Class<T> i_Type, String i_String) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public <T> void deleteObject(Class<T> i_Type, int t) {
        em = emf.createEntityManager();
        try {
            em.getTransaction().begin();
            T entity = em.find(i_Type, t);
            em.remove(entity);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public <T> T queryEntity(Class<T> i_Type, String i_PropertyName, Object i_Value) {
        CriteriaBuilder qryBuilder = this.emf.getCriteriaBuilder();
        CriteriaQuery<T> qry = qryBuilder.createQuery(i_Type);
        Root<T> root = qry.from(i_Type);
        qry = qry.where(qryBuilder.equal(root.get(i_PropertyName), i_Value));
        List<T> res = ExecQuery(i_Type, qry);
        if (!res.isEmpty()) {
            return res.get(0);
        }
        return null;
    }

    public <T> List<T> queryEntityList(Class<T> i_Type, String i_PropertyName, Object i_Value) {
        CriteriaBuilder qryBuilder = this.emf.getCriteriaBuilder();
        CriteriaQuery<T> qry = qryBuilder.createQuery(i_Type);
        Root<T> root = qry.from(i_Type);
        qry = qry.where(qryBuilder.equal(root.get(i_PropertyName), i_Value));
        List<T> res = ExecQuery(i_Type, qry);
        if (!res.isEmpty()) {
            return res;
        }
        return null;
    }

    public <T> List<T> ExecQuery(Class<T> i_Type, CriteriaQuery<T> i_Query) {
        List<T> entities = null;
        em = emf.createEntityManager();
        try {
            entities = em.createQuery(i_Query).getResultList();
        } catch (Exception e) {
            System.out.println(e);
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
        return entities;
    }

    public void close() {
        emf.close();
        //streamProvider = null;
    }

    @Override
    public String getDatabaseName() {
        return persistenceUnit;
    }
}

package lib.Database.datasources.implementors;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import javax.sql.DataSource;

/**
 * The MS-SQL data source implementor.
 */
public class MsSqlDataSourceImplementor extends BaseDataSourceFunctionalityImplementor<SQLServerDataSource> {

    @Override
    protected Class<SQLServerDataSource> getAssumedDataSourceType() {
        return SQLServerDataSource.class;
    }

    @Override
    protected void setUrlInternal(SQLServerDataSource source, String url) {
        source.setURL(url);
    }

    @Override
    protected void setUserInternal(SQLServerDataSource source, String user) {
        source.setUser(user);
    }

    @Override
    protected void setPasswordInternal(SQLServerDataSource source, String pw) {
        source.setPassword(pw);
    }

    @Override
    public DataSource create() {
        return new SQLServerDataSource();
    }
}

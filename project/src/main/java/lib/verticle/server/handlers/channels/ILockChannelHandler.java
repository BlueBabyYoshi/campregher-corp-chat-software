package lib.verticle.server.handlers.channels;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Lock channel handler.
 */
public interface ILockChannelHandler extends IHttpHandler {
}

package lib.verticle.server.handlers.channels.dtos;

public class ChannelEditDTO extends ChannelIdDTO {

    private String name;
    private boolean isPublic;
    private boolean isLocked;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPublic() {
        return isPublic;
    }

    public void setPublic(boolean aPublic) {
        isPublic = aPublic;
    }

    public boolean isLocked() {
        return isLocked;
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }
}

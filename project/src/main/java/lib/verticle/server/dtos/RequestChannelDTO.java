package lib.verticle.server.dtos;

public class RequestChannelDTO {
    //test only with text same in Frontend with DTO
    private int id;


    public RequestChannelDTO(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}

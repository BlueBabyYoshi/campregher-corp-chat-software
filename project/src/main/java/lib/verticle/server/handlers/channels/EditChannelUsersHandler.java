package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.annotations.HttpHandler;
import lib.model.BaseModel;
import lib.model.Channel;
import lib.model.User;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.channels.dtos.ChannelUserListActionDTO;

import java.util.stream.Collectors;

/**
 * The EditChannelUsersHandler gets channelid, and a list of users that are set as users for a channel via Rest Call,
 * we retrieve the channel via id, check if current calling user is allowed to change the settings,
 * then compare the lists and create a list for users IDs to be added and users to be deleted from the current users Set on the channel
 * we just remove the UserIds that in the delete list, then we retreive all users, create a list of full users entitites to be added, add them to the channelUsers Set and save the channel in the DB
 */
@HttpHandler(endpoint = HandlerEndpoints.Channels.EDIT_USERS, method = HttpMethod.POST, interfaceType = IEditChannelUsersHandler.class)
public class EditChannelUsersHandler extends ChannelHandler implements IEditChannelUsersHandler {

    @Inject
    private DatabaseHandler handler;

    @Override
    public void handle(RoutingContext context) {
        var newChannelEdit = getGsonService().fromJson(context.getBodyAsString(), ChannelUserListActionDTO.class);
        reloadById(Channel.class, newChannelEdit.getChannelId(), getQryModifier()).setHandler(res -> {
            if (res.succeeded()) {
                var channel = res.result();
                if (!channel.isUserMod(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to edit userlist of channel");
                    return;
                }
                var newUserList = newChannelEdit.getUserId();
                var userSet = channel.getChannelMembers();
                var deletelist = userSet.stream().filter(x -> !newUserList.contains(x.getId())).collect(Collectors.toList());
                var currentUsersIdList = userSet.stream().map(BaseModel::getId).collect(Collectors.toList());
                var addList = newUserList.stream().filter(y -> !currentUsersIdList.contains(y)).collect(Collectors.toList());
                if (deletelist.size() > 0) {
                    for (User user : deletelist) {
                        channel.getChannelMembers().removeIf(u -> u.getId() == user.getId());
                    }
                }
                handler.readObjects(User.class).setHandler(userListRetrieve -> {
                    if (userListRetrieve.succeeded()) {
                        var allUsers = userListRetrieve.result();
                        var fullUserAddList = allUsers.stream().filter(x -> addList.contains(x.getId())).collect(Collectors.toList());
                        fullUserAddList.forEach(user -> channel.getChannelMembers().add(user));

                        handler.saveOrUpdateObject(Channel.class, channel).setHandler(saveRes -> handleChannelSettingSaveCompleted("UserList for Channel:" + saveRes.result().getName() + " got saved!", context));
                    }
                });
            }
        });
    }
}

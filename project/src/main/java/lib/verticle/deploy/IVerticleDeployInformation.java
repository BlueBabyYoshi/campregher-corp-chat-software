package lib.verticle.deploy;

import io.vertx.core.Verticle;

/**
 * The interface of VerticleDeployInformation.
 */
public interface IVerticleDeployInformation {

    /**
     * Gets instance.
     *
     * @return the instance
     */
    Verticle getInstance();

    /**
     * Gets name.
     *
     * @return the name
     */
    String getName();
}

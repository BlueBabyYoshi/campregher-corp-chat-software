package lib.Database.services;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import lib.model.Channel;
import lib.model.Message;
import lib.model.User;

import java.util.List;

/**
 * The interface Query service.
 */
public interface IQueryService {

    /**
     * Save user.
     *
     * @param user the user
     * @return the saved user
     */
    Future<User> saveUser(User user);

    /**
     * Save channel.
     *
     * @param channel the channel
     * @return the saved channel
     */
    Future<Channel> saveChannel(Channel channel);

    /**
     * Save message.
     *
     * @param message the message
     * @return the saved message
     */
    Future<Message> saveMessage(Message message);

    /**
     * Gets message by id.
     *
     * @param messageId the messageid
     * @return the found message
     */
    Future<Message> getMessageById(int messageId);

    /**
     * Update user.
     *
     * @param user the user
     * @return the updated user
     */
    Future<User> updateUser(User user);

    /**
     * Gets user by username.
     *
     * @param username the username
     * @param vertx    the vertx
     * @return the found user
     */
    Future<User> getUserByUsername(String username, Vertx vertx);

    /**
     * Gets user by username.
     *
     * @param username the username
     * @return the found user
     */
    Future<User> getUserByUsername(String username);

    /**
     * Gets user by email.
     *
     * @param mail  the mail
     * @param vertx the vertx
     * @return the found user
     */
    Future<User> getUserByEmail(String mail, Vertx vertx);

    /**
     * Gets channel by channelname.
     *
     * @param channelname the channelname
     * @return the found channel
     */
    Future<Channel> getChannelByChannelname(String channelname);

    /**
     * Gets channel for user.
     *
     * @param userId the userid
     * @return List of channels for a user
     */
    Future<List<Channel>> getChannelForUser(int userId);

    /**
     * Gets messages for a channel by channelid.
     *
     * @param channelId the channelid
     * @return List of Messages for a channel
     */
    Future<List<Message>> getMessages(int channelId);

    /**
     * Gets all users.
     *
     * @return List of all user in the database
     */
    Future<List<User>> getAllUsers();

    /**
     * Gets channel by id.
     *
     * @param channelId the channelid
     * @return the found channel
     */
    Future<Channel> getChannelById(int channelId);
}

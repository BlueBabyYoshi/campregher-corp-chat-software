package lib.verticle.server.handlers.users;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface User info handler.
 */
public interface IUserInfoHandler extends IHttpHandler {
}

package lib.verticle.server.handlers.channels;

import com.google.inject.Inject;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import lib.Database.services.IQueryService;
import lib.annotations.HttpHandler;
import lib.model.Channel;
import lib.services.GsonService.GsonService;
import lib.verticle.Database.handlers.DatabaseHandler;
import lib.verticle.server.handlers.HandlerEndpoints;
import lib.verticle.server.handlers.channels.dtos.ChannelIdDTO;

/**
 * The DeleteChannelHandler gets channelid via rest call, we reload the channelby ID and check if calling user is allowed to delete the channel,
 * if the user is allowed to we logically delete the channel in the DB/Set the isdeleted Flag which according to DB implementation makes them invisible in normal retrieve calls
 */
@HttpHandler(endpoint = HandlerEndpoints.Channels.DELETE_CHANNEL, method = HttpMethod.DELETE, interfaceType = IDeleteChannelHandler.class)
public class DeleteChannelHandler extends ChannelHandler implements IDeleteChannelHandler {

    @Inject
    private DatabaseHandler handler;
    @Inject
    private IQueryService queryService;
    @Inject
    private GsonService gsonService;

    @Override
    public void handle(RoutingContext context) {
        var channelIdDTO = gsonService.fromJson(context.getBodyAsString(), ChannelIdDTO.class);
        queryService.getChannelById(channelIdDTO.getChannelId()).setHandler(getChannel -> {
            if (getChannel.succeeded()) {
                Channel channel = getChannel.result();
                if (!channel.isUserMod(context.session().get("username"))) {
                    System.out.println("WARNING: unauthorized user wanted to delete channel");
                    return;
                }
                handler.deleteObject(Channel.class, channel).setHandler(end ->
                        context.response().end()
                );

            }
        });
    }
}

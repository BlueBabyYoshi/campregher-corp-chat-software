package lib.verticle.server;

import com.google.inject.Injector;
import io.vertx.core.Verticle;
import lib.verticle.deploy.IVerticleDeployInformation;

/**
 * The ServerVerticleDeployInformation, important for Verticle Deployment
 */
public class ServerVerticleDeployInformation implements IVerticleDeployInformation {

    private ServerVerticle instance;

    public ServerVerticleDeployInformation(Injector injector) {

        instance = new ServerVerticle(injector);
    }

    @Override
    public Verticle getInstance() {
        return instance;
    }

    @Override
    public String getName() {
        return "server";
    }
}

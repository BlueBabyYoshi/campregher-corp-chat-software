package lib.model;

/**
 * The enum Email frequency.
 */
public enum EmailFrequency {
    Weekly,
    Monthly,
    Never
}

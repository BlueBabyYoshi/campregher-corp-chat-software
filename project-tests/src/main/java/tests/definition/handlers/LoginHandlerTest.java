package tests.definition.handlers;

import controller.AuthenticationController;
import lib.Database.services.QueryService;
import lib.model.EmailFrequency;
import lib.model.User;
import lib.verticle.server.dtos.LoginDTO;
import lib.verticle.server.dtos.LoginResultDTO;
import lib.verticle.server.dtos.NewUserDTO;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import tests.base.testdefinition.AbstractVertxTest;

/**
 * The type Login handler test.
 */
public class LoginHandlerTest extends AbstractVertxTest {

    /**
     * Sets database.
     */
    @BeforeEach
    public void setupDatabase() {
        clearDatabaseFrom(User.class);
        QueryService handler = getInjector().getInstance(QueryService.class);
        AuthenticationController controller = new AuthenticationController();
        NewUserDTO userInfo = new NewUserDTO("user1", "Peter", "user1@test.at", true, true, "IT", "1", EmailFrequency.valueOf("Weekly").toString());
        controller.registerNewUser(userInfo);

    }

    /**
     * Login user test.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void loginUserTest() throws Throwable {
        QueryService handler = getInjector().getInstance(QueryService.class);
        AuthenticationController controller = new AuthenticationController();
        var client = getRestClient();
        LoginDTO loginInfo = new LoginDTO();
        loginInfo.setPassword("password");
        loginInfo.setUsername("user1");
        handler.getUserByUsername("user1").setHandler(userQ -> {
            if (userQ.succeeded()) {
                User user = userQ.result();
                String newSalt = controller.generateSalt();
                String newPw = controller.hashPassword("password", newSalt);
                user.setSalt(newSalt);
                user.setPassword(newPw);
                handler.saveUser(user).setHandler(res -> {
                    if (res.succeeded()) {
                        LoginResultDTO result = null;
                        try {
                            result = client.post("/login", loginInfo, LoginResultDTO.class);
                            LoginResultDTO expected = new LoginResultDTO();
                            expected.setMessage("Benutzer " + "user1" + " erfolgreich eingeloggt");
                            expected.setSuccess(true);
                            Assert.assertEquals(expected, result);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }

                    }
                });
            }
        });
    }

    /**
     * Login user test fail.
     *
     * @throws Throwable the throwable
     */
    @Test
    public void loginUserTestFail() throws Throwable {
        QueryService handler = getInjector().getInstance(QueryService.class);
        AuthenticationController controller = new AuthenticationController();
        var client = getRestClient();
        LoginDTO loginInfo = new LoginDTO();
        loginInfo.setPassword("password");
        loginInfo.setUsername("user1");
        handler.getUserByUsername("user1").setHandler(userQ -> {
            if (userQ.succeeded()) {
                User user = userQ.result();
                String newSalt = controller.generateSalt();
                String newPw = controller.hashPassword("password123", newSalt);
                user.setSalt(newSalt);
                user.setPassword(newPw);
                handler.saveUser(user).setHandler(res -> {
                    if (res.succeeded()) {
                        LoginResultDTO result = null;
                        try {
                            result = client.post("/login", loginInfo, LoginResultDTO.class);
                            LoginResultDTO expected = new LoginResultDTO();
                            expected.setMessage("Konnte Benutzer nicht einloggen.");
                            expected.setSuccess(false);
                            Assert.assertEquals(expected, result);
                        } catch (Throwable throwable) {
                            throwable.printStackTrace();
                        }

                    }
                });
            }
        });
    }

}

package lib.verticle.server.handlers.messages;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Delete message handler.
 */
public interface IDeleteMessageHandler extends IHttpHandler {
}

package lib.verticle.eventbus.codecs;

/**
 * The MessageCodecNames, in our case we only use one codec, would be able to use different codecs for different communication if needed.
 */
public class MessageCodecNames {

    /**
     * The constant PERSISTENCE.
     */
    public static final String PERSISTENCE = "persistence";
}

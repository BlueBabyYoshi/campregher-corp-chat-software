package lib.verticle.server;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * The Push message service.
 */
//Class for Websockets that queues the messages that are written
public class PushMessageService {

    /**
     * The Message queue.
     */
    String[] messageQueue;

    private volatile Queue<String[]> queuedMessages = new ArrayDeque<>();

    /**
     * Gets next message.
     *
     * @return the next message
     */
    synchronized public String getNextMessage() {
        return queuedMessages.poll()[0];
    }

    /**
     * Send message to client string.
     *
     * @param message the message
     * @return the string
     */
    public String sendMessageToClient(String message) {
        return "";
    }


    /**
     * Push message.
     *
     * @param message the message
     * @param channel the channel
     */
    synchronized public void pushMessage(String message, String channel) {
        String[] arr = new String[]{message, channel};
        this.queuedMessages.add(arr);
    }

}

export interface AuthorizationResultDTO {
    isAuthenticated: boolean;
    isAuthorized: boolean;
}
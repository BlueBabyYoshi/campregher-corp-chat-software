package lib.verticle.server.handlers.auth;

import lib.verticle.server.handlers.IHttpHandler;

/**
 * The interface Logout handler.
 */
public interface ILogoutHandler extends IHttpHandler {
}

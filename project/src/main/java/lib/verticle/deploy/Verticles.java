package lib.verticle.deploy;

import com.google.inject.Injector;
import lib.verticle.Database.DBVerticleDeployInformation;
import lib.verticle.server.ServerVerticleDeployInformation;

import java.util.ArrayList;
import java.util.Collection;

/**
 * The Verticles.
 */
public class Verticles {

    /**
     * Gets list of registered app verticles, fill in more Verticles below if needed.
     *
     * @param injector the injector
     * @return the app verticles
     */
    public static Collection<IVerticleDeployInformation> getAppVerticles(Injector injector) {
        var list = new ArrayList<IVerticleDeployInformation>();
        list.add(new ServerVerticleDeployInformation(injector));
        list.add(new DBVerticleDeployInformation());
        return list;
    }
}

package lib.verticle.server.dtos;

public class RemoveMessageDTO {
    //test only with text same in Frontend with DTO
    private int id;
    private int channelId;

    public RemoveMessageDTO(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

}
